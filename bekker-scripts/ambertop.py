import numpy as np, os
from parmed import gromacs, amber
gromacs.GROMACS_TOPDIR = os.path.basename(__file__ )+"../share/top/"

systems = [] 
extraParams = []
tleap = None

def genPosres(nfo, lines):
  fname, fc, DEFINE = nfo
  
  idxs = []
  atomMode = False
  for line in lines:
    tmp = line.split()
    if atomMode and line[0] != ";" and len(tmp) > 7:
      if float(tmp[7]) >= 2: idxs.append(int(tmp[0]))
    elif line.strip().replace("[", "").replace("]", "").strip() == "atoms": atomMode = True
    elif line.strip().replace("[", "").replace("]", "").strip() == "bonds": break

  NNN = len(str(np.max(idxs)))
  fc = str(fc).rjust(len(str(fc)))
  out = "[ position_restraints ]\n"
  for idx in idxs: out += str(idx).rjust(NNN) + " 1 " + fc + " " + fc + " " + fc + "\n"
  open("../"+fname, "w").write(out)


def execute():
  global systems, extraParams, tleap

  if not os.path.exists("prep"): os.mkdir("prep")
  os.chdir("prep")

  open("prep.tleap", "w").write(tleap)
  os.system("tleap -f prep.tleap")

  if not os.path.exists("temp.prmtop"):
    print("Could not generate topology")
    exit()

  renamer = {}
  posres = {}

  for idx, system in enumerate(systems):
    if type(system) == list: 
      prename = system[0]
      system = system[1]
    else: prename = "system%s"%(idx+1)
    renamer[prename] = system
    posres[system] = [["posre_%s.itp"%(system), 1000, "POSRES"]]

  addParameters = []
  if "opc" in extraParams:
    extraParams.remove("opc")
    addParameters.append([]) # water
    addParameters[-1].append("SOL") # name, moleculetype, atomtypes, bondtypes

    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
SOL          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 SOL rtp SOL q 0.0
    1         OW      1    SOL     OW      1 -0.89517000  16.000000   ; qtot -0.895170
    2         HW      1    SOL    HW1      2 0.44758500   1.008000   ; qtot -0.447585
    3         HW      1    SOL    HW2      3 0.44758500   1.008000   ; qtot 0.000000

#ifdef FLEXIBLE

[ bonds ]
;    ai     aj funct         c0         c1         c2         c3
      2      3     1
      1      2     1
      1      3     1


#else

[ settles ]
; i     funct   doh     dhh
1     1   0.09788820   0.15985070

#endif

[ exclusions ]
1  2  3
2  1  3
3  1  2
  """)
    addParameters[-1].append(["OW             8  16.000000  0.00000000  A     0.31742704      0.6836907\n", "HW             1   1.008000  0.00000000  A              0              0\n"])
    addParameters[-1].append(["HW    HW       1   0.15985   462750.400000\n", "OW    HW       1   0.09789   462750.400000\n"])

  if "CL" in extraParams:
    extraParams.remove("CL")
    addParameters.append([])
    addParameters[-1].append("CL")
    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
CL          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 CL rtp CL q -1.0
    1        Cl-      1     CL     CL      1 -1.00000000  35.450000   ; qtot -1.000000
  """)

    addParameters[-1].append(["Cl-           17  35.450000  0.00000000  A     0.41088249       2.687244\n"])
    addParameters[-1].append([])

  if "NA" in extraParams:
    extraParams.remove("NA")
    addParameters.append([])
    addParameters[-1].append("NA")
    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
NA          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 NA rtp NA q 1.0
    1        Na+      1     NA     NA      1 1.00000000  22.990000   ; qtot 1.000000
  """)

    addParameters[-1].append(["Na+           11  22.990000  0.00000000  A     0.26174604     0.12602877\n"])
    addParameters[-1].append([])

  if "K" in extraParams:
    extraParams.remove("K")
    addParameters.append([])
    addParameters[-1].append("K")
    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
K          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 K rtp K q 1.0
    1         K+      1      K      K      1 1.00000000  39.100000   ; qtot 1.000000
  """)

    addParameters[-1].append(["K+            19  39.100000  0.00000000  A      0.3034401     0.58667224\n"])
    addParameters[-1].append([])

  if "MG" in extraParams:
    extraParams.remove("MG")
    addParameters.append([])
    addParameters[-1].append("MG")
    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
MG          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 MG rtp MG q 2.0
    1       Mg2+      1     MG     MG      1 2.00000000  24.305000   ; qtot 2.000000
  """)

    addParameters[-1].append(["Mg2+          12  24.305000  0.00000000  A     0.24285899    0.044157015\n"])
    addParameters[-1].append([])
    
  if "CA" in extraParams:
    extraParams.remove("CA")
    addParameters.append([])
    addParameters[-1].append("CA")
    addParameters[-1].append("""[ moleculetype ]
; Name            nrexcl
CA          3

[ atoms ]
;   nr       type  resnr residue  atom   cgnr    charge       mass  typeB    chargeB      massB
; residue    1 CA rtp CA q 2.0
    1       Ca2+      1     CA     CA      1 2.00000000  40.080000   ; qtot 2.000000
  """)

    addParameters[-1].append(["Ca2+          20  40.080000  0.00000000  A     0.29007662     0.39325717\n"])
    addParameters[-1].append([])

  if len(extraParams):
    print("Could not add the following parameters:", extraParams)
    exit()


  system = amber.AmberParm("temp.prmtop")
  system.load_rst7("temp.inpcrd")

  if os.path.exists("temp.top"): os.remove("temp.top")
  if os.path.exists("parameters.itp"): os.remove("parameters.itp")
  if os.path.exists("parmed.gro"): os.remove("../parmed.gro")

  system.save("temp.top", combine=None, parameters="parameters.itp")
  system.save("parmed.gro")

  ITPs = []
  top = []
  itpMode, moleculesMode = False, False
  for line in open("temp.top").readlines():
    if line.strip().replace("[", "").replace("]", "").strip() == "moleculetype":
      ITPs.append([None, []])
      itpMode = True
    if line.strip() == "[ system ]": 
      for name, moleculetype, atomtypes, bondtypes in addParameters: top.append('#include "%s.itp"\n'%name)
    if itpMode and ITPs[-1][0] == None and line[0] != ";" and line.strip().replace("[", "").replace("]", "").strip() != "moleculetype": 
      ITPs[-1][0] = line.split()[0]
      if ITPs[-1][0] in renamer: 
        ITPs[-1][0] = renamer[ITPs[-1][0]]
        line = line.replace(line.split()[0], renamer[line.split()[0]])
      top.append('#include "%s.itp"\n'%ITPs[-1][0])
      
      if ITPs[-1][0] in posres:
        for fname, fc, DEFINE in posres[ITPs[-1][0]]: top.append("""#ifdef %s
#include "%s"
#endif

"""%(DEFINE, fname))

    if line.strip().replace("[", "").replace("]", "").strip() == "system": itpMode = False
    
    if moleculesMode and not line.strip().startswith(";"):
      if line.split()[0] in renamer: line = line.replace(line.split()[0], renamer[line.split()[0]])
      
    if line.strip().replace("[", "").replace("]", "").strip() == "molecules": moleculesMode = True
    
    if itpMode: ITPs[-1][1].append(line)
    else: 
      top.append(line)


  for name, lines in ITPs: 
    if name in posres:
      for i in posres[name]: genPosres(i, lines)

    open("../%s.itp"%name, "w").write("".join(lines))

  open("../merged.top", "w").write("".join(top))
  os.remove("temp.top")

  for name, moleculetype, atomtypes, bondtypes in addParameters: open("../%s.itp"%name, "w").write(moleculetype)

  output = []
  for line in open("parameters.itp"):
    if line.startswith("["): mode = line.strip().replace("[", "").replace("]", "").strip()
    if mode == "atomtypes":
      if not line.strip():
        for name, moleculetype, atomtypes, bondtypes in addParameters: output += atomtypes
    if mode == "bondtypes":
      if not line.strip():
        for name, moleculetype, atomtypes, bondtypes in addParameters: output += bondtypes
    output.append(line)
    
  open("parameters.itp", "w").write("".join(output))
  
  os.rename("parameters.itp", "../parameters.itp")
  os.rename("parmed.gro", "../parmed.gro")

