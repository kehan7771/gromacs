import os, sys, mdtraj, yieldxtc, numpy as np, scipy, scipy.ndimage, json, itertools
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology
import sklearn, sklearn.decomposition, sklearn.cluster

from adjustText import adjust_text

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

mpl.rcParams['xtick.major.size'] = 7
mpl.rcParams['ytick.major.size'] = 7
mpl.rcParams['xtick.major.width'] = 3
mpl.rcParams['ytick.major.width'] = 3
mpl.rcParams['axes.axisbelow'] = True
mpl.rc('font', **{'size': 16})

cwd = os.getcwd()+os.sep

import tools

def additionalArguments(optparser):
  optparser.add_argument("--stage1", action="store_true")
  optparser.add_argument("--stage2", action="store_true")
  optparser.add_argument("--stage3", action="store_true")
  optparser.add_argument("--stage4", action="store_true")
  optparser.add_argument("--stage5", action="store_true")
  
  optparser.add_argument("--temp", type=float, default=300, required=False)
  
  optparser.add_argument("--skip-q", action="store_true")
  optparser.add_argument("--grid", action="store_true")
  optparser.add_argument("--no-repick", action="store_true")

tools.setup(additionalArguments)

mdx = tools.RESTART["production-phase"]
Nbakapara = tools.config["fbmd"]["seeds"]

T = round(tools.cli_args.temp)
n_structs = tools.config["analysis"].get("N-rk", 1000)
random_state = 820248
R_cluster_cutoff = tools.config["analysis"].get("cluster", {}).get("R-cutoff", 0.7)

RT = 0.008314462175 * T / 4.184

maxPMF = tools.config["analysis"]["pca"]["max-pmf"]
nBins = tools.config["analysis"]["pca"].get("plot-nbins", 50)
upscaleN = tools.config["analysis"]["pca"].get("plot-upscale", 50)

pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname

diheMode = tools.config["analysis"]["pca"].get("dihedral")

def load_data(ndists):
  for seed in range(Nbakapara):
    if diheMode:
      yield np.load(tools.analFolder+"temp/%s%s.pcadihe.npy"%(seed, pname)), np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%seed, dtype=np.float32), str(seed)
    else: 
      dists = np.fromfile(tools.analFolder+"temp/%s%s.pcadist"%(seed, pname), dtype=np.float32)
      yield np.reshape(dists, (int(len(dists)/ndists), ndists)), np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%seed, dtype=np.float32), str(seed)


def stage1(): # do clustering
  nativeDists = np.array(json.load(open(tools.analFolder+"temp/native_dists%s.json"%pname)))
  ndists = len(nativeDists)

  pca = sklearn.decomposition.IncrementalPCA(n_components=100)
  if "reference-pca" in tools.config["analysis"]["pca"]: jso = json.load(open(tools.config["analysis"]["pca"]["reference-pca"]))
  else: jso = json.load(open(tools.analFolder+"temp/pca_sklearn%s.json"%pname))
  pca.components_ = np.array(jso["components_"])
  pca.explained_variance_ = np.array(jso["explained_variance_"])
  pca.explained_variance_ratio_ = np.array(jso["explained_variance_ratio_"])
  pca.singular_values_ = np.array(jso["singular_values_"])
  pca.mean_ = np.array(jso["mean_"])
  pca.n_components_ = np.array(jso["n_components_"])
  pca.noise_variance_ = np.array(jso["noise_variance_"])

  tmp = np.cumsum(pca.explained_variance_ratio_)
  Npcs = np.where(tmp < 0.9)[0][-1]+2
  if Npcs > 100: Npcs = 100
  print("Using %s principal components (variance: %.2f%%)"%(Npcs, tmp[Npcs-1]*100))

  bins, pdf_log = tools.getReweigingProb(T)

  pdf_min = bins[0]
  nob = len(pdf_log)-1
  pdf_binSize = bins[1]-bins[0]

  data = []
  lnP = []
  meta = []
  enebins = []
  
  totalN = 0
    
  for part, ene, seed in load_data(ndists):
    pcs = pca.transform(part).T[:Npcs].T
  
    bins = np.floor((ene-pdf_min)/pdf_binSize).astype(int)
    bins[bins < 0] = 0
    bins[bins > nob] = nob

    n = np.min([len(pcs), len(bins)])
    pcs = pcs[:n]
    bins = bins[:n]
    
    data.append(pcs)
    enebins.append(bins)
    
    for i in range(n): meta.append([seed, i])
    totalN += len(part)

  data = np.concatenate(data)
  enebins = np.concatenate(enebins)
  lnP = pdf_log[enebins]
  
  if tools.config["analysis"].get("kmeans-weighting"):
    jso = json.loads(open(tools.analFolder+"distributions/reweighting.json").read())
    A = np.array(jso["internal"]["bins"])
    B = np.array(jso["energy-bins"])
    
    same, i1, i2 = np.intersect1d(A,B, return_indices=True)
    if len(i2) != len(B):
      print("ERROR: Cannot use kmeans-weighting...")
      exit()
    weights = (T/tools.config["fbmd"]["initialTemp"])*np.array(jso["internal"]["gamma"])[i1][enebins]
    inv = weights > 1
    weights[inv] = 1/weights[inv]
    weights **= tools.config["analysis"].get("kmeans-weighting-component", 1)
  else: weights = None

  print("number of structures:", len(lnP), "total:", totalN)

  if tools.config["analysis"].get("full-kmeans"): kmeans = sklearn.cluster.KMeans(n_clusters=n_structs, verbose=True, n_init=10, random_state=random_state)
  else: 
    batch_size = int(len(lnP)*0.01)
    kmeans = sklearn.cluster.MiniBatchKMeans(n_clusters=n_structs, verbose=True, batch_size=batch_size, n_init=10, random_state=random_state, max_iter=300, max_no_improvement=100)
  labels = kmeans.fit_predict(data, sample_weight=weights)
  
  #labels = kmeans.labels_

  extract = []

  import scipy.special

  for i in range(n_structs):
    idxs = np.where(labels == i)[0]
    if len(idxs) == 0: continue
    
    if tools.config["analysis"].get("simple-cluster-avg"): 
      clusterAvg = kmeans.cluster_centers_[i]
      r = np.linalg.norm(data[idxs]-clusterAvg, axis=1)
      nearest = np.argmin(r)
    else:
      offset = np.min(data[idxs])-1
      clusterAvg = np.exp(np.array([np.logaddexp.reduce(np.sort(j)) for j in (np.log(data[idxs]-offset).T+lnP[idxs])])-np.logaddexp.reduce(np.sort(lnP[idxs])))+offset

      r = np.linalg.norm(data[idxs]-clusterAvg, axis=1)
      nearest = np.argmax(np.log(1./r)+lnP[idxs])

    nfo = meta[idxs[nearest]]
    extract.append([nfo[0], nfo[1], data[idxs][nearest], lnP[idxs][nearest], np.logaddexp.reduce(np.sort(lnP[idxs]))])
  
  ORDER = np.argsort([i[4] for i in extract])[::-1] # from big to small
  extract = [extract[i] for i in ORDER]
  
  labels2 = np.zeros_like(labels)
  for i in range(n_structs): labels2[labels == ORDER[i]] = i
  np.save(tools.analFolder+"temp/cluster_labels%s.npy"%pname, labels2) # reordered labels, based on population sorting
  
  output = []
  for idx, (seed, fidx, pc123, lnp, population) in enumerate(extract): output.append([idx, seed, fidx, pc123[0], pc123[1], pc123[2], lnp, population])
  if T == 300: open(tools.analFolder+"structures_pca_%s%s.json"%(n_structs, pname), "w").write(json.dumps(output))
  else: open(tools.analFolder+"structures_pca_%s_%s%s.json"%(n_structs, T, pname), "w").write(json.dumps(output))
  
def stage2(): # generate an xtc file of all the picked structures
  if T == 300: meta = json.loads(open(tools.analFolder+"structures_pca_%s%s.json"%(n_structs, pname)).read())
  else: meta = json.loads(open(tools.analFolder+"structures_pca_%s_%s%s.json"%(n_structs, T, pname)).read())
  
  info = {}
  
  for idx, seed, fidx, pc1, pc2, pc3, lnp, population in meta:
    if not seed in info: info[seed] = {}
    info[seed][int(fidx)] = idx
  
  
  struct = tools.load(tools.analFolder+"ref.gro")
  structs = mdtraj.Trajectory(np.zeros((len(meta), len(struct.xyz[0]), 3)), struct.top, unitcell_lengths=[struct.unitcell_lengths[0] for i in range(len(meta))], unitcell_angles=[struct.unitcell_angles[0] for i in range(len(meta))])
  
  for seed, v in info.items():
    fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed)
  
    tooutput = sorted(v.keys())
    next = tooutput.pop(0)
    for i, (xyz, time, step, box) in enumerate(fp.readyield()):
      if i != next: continue
      structs.xyz[v[next]] = xyz
      print(v[next])
      if len(tooutput) == 0: break # done
      next = tooutput.pop(0)
    fp.close()
    
  if T == 300: structs.save(tools.analFolder+"structures_pca_%s%s.xtc"%(n_structs, pname))
  else: structs.save(tools.analFolder+"structures_pca_%s_%s%s.xtc"%(n_structs, T, pname))
  
  
  
BETA_CONST = 50  # 1/nm
LAMBDA_CONST = 1.8
NATIVE_CUTOFF = 0.45  # nanometers

def stage3():
  if T == 300: base = tools.analFolder+"structures_pca_%s%s"%(n_structs, pname)
  else: base = tools.analFolder+"structures_pca_%s_%s%s"%(n_structs, T, pname)
  structs = tools.load(base+".xtc", top=tools.analFolder+"ref.gro")
  
  tools.initStructure()
  outputIndexes = tools.outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms

  dockingMode = "ligand" in tools.config

  if tools.config["analysis"].get("Rcluster-manual-pairs"): heavy_pairs = __import__(tools.config["analysis"]["Rcluster-manual-pairs"]).get_Rcluster_pairs(tools)
  elif dockingMode:
    proteinHeavy = outputIndexes.receptorAtoms
    ligandHeavy = outputIndexes.ligandAtoms
    heavy_pairs = np.array(list(itertools.product(proteinHeavy, ligandHeavy)))
  
    if tools.config["analysis"].get("Rcluster-ligand-packing"): heavy_pairs = np.array(heavy_pairs.tolist() + [(i,j) for (i,j) in itertools.combinations(ligandHeavy, 2) if abs(structs.top.atom(i).residue.index - structs.top.atom(j).residue.index) > 3])
  else:
    if tools.config["analysis"].get("cyclic"): heavy_pairs = tools.buildBondList()
    else:
      proteinHeavy = outputIndexes.receptorAtoms
      heavy_pairs = np.array([(i,j) for (i,j) in itertools.combinations(proteinHeavy, 2) if abs(structs.top.atom(i).residue.index - structs.top.atom(j).residue.index) > 3])

  N = len(structs)
  Rinfo = np.zeros((N, N), dtype=np.float32)
  Nc = np.zeros(N, dtype=np.int32)

  for i, struc in enumerate(structs):
    heavy_pairs_distances = mdtraj.compute_distances(struc[0], heavy_pairs)[0]
    contacts = heavy_pairs[heavy_pairs_distances < NATIVE_CUTOFF]
    Nc[i] = len(contacts)
    if len(contacts) < 1: 
      Rinfo[i][i] = 1
      continue
    r0 = mdtraj.compute_distances(struc[0], contacts)
    Rinfo[i] = np.mean(1.0 / (1 + np.exp(BETA_CONST * (mdtraj.compute_distances(structs, contacts) - LAMBDA_CONST * r0))), axis=1)
    Rinfo[i][i] = 1
    
  Rinfo[np.isnan(Rinfo)] = 0
  
  for i in range(N):
    for j in range(i+1, N): 
      tmp = ((Rinfo[i][j]*Nc[i])+(Rinfo[j][i]*Nc[j]))/(Nc[i]+Nc[j])
      if np.isnan(tmp): tmp = 0
      Rinfo[i][j] = Rinfo[j][i] = tmp

  assigned = {}
  for i in range(N):
    if i in assigned: continue
    ctr = 0
    for j in np.where(Rinfo[i] > R_cluster_cutoff)[0]: 
      if not j in assigned: 
        assigned[j] = i
        ctr += 1

  labels = np.array([assigned[i] for i in range(N)])
  np.save(tools.analFolder+"temp/cluster_label_merge%s.npy"%pname, labels) # cluster merges
  
  stage3_repick(labels)
  
def stage3_repick(labels2):
  labels = np.load(tools.analFolder+"temp/cluster_labels%s.npy"%pname)
  
  for oldLabel, newLabel in enumerate(labels2): 
    if newLabel != oldLabel: labels[np.where(labels == oldLabel)[0]] = newLabel
  
  clusters = np.unique(labels)

  nativeDists = np.array(json.load(open(tools.analFolder+"temp/native_dists%s.json"%pname)))
  ndists = len(nativeDists)

  pca = sklearn.decomposition.IncrementalPCA(n_components=100)
  if "reference-pca" in tools.config["analysis"]["pca"]: jso = json.load(open(tools.config["analysis"]["pca"]["reference-pca"]))
  else: jso = json.load(open(tools.analFolder+"temp/pca_sklearn%s.json"%pname))
  pca.components_ = np.array(jso["components_"])
  pca.explained_variance_ = np.array(jso["explained_variance_"])
  pca.explained_variance_ratio_ = np.array(jso["explained_variance_ratio_"])
  pca.singular_values_ = np.array(jso["singular_values_"])
  pca.mean_ = np.array(jso["mean_"])
  pca.n_components_ = np.array(jso["n_components_"])
  pca.noise_variance_ = np.array(jso["noise_variance_"])

  tmp = np.cumsum(pca.explained_variance_ratio_)
  Npcs = np.where(tmp < 0.9)[0][-1]+2
  if Npcs > 100: Npcs = 100

  bins, pdf_log = tools.getReweigingProb(T)

  pdf_min = bins[0]
  nob = len(pdf_log)-1
  pdf_binSize = bins[1]-bins[0]
  data = []
  lnP = []
  meta = []
  for part, ene, seed in load_data(ndists):
    pcs = pca.transform(part).T[:Npcs]
    bins = np.floor((ene-pdf_min)/pdf_binSize).astype(int)
    bins[bins < 0] = 0
    bins[bins > nob] = nob
    data.append(pcs.T)
    lnP.append(pdf_log[bins])
    for i in range(len(pcs[0])): meta.append([seed, i])
  data = np.concatenate(data)
  lnP = np.concatenate(lnP)

  if tools.cli_args.no_repick:
    if T == 300: metadata = json.loads(open(tools.analFolder+"structures_pca_%s%s.json"%(n_structs, pname)).read())
    else: metadata = json.loads(open(tools.analFolder+"structures_pca_%s_%s%s.json"%(n_structs, T, pname)).read())

  extract, extract2 = [], []
  for cid in clusters:
    idxs = np.where(labels == cid)[0]
    if len(idxs) == 0: continue
  
    #clusterAvg = np.mean(data[idxs], axis=0)
    # weighted cluster center
    
    if not tools.cli_args.no_repick:
      offset = np.min(data[idxs])-1
      clusterAvg = np.exp(np.array([np.logaddexp.reduce(np.sort(j)) for j in (np.log(data[idxs]-offset).T+lnP[idxs])])-np.logaddexp.reduce(np.sort(lnP[idxs])))+offset
    
      r = np.linalg.norm(data[idxs]-clusterAvg, axis=1)
      nearest = np.argmax(np.log(1./r)+lnP[idxs])
      nfo = meta[idxs[nearest]]
      extract.append([nfo[0], nfo[1], data[idxs][nearest], lnP[idxs][nearest], np.logaddexp.reduce(np.sort(lnP[idxs]))])
      extract2.append([np.logaddexp.reduce(np.sort(lnP[idxs])), cid])
    else:
      nfo = metadata[np.where(labels2 == cid)[0][0]]
      extract.append([nfo[0], nfo[1], [nfo[2], nfo[3], nfo[4]], nfo[5], np.logaddexp.reduce(np.sort(lnP[idxs]))])
      extract2.append([np.logaddexp.reduce(np.sort(lnP[idxs])), cid])

  extract = [extract[i] for i in np.argsort([i[4] for i in extract])[::-1]]
  
  output = []
  for idx, (seed, fidx, pc123, lnp, population) in enumerate(extract): output.append([idx, seed, fidx, pc123[0], pc123[1], pc123[2], lnp, population])

  if T == 300: open(tools.analFolder+"structures_pca_repick_%s%s.json"%(n_structs, pname), "w").write(json.dumps(output))
  else: open(tools.analFolder+"structures_pca_repick_%s_%s%s.json"%(n_structs, T, pname), "w").write(json.dumps(output))
  
  conv = np.full(np.max(clusters)+1, fill_value=-1, dtype=int)
  for rank, i in enumerate(np.argsort([i[0] for i in extract2])[::-1]): conv[extract2[i][1]] = rank
  labels_renum = conv[labels]
  np.save(tools.analFolder+"temp/rk_clusters%s.npy"%pname, labels_renum)
  
  
def stage4():
  if T == 300: meta = json.loads(open(tools.analFolder+"structures_pca_repick_%s%s.json"%(n_structs, pname)).read())
  else: meta = json.loads(open(tools.analFolder+"structures_pca_repick_%s_%s%s.json"%(n_structs, T, pname)).read())
  
  info = {}
  
  for idx, seed, fidx, pc1, pc2, pc3, lnp, population in meta:
    if not seed in info: info[seed] = {}
    info[seed][int(fidx)] = idx

  struct = tools.load(tools.analFolder+"ref.gro")
  structs = mdtraj.Trajectory(np.zeros((len(meta), len(struct.xyz[0]), 3)), struct.top, unitcell_lengths=[struct.unitcell_lengths[0] for i in range(len(meta))], unitcell_angles=[struct.unitcell_angles[0] for i in range(len(meta))])
  
  for seed, v in info.items():
    fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed)
  
    tooutput = sorted(v.keys())
    next = tooutput.pop(0)
    for i, (xyz, time, step, box) in enumerate(fp.readyield()):
      if i != next: continue
      structs.xyz[v[next]] = xyz
      print(v[next])
      if len(tooutput) == 0: break # done
      next = tooutput.pop(0)
    fp.close()

  if T == 300: structs.save(tools.analFolder+"structures_pca_final_%s%s.xtc"%(n_structs, pname))
  else: structs.save(tools.analFolder+"structures_pca_final_%s_%s%s.xtc"%(n_structs, T, pname))
  
def stage5(): # plot
  PC1_dots, PC2_dots = [], []
  PC1_dotsQ, PC2_dotsQ = [], []
  if T == 300: fp = tools.analFolder+"structures_pca_final_%s%s.tsv"%(n_structs, pname)
  else: fp = tools.analFolder+"structures_pca_final_%s_%s%s.tsv"%(n_structs, T, pname)
  if not os.path.exists(fp):
    print("no tsv file")
    exit()
  for line in open(fp).readlines():
    if line.startswith("struc. id"): continue
    strucid, cfe, lnpop, pop, popp, pc1, pc2, pc3, pmf, rasa, rnative, rsmd, lmbd = line.split()
    if strucid[0] == "r":
      PC1_dots.append(float(pc1))
      PC2_dots.append(float(pc2))
    elif strucid[0] == "q" and not tools.cli_args.skip_q:
      PC1_dotsQ.append(float(pc1))
      PC2_dotsQ.append(float(pc2))

  PC1_dots = np.array(PC1_dots)
  PC2_dots = np.array(PC2_dots)
  PC1_dotsQ = np.array(PC1_dotsQ)
  PC2_dotsQ = np.array(PC2_dotsQ)

  RT = 0.008314462175 * T / 4.184
  
  nativeDists = np.array(json.load(open(tools.analFolder+"temp/native_dists%s.json"%pname)))
  ndists = len(nativeDists)

  if T == 300: jso = json.loads(open(tools.analFolder+"temp/pca_fel2%s.json"%pname).read())
  else: jso = json.loads(open(tools.analFolder+"temp/pca_fel2_%s%s.json"%(T, pname)).read())
  mn1 = jso["mn1"]
  mn2 = jso["mn2"]
  bs1 = jso["bs1"]
  bs2 = jso["bs2"]
  PMF = np.array(jso["PMF"])
  PMF_upper = jso["PMF_upper"]

  #np.set_printoptions(threshold=np.nan)

  nBinsX, nBinsY = np.shape(PMF)

  x1 = mn1 - bs1
  x2 = x1 + ((nBinsX+2)*bs1)
  y1 = mn2 - bs2
  y2 = y1 + ((nBinsY+2)*bs2)

  plt.xlabel("PC1")
  plt.ylabel("PC2")

  X, Y = np.meshgrid(np.array([(i*bs1)+mn1 for i in range(nBinsX)]), np.array([(j*bs2)+mn2 for j in range(nBinsY)]))

  new_bs1 = bs1/upscaleN
  new_bs2 = bs2/upscaleN
  
  PMF[PMF > maxPMF+1] = maxPMF+1
  new_PMF = scipy.ndimage.zoom(PMF, upscaleN, order=1)
  new_PMF = scipy.ndimage.gaussian_filter(new_PMF, upscaleN*.25)
  new_PMF -= np.min(new_PMF)
  new_X = scipy.ndimage.zoom(X, upscaleN, order=1)
  new_Y = scipy.ndimage.zoom(Y, upscaleN, order=1)
  
  badbad = new_PMF > maxPMF
  new_PMF[badbad] = np.nan
  CS = plt.pcolormesh(new_X, new_Y, np.ma.masked_invalid(new_PMF.T), cmap="Greys", shading='gouraud')
  plt.clim(0, maxPMF)
  cbar = plt.colorbar(CS)
  cbar.set_label("Free energy (kcal/mol)")
  plt.axis((x1,x2,y1,y2))
  plt.scatter(PC1_dots, PC2_dots, c='r', marker='o', s=4, linewidth=0.25)
  plt.scatter(PC1_dotsQ, PC2_dotsQ, c='b', marker='o', s=4, linewidth=0.25)
  
  mean_x = np.mean(new_X[~badbad.T])
  std_x = np.std(new_X[~badbad.T])
  mean_y = np.mean(new_Y[~badbad.T])
  std_y = np.std(new_Y[~badbad.T])
  x1 = mean_x - std_x*3
  x2 = mean_x + std_x*3
  y1 = mean_y - std_y*3
  y2 = mean_y + std_y*3
  
  pc1range = tools.config["analysis"]["pca"].get("pc1-range", [x1, x2])
  pc2range = tools.config["analysis"]["pca"].get("pc2-range", [y1, y2])
  plt.axis((pc1range[0], pc1range[1], pc2range[0], pc2range[1]))
  
  rk_font = {"color": "red", "size": 12}
  qk_font = {"color": "blue", "size": 12}
  arrowprops = []
  rk_texts = []
  for i, txt in enumerate(PC1_dots): 
    txt = plt.text(PC1_dots[i], PC2_dots[i], str(i+1), fontdict=rk_font)
    txt.set_bbox(dict(boxstyle="circle,pad=0.1", facecolor='white', alpha=0.75, edgecolor='none'))
    rk_texts.append(txt)
    arrowprops.append(dict(shrinkA=0, shrinkB=1.75, arrowstyle='-', color='red'))
  for i, txt in enumerate(PC1_dotsQ): 
    txt = plt.text(PC1_dotsQ[i], PC2_dotsQ[i], str(i+1), fontdict=qk_font)
    txt.set_bbox(dict(boxstyle="circle,pad=0.1", facecolor='white', alpha=0.75, edgecolor='none'))
    rk_texts.append(txt)
    arrowprops.append(dict(shrinkA=0, shrinkB=1.75, arrowstyle='-', color='blue'))
  adjust_text(rk_texts, expand_text=(1.2, 1.4), expand_points=(1.2, 1.4), force_text=(1.0, 1.0), force_points=(1.0, 1.0), arrowprops=arrowprops)

  plt.minorticks_on()
  if tools.cli_args.grid:
    plt.grid(which="major", linestyle='solid')
    plt.grid(which="minor", linestyle='dotted')

  if T == 300: plt.savefig(tools.analFolder+"fel_dots_final%s.png"%pname, dpi=600, bbox_inches='tight')
  else: plt.savefig(tools.analFolder+"fel_dots_final_%s%s.png"%(T, pname), dpi=600, bbox_inches='tight')
  plt.close()

if tools.cli_args.stage1: stage1()
if tools.cli_args.stage2: stage2()
if tools.cli_args.stage3: stage3()
if tools.cli_args.stage4: stage4()
if tools.cli_args.stage5: stage5()
