import numpy as np, mdtraj, scipy, scipy.ndimage, itertools, sys, json, os
import sklearn, sklearn.decomposition

import tools

tools.setup()

nBins = tools.config["analysis"]["pca"].get("plot-nbins", 50)
upscaleN = tools.config["analysis"]["pca"].get("plot-upscale", 50)
maxPMF = tools.config["analysis"]["pca"]["max-pmf"]
maxPMF_pick = tools.config["analysis"]["pca"].get("max-pmf-pick", maxPMF*.5)
n_structs = tools.config["analysis"].get("N-rk", 1000)

pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname

pathMode = True
try:
  path = np.array([[float(j) for j in i.split(",")] for i in open(tools.CWD+tools.config["fbmd"]["restraints"]["path-file"]).readlines()[1:]])
  vec = path[1]-path[0]
  vec /= np.linalg.norm(vec)
except: pathMode = False
  
pca = sklearn.decomposition.IncrementalPCA(n_components=10)
if "reference-pca" in tools.config["analysis"]["pca"]: jso = json.load(open(tools.config["analysis"]["pca"]["reference-pca"]))
else: jso = json.load(open(tools.analFolder+"temp/pca_sklearn%s.json"%pname))
pca.components_ = np.array(jso["components_"])
pca.explained_variance_ = np.array(jso["explained_variance_"])
pca.explained_variance_ratio_ = np.array(jso["explained_variance_ratio_"])
pca.singular_values_ = np.array(jso["singular_values_"])
pca.mean_ = np.array(jso["mean_"])
pca.n_components_ = np.array(jso["n_components_"])
pca.noise_variance_ = np.array(jso["noise_variance_"])

jso = json.loads(open(tools.analFolder+"temp/pca_fel2%s.json"%pname).read())
mn1 = jso["mn1"]
mn2 = jso["mn2"]
bs1 = jso["bs1"]
bs2 = jso["bs2"]
PMF = np.array(jso["PMF"])
PMF_upper = jso["PMF_upper"]


nBinsX, nBinsY = np.shape(PMF)

x1 = mn1 - bs1
x2 = x1 + ((nBinsX+2)*bs1)
y1 = mn2 - bs2
y2 = y1 + ((nBinsY+2)*bs2)

X, Y = np.meshgrid(np.array([(i*bs1)+mn1 for i in range(nBinsX)]), np.array([(j*bs2)+mn2 for j in range(nBinsY)]))

new_bs1 = bs1/upscaleN
new_bs2 = bs2/upscaleN

wasjunk = PMF == np.inf
wasjunk_new = scipy.ndimage.zoom(wasjunk.astype(np.float32), upscaleN, order=1)
wasjunk_new = scipy.ndimage.gaussian_filter(wasjunk_new, upscaleN*.25)
  
PMF[wasjunk] = np.max(PMF[~wasjunk])+1
new_PMF = scipy.ndimage.zoom(PMF, upscaleN, order=1)
new_PMF = scipy.ndimage.gaussian_filter(new_PMF, upscaleN*.25)
new_PMF -= np.min(new_PMF)
new_PMF[new_PMF >= np.mean(new_PMF[wasjunk_new >= 0.25])] = np.inf

try: native = tools.load(tools.CWD+tools.config["analysis"]["ref-struc"])
except: native = tools.load(tools.CWD+tools.config["fbmd"]["production"]["ref-struc"])
index = tools.loadIndex(tools.CWD+tools.config["base"]["index"])


refgro = tools.load(tools.analFolder+"ref.gro")
try: native.superpose(refgro, 0, native.top.select("protein and backbone"), refgro.top.select("protein and backbone"))
except: pass

outputIndexes = tools.outputIndexes()
indicesSI = outputIndexes.receptorCalpha

foldingMode = not "ligand" in tools.config

try: heavy = native.top.select("symbol != H")
except: heavy = native.top.select("mass > 2")
proteinHeavy = np.intersect1d(tools.getReceptorList(), heavy)

if not foldingMode:
  if "names" in tools.config["ligand"]: ligandIdxs = index[pname[1:]]
  else: ligandIdxs = index[tools.config["ligand"]["name"]]
  ligandHeavy = np.intersect1d(ligandIdxs, heavy)
  keep = np.sort(np.concatenate([proteinHeavy, ligandHeavy]))
  ligandMass = np.array([native.top.atom(i).element.mass for i in ligandIdxs])
  if pathMode: nativeLambda = np.dot(np.average(native.xyz[0,ligandIdxs], weights=ligandMass, axis=0)-path[0], vec)*10
  else: nativeLambda = 0
else:
  keep = np.sort(proteinHeavy)
  nativeLambda = 0
native.atom_slice(keep, inplace=True)

proteinHeavy = np.array([np.where(i == keep)[0][0] for i in proteinHeavy])
if not foldingMode: ligandHeavy = np.array([np.where(i == keep)[0][0] for i in ligandHeavy])

if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  print(len(indicesSI), "atoms selected for superposition")

BETA_CONST = 50  # 1/nm
LAMBDA_CONST = 1.8
NATIVE_CUTOFF = 0.45  # nanometers

if foldingMode:
  pairs = np.array([[i,j] for [i,j] in itertools.combinations(proteinHeavy, 2) if np.abs(native.top.atom(i).residue.index-native.top.atom(j).residue.index) > 3])
  refCoords = native.xyz[:,proteinHeavy]
else:
  pairs = np.array(list(itertools.product(proteinHeavy, ligandHeavy)))
  refCoords = native.xyz[:,ligandHeavy]
native_contacts = pairs[mdtraj.compute_distances(native[0], pairs)[0] < NATIVE_CUTOFF]
r0 = mdtraj.compute_distances(native[0], native_contacts, periodic=(not tools.config["analysis"].get("no-periodic-ref-struc")))

if "gomi-native" in tools.config["analysis"]:
  gomi = tools.load(tools.analFolder+tools.config["analysis"]["gomi-native"])
  while True:
    gomi.superpose(native, 0, indicesSI)
    dx = np.linalg.norm(gomi.xyz[0,indicesSI]-native.xyz[0,indicesSI], axis=1)
    rmsd = np.sqrt(np.mean(np.square(dx)))
    worst = np.argmax(dx)
    if dx[worst] < 0.2: break
    indicesSI = np.delete(indicesSI, [worst])
  if foldingMode: refCoords = gomi.xyz[:,proteinHeavy]
  else: refCoords = gomi.xyz[:,ligandHeavy]
  native_contacts = pairs[mdtraj.compute_distances(gomi[0], pairs)[0] < NATIVE_CUTOFF]
  r0 = mdtraj.compute_distances(gomi[0], native_contacts)
  native = gomi

# also output for native...
# cluster energy, PC1, PC2, PC3, PCA free energy, RASA, R(native), RMSD

output = ["struc. id\tCluster free energy\tln population\tPopulation\tPopulation %\tPC1\tPC2\tPC3\tPCA free energy\tRASA\tR(native)-value\tRMSD\tLambda"]


struc_data = json.load(open(tools.analFolder+"structures_pca_repick_%s%s.json"%(n_structs, pname)))
tot_ln_pop = np.logaddexp.reduce(np.sort([ln_pop for rk, seed, sidx, pc1, pc2, pc3, lnp, ln_pop in struc_data]))

RT = 0.008314462175 * 300 / 4.184

for rk, seed, sidx, pc1, pc2, pc3, lnp, ln_pop in struc_data:
  fname = f"r{rk+1}.gro"
  population = np.exp(ln_pop)
  pop_perc = np.exp(ln_pop-tot_ln_pop)*100
  pmf = (ln_pop-struc_data[0][7])*-RT
  
  if pmf > maxPMF_pick: continue
  
  n1 = np.floor((pc1-mn1)/new_bs1).astype(int)
  n2 = np.floor((pc2-mn2)/new_bs2).astype(int)
  
  struc = tools.load(tools.analFolder+"structures_pca_final_%s_raw%s/%s"%(n_structs, pname, fname))
  if pathMode: lmbd = np.dot(np.average(struc.xyz[0,ligandIdxs], weights=ligandMass, axis=0)-path[0], vec)*10
  else: lmbd = 0
  struc = struc.atom_slice(keep, inplace=True)
  
  r = mdtraj.compute_distances(struc, native_contacts)
  Rnative = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))), axis=1)[0]
  
  struc.superpose(native, 0, indicesSI)
  
  if foldingMode:
    rmsd = np.sqrt(3*np.mean(np.square(struc.xyz[:,proteinHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
    try: sasa = np.sum(mdtraj.shrake_rupley(struc)[0][proteinHeavy])
    except: sasa = 0.0
    RASA = sasa
  else: 
    rmsd = np.sqrt(3*np.mean(np.square(struc.xyz[:,ligandHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
    try: sasa = np.sum(mdtraj.shrake_rupley(struc)[0][ligandHeavy])
    except: sasa = 0.0
    struc.atom_slice(ligandHeavy, inplace=True)
    
    try: sasa_full = np.sum(mdtraj.shrake_rupley(struc)[0])
    except: sasa_full = 0.0
    try: RASA = sasa/sasa_full
    except: RASA = 0.0
  
  output.append("%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.3f\t%.2f\t%.2f"%(fname.split(".gro")[0], pmf, ln_pop, population, pop_perc, pc1, pc2, pc3, new_PMF[n1][n2], RASA, Rnative, rmsd, lmbd))

if os.path.exists(tools.analFolder+"stability2/r1/repr.gro"):
  tools.initStructure()
  forpca = tools.outputIndexes().raw
  
  tmp = []
  for rx in os.listdir(tools.analFolder+"stability2"):
    if not rx.startswith("r"): continue
    if not os.path.isdir(tools.analFolder+"stability2/"+rx): continue
    tmp.append(int(rx[1:]))
  
  for rx in sorted(tmp):
    rx = "r%s"%rx
    struc = tools.load(tools.analFolder+"stability2/%s/repr.gro"%rx)
    if pathMode: lmbd = np.dot(np.average(struc.xyz[0,ligandIdxs], weights=ligandMass, axis=0)-path[0], vec)*10
    else: lmbd = 0
    pcastruc = struc.atom_slice(forpca, inplace=False)
    
    #pcastruc.save_gro("test_%s.gro"%i)
    struc = struc.atom_slice(keep, inplace=True)
    
    if rx == "r1": pairs = tools.pca_distance_pairs(pcastruc)
    distances = mdtraj.compute_distances(pcastruc, pairs)[0].astype(np.float32)*10 # nm -> A
    pcProj = pca.transform([distances])[0]
    
    pc1 = pcProj[0]
    pc2 = pcProj[1]
    pc3 = pcProj[2]

    n1 = np.floor((pc1-mn1)/new_bs1).astype(int)
    n2 = np.floor((pc2-mn2)/new_bs2).astype(int)

    r = mdtraj.compute_distances(struc, native_contacts)
    Rnative = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))), axis=1)[0]
    
    struc.superpose(native, 0, indicesSI)
    
    if foldingMode:
      rmsd = np.sqrt(3*np.mean(np.square(struc.xyz[:,proteinHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
      try: sasa = np.sum(mdtraj.shrake_rupley(struc)[0][proteinHeavy])
      except: sasa = 0.0
      RASA = sasa
    else: 
      rmsd = np.sqrt(3*np.mean(np.square(struc.xyz[:,ligandHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
      try: sasa = np.sum(mdtraj.shrake_rupley(struc)[0][ligandHeavy])
      except: sasa = 0.0
      struc.atom_slice(ligandHeavy, inplace=True)
      
      try: sasa_full = np.sum(mdtraj.shrake_rupley(struc)[0])
      except: sasa_full = 0.0
      try: RASA = sasa/sasa_full
      except: RASA = 0.0
  
    try: pmf = new_PMF[n1][n2]
    except: pmf = -1
  
    output.append("q%s\t-\t-\t-\t-\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.3f\t%.2f\t%.2f"%(rx[1:], pc1, pc2, pc3, pmf, RASA, Rnative, rmsd, lmbd))
  
  
if "gomi-native" in tools.config["analysis"]:
  pairs = np.load(tools.analFolder+"temp/pcapairs%s.npy"%pname)
  nativeDists = mdtraj.compute_distances(native, pairs)[0].astype(np.float32)*10 # nm -> A
else: nativeDists = np.array(json.load(open(tools.analFolder+"temp/native_dists%s.json"%pname)))

nativePCs = pca.transform([nativeDists])[0]
  
pc1 = nativePCs[0]
pc2 = nativePCs[1]
pc3 = nativePCs[2]
n1 = np.floor((pc1-mn1)/new_bs1).astype(int)
n2 = np.floor((pc2-mn2)/new_bs2).astype(int)

if "gomi-native" in tools.config["analysis"]:
  if foldingMode:
    rmsd = np.sqrt(3*np.mean(np.square(native.xyz[:,proteinHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
    try: sasa = np.sum(mdtraj.shrake_rupley(native)[0][proteinHeavy])
    except: sasa = 0.0
    native.atom_slice(proteinHeavy, inplace=True)
    RASA = sasa
  else:
    sasa = np.sum(mdtraj.shrake_rupley(gomi)[0][ligandHeavy])
    gomi.atom_slice(ligandHeavy, inplace=True)
    sasa_full = np.sum(mdtraj.shrake_rupley(gomi)[0])
    RASA = sasa/sasa_full
else:
  if foldingMode:
    rmsd = np.sqrt(3*np.mean(np.square(native.xyz[:,proteinHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
    try: sasa = np.sum(mdtraj.shrake_rupley(native)[0][proteinHeavy])
    except: sasa = 0.0
    native.atom_slice(proteinHeavy, inplace=True)
    RASA = sasa
  else: 
    rmsd = np.sqrt(3*np.mean(np.square(native.xyz[:,ligandHeavy]-refCoords),axis=(1,2)))[0]*10 # nm->A
    try: sasa = np.sum(mdtraj.shrake_rupley(native)[0][ligandHeavy])
    except: sasa = 0.0
    native.atom_slice(ligandHeavy, inplace=True)
    try: sasa_full = np.sum(mdtraj.shrake_rupley(native)[0])
    except: sasa_full = 0
    try: RASA = sasa/sasa_full
    except: RASA = 0

try: saa = new_PMF[n1][n2]
except: saa = np.inf
output.append("native\t-\t-\t-\t-\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.3f\t%.2f\t%.2f"%(pc1, pc2, pc3, saa, RASA, 1.0, 0.0, nativeLambda))

open(tools.analFolder+"structures_pca_final_%s%s.tsv"%(n_structs, pname), "w").write("\n".join(output))
