import os, numpy as np, mdtraj, json, itertools, yieldxtc, sklearn, sklearn.cluster, shutil, sys, scipy, scipy.stats
from collections import OrderedDict

mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology

import tools

def additionalArguments(optparser):
  optparser.add_argument("--debug", action="store_true")
  optparser.add_argument("--restart", action="store_true")
  optparser.add_argument("--rid", type=int, required=False)

tools.setup(additionalArguments)

debug = tools.cli_args.debug
restart = tools.cli_args.restart
RID = tools.cli_args.rid

class Logger(object):
  def __init__(self, fname, mode):
    self.terminal = sys.stdout
    self.log = open(fname, mode)

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)
    self.terminal.flush()
    self.log.flush()
    
class fauxLogger(object):
  def __init__(self, fname=None, mode=None):
    self.terminal = sys.stdout
  
  def write(self, message): 
    self.terminal.write(message)
    self.terminal.flush()

Rcutoff = tools.config["pathing"].get("r-cutoff-initial", 0.9)
RMSDcutoff = tools.config["pathing"].get("rmsd-cutoff-initial", 0.5) * .1 # A -> nm

Rcutoff_final = tools.config["pathing"].get("r-cutoff-final", 0.8)
RMSDcutoff_final = tools.config["pathing"].get("rmsd-cutoff-final", 3.5) * .1 # A -> nm

# maybe we also have to use Rcluster-ligand-packing...

initial_structs_per_window = tools.config["pathing"].get("structures-per-window", 3)

minimalContactsR = tools.config["pathing"].get("r-minimal-contacts", 25)
kmeans_random_state = tools.config["pathing"].get("kmeans-random-number", 87324)
simplifiedRmode = tools.config["pathing"].get("simplified-R-mode", False)
minNumberPerCluster = tools.config["pathing"].get("min-structs-per-cluster", 1)
COM_cutoff = tools.config["pathing"].get("COM-cutoff", 5.0) * 0.1
n_structs = tools.config["analysis"].get("N-rk", 1000)

BETA_CONST = 50  # 1/nm
LAMBDA_CONST = 1.8
NATIVE_CUTOFF = 0.45  # nanometers

Nbakapara = tools.config["fbmd"]["seeds"]

windowCenters = np.array([float(i) for i in tools.config["pathing"]["window-centers"]])

if tools.config["pathing"].get("upscale"):
  upscale = initial_structs_per_window
  initial_structs_per_window = 1
  windowCenters2 = []
  for idx, lmbd1 in enumerate(windowCenters):
    lmbd0 = lmbd2 = None
    if idx != 0: lmbd0 = windowCenters[idx-1]
    if idx != len(windowCenters)-1: lmbd2 = windowCenters[idx+1]
    if idx == 0: lmbd0 = lmbd1-(lmbd2-lmbd1)
    if idx == len(windowCenters)-1: lmbd2 = lmbd1+(lmbd1-lmbd0)
    dlmbd = ((lmbd2-lmbd0)/upscale)*.5 # window size
    start = lmbd0*(1./3) + lmbd1*(2./3) # start pos
    for i in range(upscale): windowCenters2.append(start + i*dlmbd)
  windowCenters = np.array(windowCenters2)

windowRanges = np.array([[0.0, 0.0] for i in windowCenters])
for i in range(1, len(windowCenters)-1):
  windowRanges[i][0] = (windowCenters[i-1]+windowCenters[i])*.5
  windowRanges[i][1] = (windowCenters[i+1]+windowCenters[i])*.5
windowRanges[0][1] = (windowCenters[1]+windowCenters[0])*.5
windowRanges[0][0] = windowRanges[0][1]-(windowRanges[1][1]-windowRanges[1][0])
windowRanges[-1][0] = (windowCenters[-2]+windowCenters[-1])*.5
windowRanges[-1][1] = windowRanges[-1][0] + (windowRanges[-2][1]-windowRanges[-2][0])

tools.initStructure()
index = tools.index
outputIndexes = tools.outputIndexes()

ligAtoms = outputIndexes.ligandAtoms
proteinAtoms = outputIndexes.receptorAtoms

ref = tools.load(tools.analFolder+"ref.gro")

initialStructures = []
indicesSI = outputIndexes.receptorCalpha

id = tools.config["pathing"].get("id")
if RID != None: id = RID
outputDir = tools.analFolder+"lambda_picked%s/"%(id and "_"+id or "")
if os.path.exists(outputDir) and not (debug or restart):
  print("output directory already exists...\n")
  exit()

if not os.path.exists(outputDir):
  os.mkdir(outputDir)
  os.mkdir(outputDir+"repr/")
  os.mkdir(outputDir+"cleaned/")
  os.mkdir(outputDir+"cleaned/repr/")

if debug: logger = fauxLogger()
else: logger = Logger(outputDir+"out.log", "a")

struc = tools.baseStruct.atom_slice(outputIndexes.raw, inplace=False)
otherAtoms = np.array([i for i in range(len(struc.xyz[0])) if not i in ligAtoms])

baseStruct = tools.load(tools.CWD+tools.config["base"]["pdb"])

if tools.config["pathing"]["initial-structure"].startswith("/"): initialStructurePath = tools.config["pathing"]["initial-structure"]
else: initialStructurePath = tools.analFolder + tools.config["pathing"]["initial-structure"]
if RID: initialStructurePath = tools.analFolder + "structures_pca_final_%s_raw/%s.gro"%(n_structs, RID)

initialStruc = tools.load(initialStructurePath)
initialStruc.atom_slice(outputIndexes.raw, inplace=True)
masses = np.array([initialStruc.top.atom(a).element.mass for a in ligAtoms])

if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  logger.write("%s atoms selected for superposition\n"%(len(indicesSI),))
else:
  tmpsel = np.array(indicesSI.tolist())
  while True:
    ref.superpose(initialStruc, 0, tmpsel)
    dx = np.linalg.norm(ref.xyz[0,indicesSI]-initialStruc.xyz[0,indicesSI], axis=1)
    tmpsel2 = indicesSI[dx < 0.2]
    if len(tmpsel) == len(tmpsel2) and np.array_equal(tmpsel, tmpsel2): break
    tmpsel = tmpsel2
  indicesSI = tmpsel
  logger.write("%s atoms selected for superposition\n"%(len(indicesSI),))

COM = np.average(initialStruc.xyz[0,ligAtoms], axis=0, weights=masses)
initialStructures.append(initialStruc)
  
lambdaCoordPath = tools.analFolder+"cleaned/"

initialStruc.save(outputDir+"cleaned/initial.gro")

fileInfoList = tools.lineageRanges()
psout = tools.config["fbmd"]["production"]["trj-save-fs"]*.001

def calcLambda(path, vec):
  logger.write("  Outputting corresponding lambda coordinates for structural ensemble...\n")
  for seed in range(Nbakapara):
    if os.path.exists(lambdaCoordPath+"%s.aligned.lambda"%seed): continue
    traj = tools.load(tools.analFolder+"cleaned/%s.xtc"%seed, top=initialStruc.top)
    traj.superpose(initialStruc, 0, indicesSI)

    # we need to actually calculate the pbc-fixed COM...
    xyz = np.zeros((len(traj.xyz), 2, 3), dtype=np.float32)
    xyz[:,0] = np.average(traj.xyz[:,ligAtoms], axis=1, weights=masses)
    xyz[:,1] = path[0]
    displacement = np.zeros((len(traj.xyz), 1, 3), dtype=np.float32)
    pairs = np.array([[1, 0]], dtype=np.int32)
    box = mdtraj.utils.ensure_type(traj.unitcell_vectors, dtype=np.float32, ndim=3, name='unitcell_vectors', shape=(len(xyz), 3, 3), warn_on_cast=False)
    orthogonal = np.allclose(traj.unitcell_angles, 90)
    mdtraj.geometry._geometry._dist_mic_displacement(xyz, pairs, box.transpose(0, 2, 1).copy(), displacement, orthogonal)
    lmbd = np.dot(displacement, vec).astype(np.float32)
  
    logger.write("    %s %s %s %s\n"%(seed, np.shape(displacement), np.min(lmbd), np.max(lmbd)))

    lmbd.tofile(lambdaCoordPath+"%s.aligned.lambda"%seed)

def estDisDir():
  global path, vec
  if tools.config["pathing"].get("custom-lambda"):
    shutil.copy2(tools.analFolder+tools.config["pathing"]["custom-lambda"], outputDir+"cleaned/lambda.path")
    path = np.array([[float(j) for j in i.strip().split(",")] for i in open(outputDir+"cleaned/lambda.path").readlines()[1:]])
    vec = path[1]-path[0]
    vec /= np.linalg.norm(vec)
  else:
    logger.write("Estimating dissociation direction...\n")
    dLambda = 1.0
    cLambda = dLambda
  
    heavyAtoms = initialStruc.xyz[0,proteinAtoms]*10
    pocketCenter = COM*10
    refPoints = []
  
    ligand = initialStruc.xyz[0,ligAtoms]*10
    lpc = np.mean(ligand, axis=0)
    RG = np.sqrt(np.mean(np.linalg.norm(ligand-lpc, axis=1)**2))
    d = RG + 10.0
  
    while True:
      samples = np.round(4 * np.pi * cLambda * cLambda).astype(int)
      logger.write("  %s %s "%(cLambda, samples))
  
      offset = 2./samples
      increment = np.pi * (3. - np.sqrt(5.))
  
      grid = []
      for i in range(samples):
        y = ((i * offset) - 1) + (offset / 2);
        r = np.sqrt(1 - pow(y,2))

        phi = (i%samples) * increment

        x = np.cos(phi) * r
        z = np.sin(phi) * r
    
        grid.append([x*cLambda,y*cLambda,z*cLambda])
    
      grid = np.array(grid)
    
      grid += pocketCenter

      dists = np.array([np.min(np.linalg.norm(grid[i]-heavyAtoms, axis=1)) for i in range(len(grid))])
  
      # this keeps track of the previous position in order to minimize jumping...
      if len(refPoints):
        rp = np.array(refPoints[-1][1:])
        alt_dists = np.linalg.norm(grid-rp, axis=1)
        idx = np.argmax(dists-alt_dists)
      else: idx = np.argmax(dists)

      logger.write("%s\n"%dists[idx])

      if dists[idx] > d: break
  
      if dists[idx] > RG+2:
        gp = grid[idx]
        refPoints.append([cLambda, gp[0], gp[1], gp[2]])
  
      cLambda += dLambda
  
    refPoints = np.array(refPoints).T
  
    X = scipy.stats.linregress(refPoints[0], refPoints[1])
    Y = scipy.stats.linregress(refPoints[0], refPoints[2])
    Z = scipy.stats.linregress(refPoints[0], refPoints[3])

    axis = np.array([X[0], Y[0], Z[0]])
    axis /= np.linalg.norm(axis)
  
    endPoint = (axis*cLambda)+pocketCenter
    open(outputDir+"cleaned/lambda.path", "w").write("2\n%s,%s,%s\n%s,%s,%s\n"%(repr(pocketCenter[0]*.1), repr(pocketCenter[1]*.1), repr(pocketCenter[2]*.1), repr(endPoint[0]*.1), repr(endPoint[1]*.1), repr(endPoint[2]*.1)))

    path = np.array([COM, COM+axis])
    vec = axis
  
  calcLambda(path, vec)
  
  logger.write("Done.\n\n")

try:
  path = tools.path
  vec = tools.vec
except:
  lambdaCoordPath = tools.config["pathing"].get("try-path") and tools.CWD+tools.config["pathing"].get("try-path") or None
  if not os.path.exists(lambdaCoordPath):
    lambdaCoordPath = outputDir+"cleaned/"
    if not os.path.exists(lambdaCoordPath+"%s.aligned.lambda"%0): estDisDir()
    lambdaCoordPath = outputDir+"cleaned/lambda.path"
  path = np.array([[float(j) for j in i.strip().split(",")] for i in open(lambdaCoordPath).readlines()[1:]])
  vec = path[1]-path[0]
  vec /= np.linalg.norm(vec)
  
if not os.path.exists(lambdaCoordPath+"0.aligned.lambda"):
  lambdaCoordPath = outputDir+"cleaned/"
  calcLambda(path, vec)

RC_offset = np.dot(COM-path[0], vec) # lambda_s

logger.write("lambda_s = %s\n\n"%(RC_offset,))

bins, pdf_log = tools.getReweigingProb("300")
pdf_min = bins[0]
nob = len(pdf_log)-1
pdf_binSize = bins[1]-bins[0]

heavy_pairs = np.array(list(itertools.product(ligAtoms, otherAtoms)), dtype=np.int32)
if tools.config["analysis"].get("Rcluster-ligand-packing"): heavy_pairs = np.array(heavy_pairs.tolist() + [(i,j) for (i,j) in itertools.combinations(ligAtoms, 2) if abs(initialStruc.top.atom(i).residue.index - initialStruc.top.atom(j).residue.index) > 3], dtype=np.int32)

xout_time = (tools.config["fbmd"]["production"]["trj-save-fs"]/tools.config["base"]["dt"])*0.001 # ps

def scanRvalue(inside, prevStructures, RCcurs, ligCurs, r0s, contactss, COMs):
  lnP = []
  indexInfo = []
  Rs = []
  nocontacts = False
  for id, info in inside.items():
    ene = np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%(id,), dtype=np.float32)
    last = np.max(list(info.keys()))
    fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%id)
    fp.__len__() # workaround to weird mdtraj/python/cython bug...
    for idx, (xyz, time, step, box) in enumerate(fp.readyield()):
      if not idx in info: continue
      if idx > last: break

      bin = np.floor((ene[idx]-pdf_min)/pdf_binSize).astype(int)
      if bin < 0: bin = 0
      elif bin > nob: bin = nob

      com = np.average(xyz[ligAtoms], axis=0, weights=masses)
      RC_val = np.dot(com-path[0], vec)
      
      best_R = 0.0

      r = np.min([np.linalg.norm(calcPBCawareDiff(com, COM)) for COM in COMs]) # PBC aware distance
      
      if r < COM_cutoff:
        for s in range(len(prevStructures)):
          contacts = contactss[s]
          if len(contacts) < minimalContactsR: R = 0
          else:
            r0 = r0s[s]
            RCcur = RCcurs[s]
            tf = (vec*(RC_val-RCcur))
            
            # we should also make THIS pbc aware, as pbc fixing doesn't work very well...

            r = calcPBCawareDistances(xyz, contacts)
            R = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))))
      
            if R < Rcutoff and not simplifiedRmode:
              xyz[ligAtoms] -= tf # align the lambda value of the ligand to the one of the representative structure of the previous window
              r = calcPBCawareDistances(xyz, contacts)
              R_L = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))))
              R = max(R, R_L)
              xyz[ligAtoms] += tf # restore

          if R > best_R: best_R = R

      lnP.append(pdf_log[bin])
      indexInfo.append([id, idx])
      Rs.append(best_R)
      
    fp.close()

  return lnP, indexInfo, Rs
  
PBCaD_xyz = np.zeros((1, 2, 3), dtype=np.float32)
PBCaD_displacement = np.zeros((1, 1, 3), dtype=np.float32)
PBCaD_pairs = np.array([[1, 0]], dtype=np.int32)
PBCaD_box = mdtraj.utils.ensure_type(initialStruc.unitcell_vectors[:1], dtype=np.float32, ndim=3, name='unitcell_vectors', shape=(1, 3, 3), warn_on_cast=False).transpose(0, 2, 1).copy()
PBCaD_orthogonal = np.allclose(initialStruc.unitcell_angles[:1], 90)
def calcPBCawareDiff(A, B):
  PBCaD_xyz[:,0] = A
  PBCaD_xyz[:,1] = B
  mdtraj.geometry._geometry._dist_mic_displacement(PBCaD_xyz, PBCaD_pairs, PBCaD_box, PBCaD_displacement, PBCaD_orthogonal)
  return np.copy(PBCaD_displacement[0,0])
  
def calcPBCawareDistances(xyz, pairs, system=None):
  if system == None:
    box = PBCaD_box
    orthogonal = PBCaD_orthogonal
  distances = np.zeros((1, len(pairs)), dtype=np.float32)
  mdtraj.geometry._geometry._dist_mic(np.array([xyz]), pairs, box, distances, orthogonal)
  return distances[0]
  
def scanRMSD(inside, prevStructures, RCcurs, ligCurs, COMs):
  tmplt = mdtraj.Trajectory(ref.xyz[0], topology=initialStruc.top)
  
  RMSDs = []
  for id, info in inside.items():
    last = np.max(list(info.keys()))
    fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%(id,))
    fp.__len__() # workaround to weird mdtraj/python/cython bug...
    for idx, (xyz, time, step, box) in enumerate(fp.readyield()):
      if not idx in info: continue
      if idx > last: break

      # make this pbc aware...
      com = np.average(xyz[ligAtoms], axis=0, weights=masses)
      RC_val = np.dot(com-path[0], vec) # PBC unaware lambda value, to get translation in absolute space
      
      best_R = 0.0
      best_rmsd = 1e4

      r = np.min([np.linalg.norm(calcPBCawareDiff(com, COM)) for COM in COMs]) # PBC aware distance
      if r < COM_cutoff:
        # need to do a superpose first...
        tmplt.xyz[0] = xyz
        tmplt.superpose(initialStruc, 0, indicesSI)
        xyz = tmplt.xyz[0]
        
        for s in range(len(prevStructures)):
          RCcur = RCcurs[s]
          ligCur = ligCurs[s]

          tf = (vec*(RC_val-RCcur))
          lig = xyz[ligAtoms]-tf
          rmsd = np.sqrt(np.mean(np.square(np.linalg.norm(lig-ligCur, axis=1)))) # RMSD_L

          if rmsd < best_rmsd: best_rmsd = rmsd

      RMSDs.append(best_rmsd)
      
  return RMSDs

def filterEnsemble(inside, prevStructures): # step 2
  RCcurs = [] # RCcur
  ligCurs = [] # ligCur
  r0s = [] # r0
  contactss = [] # contacts
  COMs = []
  nocontacts = False
  for prevStructure in prevStructures:
    com = np.average(prevStructure[ligAtoms], axis=0, weights=masses)
    COMs.append(com)
    RCcur = np.dot(com-path[0], vec)
    RCcurs.append(RCcur)
    ligCurs.append(prevStructure[ligAtoms])

    distances = calcPBCawareDistances(prevStructure, heavy_pairs)
    nearby = distances < NATIVE_CUTOFF
    contactss.append(heavy_pairs[nearby])
    r0s.append(distances[nearby])
    if len(contactss[-1]) < minimalContactsR: nocontacts = True

  lnP = []
  indexInfo = []
  Rs = []
  RMSDs = []

  lnP, indexInfo, Rs = scanRvalue(inside, prevStructures, RCcurs, ligCurs, r0s, contactss, COMs)
  
  cutoff_rmsd = RMSDcutoff
  cutoff_R = Rcutoff
  
  Rs = np.array(Rs)
  selection = np.where(Rs >= cutoff_R)[0]
  
  if not nocontacts:
    while len(selection) < minNumberPerCluster*initial_structs_per_window: # we prefer at least 25 structures per cluster...
      selection = np.where(Rs >= cutoff_R)[0]
      if cutoff_R <= Rcutoff_final: break
      cutoff_R -= 0.01 # -1% cutoff
  else: cutoff_R = 0

  rmsdCalc = False
  if (len(selection) < minNumberPerCluster*initial_structs_per_window) or nocontacts:
    RMSDs = scanRMSD(inside, prevStructures, RCcurs, ligCurs, COMs)
    RMSDs = np.array(RMSDs)
    rmsdCalc = True
    while len(selection) < minNumberPerCluster*initial_structs_per_window: # we prefer at least 25 structures per cluster...
      if nocontacts: selection = np.where(RMSDs <= cutoff_rmsd)[0]
      else: selection = np.where((RMSDs <= cutoff_rmsd) | (Rs >= cutoff_R))[0]
      if cutoff_rmsd >= RMSDcutoff_final: break
      cutoff_rmsd += 0.05 # add 0.5 A extra cutoff...
  
  logger.write("    Rcutoff: %s"%cutoff_R)
  if rmsdCalc: logger.write(" RMSDcutoff: %s\n"%cutoff_rmsd)
  else: logger.write("\n")
  
  if len(selection) == 0: 
    if rmsdCalc: selection = [np.argmin(RMSDs)]
    else: selection = [np.argmax(Rs)] 
  
  indexInfo2 = OrderedDict()
  lnP2 = []
  
  for idx, i in enumerate(selection):
    if not indexInfo[i][0] in indexInfo2: indexInfo2[indexInfo[i][0]] = {}
    indexInfo2[indexInfo[i][0]][indexInfo[i][1]] = idx
    lnP2.append(lnP[i])
  
  return lnP2, indexInfo2

def getRepr(prevStructures, lnP, indexInfo, n_clusters): # step 3
  if len(lnP) <= n_clusters:
    nextStructures = []
    for id, info in indexInfo.items():
      for idx, v in info.items(): nextStructures.append([id, idx, info[idx]])
    return nextStructures

  nearby = set()
  for prevStructure in prevStructures:
    distances = calcPBCawareDistances(prevStructure, heavy_pairs)
    for i in np.where(distances < NATIVE_CUTOFF)[0]: nearby.add(i)
  if len(nearby): contacts = heavy_pairs[np.array(sorted(nearby))]
  else: contacts = []
  
  coords = []
  mapping = []
  for id, info in indexInfo.items():
    fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%(id,))
    fp.__len__() # workaround to weird mdtraj/python/cython bug...
    ene = np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%(id,), dtype=np.float32)
    last = np.max(list(info.keys()))
    for idx, (xyz, time, step, box) in enumerate(fp.readyield()):
      if not idx in info: continue
      if idx > last: break
      if len(contacts) < minimalContactsR: coords.append(xyz[ligAtoms].flatten()) # simply cluster based on xyz since there are not enough contacts
      else: coords.append(calcPBCawareDistances(xyz, contacts))
      mapping.append([id, idx, info[idx]])

  kmeans = sklearn.cluster.KMeans(n_clusters=n_clusters, random_state=kmeans_random_state)
  kmeans.fit(coords)
  
  nextStructures = []
  
  for i in range(n_clusters):
    average = np.zeros(shape=len(coords[0]), dtype=np.float32)
    N = 0
    
    tmp = []
    for j in np.where(kmeans.labels_ == i)[0]: tmp.append(lnP[mapping[j][2]])
    LSE_max = np.max(tmp)
    
    for j in np.where(kmeans.labels_ == i)[0]:
      selIdx = mapping[j][2]
    
      P = np.exp(lnP[selIdx]-LSE_max)
      average += coords[j]*P
      N += P
    average /= N
    
    best = None
    lowest_MSD = 1e99
    
    for j in np.where(kmeans.labels_ == i)[0]:
      msd = np.mean(np.square(coords[j]-average))
      if msd < lowest_MSD:
        best = mapping[j]
        lowest_MSD = msd
    if best == None:
      logger.write("baka %s\n"%lowest_MSD)
      exit()
    logger.write("    lowest RMSD: %s\n"%(lowest_MSD**.5))
    nextStructures.append(best)
  
  return nextStructures

def fixPBC(xyz):
  r = calcPBCawareDistances(xyz, heavy_pairs)
  nearest_pair = heavy_pairs[np.argmin(r)]
  xyz[ligAtoms] += (xyz[nearest_pair[1]]-xyz[nearest_pair[0]]) + calcPBCawareDiff(xyz[nearest_pair[0]], xyz[nearest_pair[1]])
  
def calcPBCawareDiff(A, B):
  PBCaD_xyz[:,0] = A
  PBCaD_xyz[:,1] = B
  mdtraj.geometry._geometry._dist_mic_displacement(PBCaD_xyz, PBCaD_pairs, PBCaD_box, PBCaD_displacement, PBCaD_orthogonal)
  return np.copy(PBCaD_displacement[0,0])
  
def getStructuresClean(structures2fetch):
  finfo = {}
  for name, (seed, sidx, selIdx) in structures2fetch.items(): 
    if not seed in finfo: finfo[seed] = {}
    finfo[seed][sidx] = name

  structures = {}
  for seed, v in finfo.items():
    fname = tools.analFolder+"cleaned/%s.xtc"%seed
    fp = yieldxtc.XTCTrajectoryFile(fname)
    fp.__len__() # workaround to weird mdtraj/python/cython bug...
  
    tooutput = sorted(v.keys())
    next = tooutput.pop(0)
    for sidx, (xyz, time, step, box) in enumerate(fp.readyield()):
      if sidx != next: continue
      fixPBC(xyz)
      structures[v[next]] = [xyz, time, step, box]
      if len(tooutput) == 0: break # done
      next = tooutput.pop(0)
    fp.close()

  return structures

def getStructuresRaw(structures2fetch):
  finfo = {}
  
  for fname, (seed, sidx, selIdx) in structures2fetch.items():
    atTime = int(sidx)*psout
    fobj = tools.figureOutWhichFile(fileInfoList[seed], atTime)
    fin = fobj[0]
    atTime -= fobj[3]-fobj[1]
    if not fin in finfo: finfo[fin] = {}
    finfo[fin][atTime] = fname

  structures = {}

  for fin, v in finfo.items():
    fp = yieldxtc.XTCTrajectoryFile(fin)
    fp.__len__() # workaround to weird mdtraj/python/cython bug...
  
    tooutput = sorted(v.keys())
    next = tooutput.pop(0)
    for xyz, time, step, box in fp.readyield():
      if time != next: continue
      structures[v[next]] = [xyz, time, step, box]
    
      if len(tooutput) == 0: break # done
      next = tooutput.pop(0)
    fp.close()
  return structures
  
def getNative(fp_log): # for initial window
  prevStructures = [i.xyz[0] for i in initialStructures]
  widx = np.where(windowCenters == 0.0)[0][0]
  windowCenter = windowCenters[widx]
  
  if len(prevStructures) >= initial_structs_per_window: # we don't need to do this because we already heave enough structures...
    shutil.copy2(initialStructurePath, outputDir+"/repr/%s.%s.gro"%(windowCenter, 0))
    return prevStructures
  
  inside = {}
  N = 0
  
  for seed in range(Nbakapara): # step 1
    RCs = np.fromfile(lambdaCoordPath+"%s.aligned.lambda"%(seed,), dtype=np.float32)-RC_offset
    tmp = {}
    for i in np.where((RCs >= windowRanges[widx][0]) & (RCs < windowRanges[widx][1]))[0]: tmp[i] = RCs[i]
    
    N += len(tmp)
    if len(tmp): inside[int(seed)] = tmp
    
    
    #####################################break #################################################################

  logger.write("  filtering nearby structures (N: %s)\n"%N)
  
  old_settings = np.seterr(over='ignore')
  lnP, indexInfo = filterEnsemble(inside, prevStructures)
  np.seterr(**old_settings)
  
  logger.write("  clustering structures (N: %s)\n"%(len(lnP)))

  # instead of calculating the average, cluster the data instead...
  nextStructures = getRepr(prevStructures, lnP, indexInfo, initial_structs_per_window)

  tmp = {}
  for nidx, best in enumerate(nextStructures): structures2save["%s.%s"%(windowCenter, nidx)] = tmp["%s.%s"%(windowCenter, nidx)] = best
  tmpStructs = getStructuresClean(tmp)

  bestScan = np.zeros((len(prevStructures), len(nextStructures)))

  structures = []

  for nidx, best in enumerate(nextStructures):
    xyz, time, step, box = tmpStructs["%s.%s"%(windowCenter, nidx)]
    seed, sidx, selIdx = best

    # recalculate rmsd & R
      
    bestR = 0
    bestN = -1
    for pidx, prevStructure in enumerate(prevStructures):
      rmsd = np.sqrt(np.mean(np.square(np.linalg.norm(prevStructure[ligAtoms]-xyz[ligAtoms], axis=1))))

      distances = calcPBCawareDistances(prevStructure, heavy_pairs)
      nearby = distances < NATIVE_CUTOFF
      contacts = heavy_pairs[nearby]
      r0 = distances[nearby]
      r = calcPBCawareDistances(xyz, contacts)
      R = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))))
    
      bestScan[pidx,nidx] = R
      
      logger.write("  > %s->%s > %s %s %s %s %s %s %s\n"%(pidx, nidx, windowCenter, inside[seed][sidx], seed, sidx, lnP[selIdx], rmsd, R))
      fp_log.write("%s %s %s %s %s %s %s\n"%(windowCenter, inside[seed][sidx], seed, sidx, lnP[selIdx], rmsd, R))
      fp_log.flush()
      
    structures.append(xyz)
      
    # save gro file
    struc = mdtraj.Trajectory(np.array([xyz]), initialStruc.top, np.array([time]))
    struc.superpose(initialStruc, 0, indicesSI)
    struc.unitcell_vectors = np.array([box])
    struc.save_gro(outputDir+"/cleaned/repr/%s.%s.gro"%(windowCenter, nidx))

  # now, from bestScan, figure out which entries to replace...
    
  if tools.config["pathing"].get("replace-initial", True):
    mask = np.ones_like(bestScan, dtype=bool)
    temp = np.copy(bestScan)
  
    for j in range(len(prevStructures)):
      best = [-1, -1, -1]
      for i in range(len(prevStructures)):
        bestN = np.argmax(temp[i])
        if temp[i][bestN] > best[0]: best = [temp[i][bestN], i, bestN]
  
      R, i, bestN = best
    
      logger.write("  replacing %s with input structure %s\n"%(bestN, initialStructurePath))
      shutil.copy2(initialStructurePath, outputDir+"/repr/%s.%s.gro"%(windowCenter, bestN))
      
      struc = mdtraj.Trajectory(np.array([prevStructures[i]]), initialStruc.top, np.array([0]))
      struc.superpose(initialStruc, 0, indicesSI)
      struc.save_gro(outputDir+"/cleaned/repr/%s.%s.gro"%(windowCenter, bestN))
    
      del structures2save["%s.%s"%(windowCenter, bestN)]
    
      structures[bestN] = prevStructures[i]
    
      # invalidate
      temp[i,:] = -np.inf
      temp[:,bestN] = -np.inf

  logger.write("done.\n\n")
  
  return structures
  
  
def getStructure(widx, prevStructures, fp_log): # for other windows
  windowCenter = windowCenters[widx]
  
  logger.write("starting %s nm\n"%windowCenter)
  inside = {}
  N = 0

  for seed in range(Nbakapara): # step 1
    RCs = np.fromfile(lambdaCoordPath+"%s.aligned.lambda"%(seed,), dtype=np.float32)-RC_offset

    tmp = {}
    
    for i in np.where((RCs >= windowRanges[widx][0]) & (RCs < windowRanges[widx][1]))[0]: tmp[i] = RCs[i]
    N += len(tmp)
    if len(tmp): inside[int(seed)] = tmp
    
    #####################break ######################################

  if N == 0: return prevStructures, -1
    
  logger.write("  filtering nearby structures (N: %s)\n"%N)
  
  old_settings = np.seterr(over='ignore')
  lnP, indexInfo = filterEnsemble(inside, prevStructures)
  np.seterr(**old_settings)
  
  if len(lnP) < 1:
    logger.write("No structures found\n")
    return prevStructures, -1
  
  logger.write("  clustering structures (N: %s)\n"%(len(lnP)))
  
  # instead of calculating the average, cluster the data instead...
  nextStructures = getRepr(prevStructures, lnP, indexInfo, initial_structs_per_window)

  logger.write("  output average structures (N: %s)\n"%(len(nextStructures)))

  structures = []
  
  tmp = {}
  for nidx, best in enumerate(nextStructures): structures2save["%s.%s"%(windowCenter, nidx)] = tmp["%s.%s"%(windowCenter, nidx)] = best
  tmpStructs = getStructuresClean(tmp)

  for nidx, best in enumerate(nextStructures):
    xyz, time, step, box = tmpStructs["%s.%s"%(windowCenter, nidx)]
    seed, sidx, selIdx = best
      
    # recalculate rmsd & R
    bestR = 0
    bestN = -1
    for pidx, prevStructure in enumerate(prevStructures):
      rmsd = np.sqrt(np.mean(np.square(np.linalg.norm(prevStructure[ligAtoms]-xyz[ligAtoms], axis=1))))

      distances = calcPBCawareDistances(prevStructure, heavy_pairs)
      nearby = distances < NATIVE_CUTOFF
      contacts = heavy_pairs[nearby]
      r0 = distances[nearby]
      r = calcPBCawareDistances(xyz, contacts)
      R = np.mean(1.0 / (1 + np.exp(BETA_CONST * (r - LAMBDA_CONST * r0))))
      
      logger.write("  > %s->%s > %s %s %s %s %s %s %s\n"%(pidx, nidx, windowCenter, inside[seed][sidx], seed, sidx, lnP[selIdx], rmsd, R))
      fp_log.write("%s %s %s %s %s %s %s\n"%(windowCenter, inside[seed][sidx], seed, sidx, lnP[selIdx], rmsd, R))
      fp_log.flush()
      
    structures.append(xyz)

    # save gro file
    struc = mdtraj.Trajectory(np.array([xyz]), initialStruc.top, np.array([time]))
    struc.superpose(initialStruc, 0, indicesSI)
    struc.unitcell_vectors = np.array([box])
    struc.save_gro(outputDir+"/cleaned/repr/%s.%s.gro"%(windowCenter, nidx))

  logger.write("done.\n\n")
  
  return structures

structures2save = {}

def scan():
  fp_log = open(outputDir+"/lambdaScan.log", "w")
  
  # get structures for the window where lambda' = 0
  nativeStructures = getNative(fp_log)
  
  # get structures for the windows where lambda' < 0
  structures = nativeStructures
  for widx in np.where(windowCenters < 0)[0][::-1]: structures = getStructure(widx, structures, fp_log)
    
  # get structures for the windows where lambda' > 0
  structures = nativeStructures
  for widx in np.where(windowCenters > 0)[0]: structures = getStructure(widx, structures, fp_log)

  logger.write("Outputting final files...\n\n")

  for name, (xyz, time, step, box) in getStructuresRaw(structures2save).items():
    # save gro file
    struc = mdtraj.Trajectory(np.array([xyz]), baseStruct.top, np.array([time]))
    struc.unitcell_vectors = np.array([box])
    struc.save_gro(outputDir+"/repr/%s.gro"%(name,))


  logger.write("Done.\n\n")
  
  fp_log.close()

scan()
