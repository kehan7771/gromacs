# Contact gertjan.bekker@protein.osaka-u.ac.jp for more info

# this script executes the fb-mcmd simulation on the cmc cluster in an automated manner based on the input config.yml script


# imports
import yaml, os, numpy as np, subprocess, time, sys, struct, datetime, random, shlex

PY3K = sys.version_info >= (3, 0)

try: 
  from collections import OrderedDict
except: 
  from ordereddict import OrderedDict
  
# make matplotlib optional...
import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
  
CAL = 4.184
  
# in the future this should be read in from the command line...
conf_file = sys.argv[1]

# load config & init
__initial_dir__ = os.getcwd()
CWD = os.path.abspath(os.path.dirname(os.getcwd()+os.sep+conf_file))+os.sep
conf_file = os.path.basename(conf_file)
os.chdir(CWD)

config = None
def loadConfig():
  global config
  try: config = yaml.load(open(CWD+conf_file).read(), Loader=yaml.FullLoader)
  except: config = yaml.load(open(CWD+conf_file).read())

loadConfig()
__base__ = config["base"]
__compute__ = config["compute"]
GMX = __compute__["gmx"]

restart_file = CWD + os.path.splitext(conf_file)[0]+".restart.yml"
try: 
  try: RESTART = yaml.load(open(restart_file).read(), Loader=yaml.FullLoader)
  except: RESTART = yaml.load(open(restart_file).read())
  if RESTART == None: RESTART = {}
except: RESTART = {}

if __compute__["compute_mode"] not in ["single", "multi"]: PBSscript = open(__base__["pbs-base"]).read()


# make modifications to the code to do a restart after all the jobs are finished on tsubame
# this, because there is a cpu-time limit on each process...

class Logger(object):
  def __init__(self, fname, mode):
    self.terminal = sys.stdout
    self.log = open(fname, mode)

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)
    self.terminal.flush()
    self.log.flush()
    
class fauxLogger(object):
  def __init__(self, fname=None, mode=None):
    self.terminal = sys.stdout
  
  def write(self, message): 
    self.terminal.write(message)
    self.terminal.flush()

### processify (https://gist.github.com/schlamar/2311116)

import traceback
from functools import wraps
from multiprocessing import Process, Queue


def processify(func):
    '''Decorator to run a function as a process.
    Be sure that every argument and the return value
    is *pickable*.
    The created process is joined, so the code does not
    run in parallel.
    '''

    def process_func(q, *args, **kwargs):
        try:
            ret = func(*args, **kwargs)
        except Exception:
            ex_type, ex_value, tb = sys.exc_info()
            error = ex_type, ex_value, ''.join(traceback.format_tb(tb))
            ret = None
        else:
            error = None

        q.put((ret, error))

    # register original function with different name
    # in sys.modules so it is pickable
    process_func.__name__ = func.__name__ + 'processify_func'
    setattr(sys.modules[__name__], process_func.__name__, process_func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        q = Queue()
        p = Process(target=process_func, args=[q] + list(args), kwargs=kwargs)
        p.start()
        ret, error = q.get()
        p.join()

        if error:
            ex_type, ex_value, tb_str = error
            message = '%s (in subprocess)\n%s' % (ex_value, tb_str)
            raise ex_type(message)

        return ret
    return wrapper

### END processify

def restart_program():
  sys.exit(3) # since nothing works, simply exit and have a parent runner take care of everything...
    
def saveRESTART(): open(restart_file, "w").write(yaml.dump(RESTART))

def loadMD(inp):
  obj = OrderedDict()
  for line in inp.splitlines():
    line = line.split(";")[0].strip()
    if not line or line[0] == ";": continue
    line = line.partition("=")
    obj[line[0].strip()] = line[2].strip()
  return obj
    
def saveMD(inp):
  out = ""
  n = max([len(i) for i in inp.keys()])
  for k,v in inp.items(): out += ("%-"+str(n)+"s = %s\n")%(k, v)
  return out
  
__AVAIL_NODES__ = None
 
###### 

def paraQueueProc():
  while True:
    cwd, name, commands = paraQueueProc_queue.get()
    gpuid = paraQueueProc_GPUIDs.pop(0)
  
    my_env = os.environ.copy()
    try: del my_env["OMP_NUM_THREADS"]
    except: pass
    my_env["CUDA_VISIBLE_DEVICES"] = str(gpuid)

    with open(cwd+os.sep+name+".pjs.log", "w") as fp:
      for cmd in commands: 
        process = subprocess.Popen(shlex.split(cmd), stdout=fp, stderr=subprocess.STDOUT, cwd=cwd, env=my_env)
        process.wait()
  
    paraQueueProc_GPUIDs.append(gpuid)
    paraQueueProc_queue.task_done()
 
import queue, threading
paraQueueProc_queue = queue.Queue()
paraQueueProc_GPUIDs = list(range(__compute__.get("para-jobs", 1)))
for _ in range(__compute__.get("para-jobs", 1)): threading.Thread(target=paraQueueProc, daemon=True).start()
  
def queueJob(mdp_file, struc_file, restart_file, tpl_file, wt_nod, options="", cwd=None):
  name = os.path.splitext(os.path.basename(mdp_file))[0]
  tpr_file = name+".tpr"
  if restart_file: ppc = "%s grompp -f %s -c %s -t %s -p %s -o %s %s -po %s"%(GMX, mdp_file, struc_file, restart_file, tpl_file, tpr_file, options, mdp_file.replace(".mdp", ".out.mdp"))
  else: ppc = "%s grompp -f %s -c %s -p %s -o %s %s -po %s"%(GMX, mdp_file, struc_file, tpl_file, tpr_file, options, mdp_file.replace(".mdp", ".out.mdp"))
  #if __compute__["compute_mode"] == "single": os.system("%s mdrun -deffnm %s -notunepme >> %s.log 2>&1"%(GMX, name, name))
  if __compute__["compute_mode"] == "single":
    os.system(ppc)
    os.system("%s mdrun -deffnm %s -notunepme -ntmpi 1"%(GMX, name))
  elif __compute__["compute_mode"] == "multi":
    paraJobs = __compute__.get("para-jobs", 1)
    ntomp = __compute__.get("para-ntomp", 1)
    paraQueueProc_queue.put([os.getcwd(), name, [ppc, "%s mdrun -deffnm %s -notunepme -ntmpi 1 -ntomp %s"%(GMX, name, ntomp)]])
  elif __compute__["compute_mode"] == "pbs":
    pbs_loc = os.path.splitext(mdp_file)[0]+".pbs"
    open(pbs_loc, "w").write(PBSscript%(__base__["name"]+"/"+name, os.path.dirname(mdp_file), ppc, GMX, name))
    process = subprocess.Popen(["qsub", pbs_loc], stdout=subprocess.PIPE)
    out, err = process.communicate()
    return out.strip()
  elif __compute__["compute_mode"] == "slurm":
    pbs_loc = os.path.splitext(mdp_file)[0]+".sh"
    open(pbs_loc, "w").write(PBSscript%(__base__["name"]+"/"+name, os.path.dirname(mdp_file), ppc, GMX, name))
    process = subprocess.Popen(["sbatch", pbs_loc], stdout=subprocess.PIPE)
    out, err = process.communicate()
    return out.strip().split()[-1]
  elif __compute__["compute_mode"] == "fugaku":
    wt_secs = int(np.round((wt_nod*24*60*60*1.1)+300)) # walltime in days to seconds + 10% + 5 min
    wt = "%02d:%02d:%02d"%((wt_secs // (3600 * 24))*24 + ((wt_secs // 3600) % 24), (wt_secs // 60) % 60, wt_secs % 60)
    pbs_loc = os.path.splitext(mdp_file)[0]+".pbs"
    conf = {"walltime": wt, "mdxdir": os.path.dirname(mdp_file), "gromppCMD": ppc, "GMX": GMX, "GMXmpi": __compute__.get("gmx-mpi", GMX), "tprname": name, "maxh": wt_secs/3600., "jobname": __base__["name"]+"-"+name}
    pbs = PBSscript%conf
    open(pbs_loc, "w").write(pbs)    
    while True:
      try:
        process = subprocess.Popen(["pjsub", pbs_loc], stdout=subprocess.PIPE)
        break
      except OSError: time.sleep(1)
    out, err = process.communicate()
    return out.strip().split()[5]
  elif __compute__["compute_mode"] == "tsubame" or __compute__["compute_mode"] == "sge":
    pbs_loc = os.path.splitext(mdp_file)[0]+".pbs"

    wt_secs = int(np.round((wt_nod*24*60*60*1.1)+300)) # walltime in days to seconds + 10% + 5 min
    wt = "%02d:%02d:%02d"%((wt_secs // (3600 * 24))*24 + ((wt_secs // 3600) % 24), (wt_secs // 60) % 60, wt_secs % 60)
    jobname = __base__["name"]+"-"+name

    if not os.path.exists(pbs_loc):
      if __compute__["compute_mode"] == "tsubame": open(pbs_loc, "w").write(PBSscript%(os.path.dirname(mdp_file), ppc, "%s mdrun -deffnm %s -ntomp 14 -ntmpi 1 -maxh %s -pin on"%(GMX, name, wt_secs/3600.)))
      else: open(pbs_loc, "w").write(PBSscript%(jobname, os.path.dirname(mdp_file), ppc, GMX, name))

    for i in range(10):
      if __compute__["compute_mode"] == "tsubame": cmd = "qsub -g %s -l q_node=1 -l h_rt=%s -N %s %s"%(__base__["tsubame-group"], wt, jobname, pbs_loc)
      else: cmd = "qsub %s"%(pbs_loc)
      
      try: process = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
      except BlockingIOError:
        time.sleep(10)
        continue
    
      out, err = process.communicate()
      try: return out.split()[2]
      except: time.sleep(5) # wait 5 seconds and try to submit again...
      
  elif __compute__["compute_mode"] == "kaneka":
    pbs_loc = os.path.splitext(mdp_file)[0]+".bash"
    open(pbs_loc, "w").write(PBSscript%(__base__["name"]+"/"+name, os.path.dirname(mdp_file), ppc, GMX, name))
    process = subprocess.Popen(["bsub", '-n 5 -gpu "num=1:mode=exclusive_process"', " bash %s " %(pbs_loc)], stdout=subprocess.PIPE)
    out, err = process.communicate()
    return out.strip()
      
def startNext(jobs, goNext, args=[], jobCommands={}, targetFiles={}, skipRestart=False):
  step = args[0]-1
  if __compute__["compute_mode"] == "single" or __compute__["compute_mode"] == "multi": goNext(*args)
  elif __compute__["compute_mode"] == "pbs":
    while True:
      
      qstatSet = set()
      
      process = subprocess.Popen(['qstat'], stdout=subprocess.PIPE)
      out, err = process.communicate()
      OK = True
      for line in out.splitlines()[2:]:
        line = line.decode("utf-8").split()
        qstatSet.add(line[0])
        if line and line[0] in jobs:
          OK = False
          break
        else:
          "add option to check whether the job finished correctly --> if not, resubmit..."

      for jobid, seed in jobs.items():
        if jobid in qstatSet: continue
        if not os.path.exists(os.path.dirname(targetFiles[seed])+os.sep+"%s.o%s"%(seed, jobid.split(".")[0])) and not os.path.exists(os.path.dirname(targetFiles[seed])+os.sep+"%s.pbs.o%s"%(seed, jobid.split(".")[0])):
          OK = False
          continue
        if not os.path.exists(targetFiles[seed]):
          logger.write("job %s was finished for seed %s, but didn't finish properly; rerunning...\n"%(jobid, seed))
          OK = False
          os.chdir(CWD+"mdx-%s"%step)
          new_jobid = queueJob(*jobCommands[seed])
          os.chdir(CWD)
          jobs.pop(jobid)
          jobs[new_jobid] = seed
          qstatSet.add(new_jobid)
          
          RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
          saveRESTART()
          
      if OK: break
      else: time.sleep(30)

    goNext(*args) 
  elif __compute__["compute_mode"] == "slurm":
    while True:
      
      qstatSet = set()
      
      process = subprocess.Popen(['squeue'], stdout=subprocess.PIPE)
      out, err = process.communicate()

      OK = True
      for line in out.splitlines()[1:]:
        line = line.strip().split()
        qstatSet.add(line[0])
        if line and line[0] in jobs:
          OK = False
          break
        else:
          "add option to check whether the job finished correctly --> if not, resubmit..."

      for jobid, seed in jobs.items():
        if PY3K: jobid = jobid.decode("utf-8")
        if jobid in qstatSet: continue
        stdoutf = os.path.dirname(targetFiles[seed])+os.sep+"%s.done"%(jobid.split(".")[0],)
        if not os.path.exists(stdoutf):
          OK = False
          continue
        if not os.path.exists(targetFiles[seed]):
          logger.write("job %s was finished for seed %s, but didn't finish properly; rerunning...\n"%(jobid, seed))
          OK = False
          os.chdir(CWD+"mdx-%s"%step)
          new_jobid = queueJob(*jobCommands[seed])
          os.chdir(CWD)
          jobs.pop(jobid)
          jobs[new_jobid] = seed
          qstatSet.add(new_jobid)
          
          RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
          saveRESTART()
      
      if OK: break
      else: time.sleep(30)

    goNext(*args) 
  elif __compute__["compute_mode"] == "fugaku":
    while True:
      
      qstatSet = set()
      
      while True:
        try:
          process = subprocess.Popen(['pjstat'], stdout=subprocess.PIPE)
          break
        except OSError: time.sleep(1)
      
      out, err = process.communicate()

      OK = True
      for line in out.splitlines()[1:]:
        line = line.strip().split()
        qstatSet.add(line[0].decode("utf-8"))
        if line and line[0] in jobs: OK = False
        else:
          "add option to check whether the job finished correctly --> if not, resubmit..."
      for jobid, seed in jobs.items():
        try: jobid = jobid.decode("utf-8")
        except: pass
        if jobid in qstatSet: continue
        if not os.path.exists(targetFiles[seed]):
          logger.write("job %s was finished for seed %s, but didn't finish properly; rerunning...\n"%(jobid, seed))
          OK = False
          os.chdir(CWD+"mdx-%s"%step)
          new_jobid = queueJob(*jobCommands[seed])
          os.chdir(CWD)
          jobs.pop(jobid)
          jobs[new_jobid] = seed
          qstatSet.add(new_jobid)
          
          RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
          saveRESTART()
      if OK: break
      else: time.sleep(30)

    goNext(*args) 
  elif __compute__["compute_mode"] == "tsubame" or __compute__["compute_mode"] == "sge":
    while True:
      qstatSet = set()

      try:
        process = subprocess.Popen(['qstat'], stdout=subprocess.PIPE)
        out, err = process.communicate()
      except BlockingIOError:
        time.sleep(10)
        continue
      OK = True
      for line in out.splitlines()[2:]:
        line = line.split()
        qstatSet.add(line[0])
        if line and line[0] in jobs:
          OK = False
          break
          
      if OK:
        time.sleep(30)
        bad = []
        for jobid, seed in jobs.items():
          baka = True
          for i in range(10):
            if os.path.exists(targetFiles[seed]) and os.stat(targetFiles[seed]).st_size > 0:
              baka = False
              break
            time.sleep(10)

          if baka:
            bad.append(jobid)
            OK = False
            
        for jobid in bad:
          seed = jobs[jobid]
          logger.write("job %s was finished for seed %s, but didn't finish properly; rerunning...\n"%(jobid, seed))
          os.chdir(CWD+"mdx-%s"%step)
          new_jobid = queueJob(*jobCommands[seed])
          os.chdir(CWD)
          jobs.pop(jobid)
          jobs[new_jobid] = seed
          qstatSet.add(new_jobid)
          RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
          saveRESTART()
          time.sleep(10)
        if OK: break
      time.sleep(60) # 1 minute
    
    
    
    if skipRestart or __compute__["compute_mode"] == "sge": goNext(*args)
    else: return restart_program() # on tsubame we have to restart the script in order not to get hit by the cpu-time limit...     
    
  elif __compute__["compute_mode"] == "kaneka":
    jobids = [] 
    for jobid, seed in jobs.items():
      job_id = jobid.split()
      jobids.append(job_id[1][1:-1])
 
    while True:
      qstatSet = set()

      process = subprocess.Popen(['bjobs'], stdout=subprocess.PIPE)
      out, err = process.communicate()
      OK = True
      #for line in out.splitlines()[2:]:
      for line in out.splitlines()[1:]:
        line = line.split()
        qstatSet.add(line[0])
        RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
        saveRESTART()
        #if line and line[0] in jobs:
        if line[0] in jobids:
          OK = False
          break
          
      if OK: break
      time.sleep(30) # .5 minute
    goNext(*args)
  
  
  
def goNext():
  os.chdir(CWD)
  folders = [i for i in os.listdir(CWD) if os.path.isdir(CWD+i)]
  files = [i for i in os.listdir(CWD) if not os.path.isdir(CWD+i)]
  x = -2
  for i in folders:
    if i[:4] == "mdx-" and int(i[4:]) > x: x = int(i[4:])

  if "CURRENT_JOBS" in RESTART:
    jobs, jobCommands, targetFiles = RESTART["CURRENT_JOBS"]
    ttt = False
    try:
      if os.path.exists(jobCommands[0][0]): ttt = True
    except: pass
    if ttt: return startNext(jobs, mdXrun, [x+1], jobCommands, targetFiles, True)
  mdXrun(x+1)
  
@processify
def com_histogram(step):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  if "restraints" in fbmd and "path-file" in fbmd["restraints"]:
    path = np.array([[float(j) for j in i.strip().split(",")] for i in open(CWD+config["fbmd"]["restraints"]["path-file"]).readlines()[1:]])
    vec = path[1]-path[0]
    vec /= np.linalg.norm(vec)
    
    lambdaFrom = fbmd["restraints"]["lambda-from"]*.1 - .2
    lambdaTo = fbmd["restraints"]["lambda-to"]*.1 + .2
    bins = np.arange(lambdaFrom, lambdaTo+.05, .025)
    hist = np.array([0 for i in bins])

    for i in range(len(RESTART["seeds"])):
      seed = RESTART["seeds"][i]
      if not os.path.exists(CWD+"mdx-%s/%s.cylinder.mc"%(step, seed)): continue
      
      try: COMs = np.fromfile(CWD+"mdx-%s/%s.cylinder.mc"%(step, seed), dtype=np.float32)
      except: continue # tsubame sucks
      COMs = COMs[:int(np.floor(len(COMs)/3)*3)]
      COMs = COMs.reshape((int(len(COMs)/3), 3))
      lmbd = np.tensordot(COMs-path[0], vec, axes=1)
      if step == 0: lmbd = lmbd[equilSteps:]
      
      np.add.at(hist, np.digitize(lmbd, bins)-1, 1)
    
    # clear memory
    COMs = []
    lmbd = []
      
    hist = np.log(hist/np.sum(hist).astype(np.float64))
    plt.plot(bins+((bins[1]-bins[0])*.5), hist)
    plt.suptitle("mdx-%s lambda distribution"%step)
    plt.xlabel("Lambda (nm)")
    plt.ylabel("lnP")
    plt.savefig(CWD+"histograms/mdx-%s.lambda.png"%step, dpi=600)
    plt.close()
    
@processify
def prep_histogram(step):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)

  maxEnergyBin = -1e99
  minEnergyBin = 1e99
  
  bxpstats = list()
  fig, ax = plt.subplots(1,1)
  for i in range(len(RESTART["seeds"])):
    seed = RESTART["seeds"][i]
    if not os.path.exists(CWD+"mdx-%s/%s.fb.ene"%(step, seed)): continue
    
    try: single = np.fromfile(CWD+"mdx-%s/%s.fb.ene"%(step, seed), dtype=np.float32)
    except: continue # tsubame sucks
    if step == 0: single = single[equilSteps:]
    if len(single) == 0: continue # tsubame sucks

    bxpstats.extend(mpl.cbook.boxplot_stats(single, labels=[seed]))
    
    mn = np.min(single)
    mx = np.max(single)
    if mn < minEnergyBin: minEnergyBin = mn
    if mx > maxEnergyBin: maxEnergyBin = mx
    
  # clear memory
  single = []
    
  ax.bxp(bxpstats, vert=0)
  plt.suptitle("mdx-%s potential energy range plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.savefig(CWD+"histograms/mdx-%s.erp.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  return minEnergyBin, maxEnergyBin
  
@processify
def load_histogram(step, bins):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  
  hist = np.array([0 for i in bins])
  arr = np.array([[0 for i in bins] for j in bins], dtype=np.int)
  
  for i in range(len(RESTART["seeds"])):
    seed = RESTART["seeds"][i]
    if not os.path.exists(CWD+"mdx-%s/%s.fb.ene"%(step, seed)): continue
    
    single = np.fromfile(CWD+"mdx-%s/%s.fb.ene"%(step, seed), dtype=np.float32)
    if step == 0: single = single[equilSteps:]
    #single = single[np.isfinite(single)]
    
    indexes = np.digitize(single, bins)-1 #
    
    np.add.at(hist, indexes, 1)
    np.add.at(arr, (indexes[:-1], indexes[1:]), 1)
    
  # clear memory
  single = []
  indexes = []
  
  return hist, arr
  
def buildHistogram(step):
  fbmd = config["fbmd"]
  
  binSize = float(config["fbmd"]["binSize"])
  initialTemp = float(config["fbmd"]["initialTemp"])
  minTemp = float(config["fbmd"]["minTemp"])
  maxTemp = float(config["fbmd"]["maxTemp"])
  minSamples = float(config["fbmd"]["minSamples"])
  convVal =  0.008314462175 # kj/(mol*K)
  
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  baka = np.array([initialTemp/minTemp, initialTemp/maxTemp], dtype=np.float32) # gamma itself was saved in single precision, so the comparison must be made using single precision data, else it doesn't match

  # execute this in a new process...
  #if "restraints" in fbmd and "path-file" in fbmd["restraints"]: com_histogram(step)
  minEnergyBin, maxEnergyBin = prep_histogram(step)
  ####minEnergyBin, maxEnergyBin = -456305.97, -238916.28

  if step > 0:
    gfp = open(CWD+"histograms/mdx-%s.dat"%(step-1,), "rb")
    chunk = gfp.read(12)
    data = struct.unpack("=ffi", chunk)
    alt_bins = [data[0]]
    binSize = data[1]
    for i in range(data[2]-1): alt_bins.append(alt_bins[i]+binSize)
    alt_gamma = struct.unpack("f"*data[2], gfp.read(4*data[2]))
    alt_gamma = np.array(alt_gamma, dtype=np.float32)
    
    minEnergyBin = float(int(minEnergyBin)-(int(minEnergyBin)%binSize))
    maxEnergyBin = float(int(maxEnergyBin)-(int(maxEnergyBin)%binSize))
    
    bins = list(np.arange(minEnergyBin, maxEnergyBin+binSize, binSize))
    bins = list(np.arange(min(bins+alt_bins), max(bins+alt_bins)+binSize+binSize, binSize))
    
    gamma = [None for i in range(len(bins))]

    for i in range(len(alt_gamma)): gamma[bins.index(alt_bins[i])] = alt_gamma[i]
    
    x = -1
    for i in range(len(gamma)):
      if gamma[i] != None: 
        x = i
        break
        
    for i in range(0, x): gamma[i] = gamma[x]
    
    x = -1
    for i in reversed(range(len(gamma))):
      if gamma[i] != None: 
        x = i
        break
    for i in range(x, len(gamma)): gamma[i] = gamma[x]
    
    bins = np.array(bins, dtype=np.float32)
  else:
    minEnergyBin = float(int(minEnergyBin)-(int(minEnergyBin)%binSize))
    maxEnergyBin = float(int(maxEnergyBin)-(int(maxEnergyBin)%binSize))
    bins = np.arange(minEnergyBin, maxEnergyBin+binSize+binSize, binSize)
    gamma = [initialTemp/fbmd.get("center-temp", fbmd["maxTemp"]) for i in range(len(bins))]

  # execute this in a new process...
  hist, arr = load_histogram(step, bins)
  
  Dgamma = np.array([0.0 for i in gamma], dtype=np.float32)
  
  hist_pdf = hist/float(np.sum(hist))
  hist_pdf_log = np.log(hist_pdf)
    
  bincenters = bins+((bins[1]-bins[0])*.5)
  plt.suptitle("mdx-%s potential energy distribution plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.ylabel("ln(P)")
  plt.plot(bincenters, hist_pdf_log, linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.edpf.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  hist_pdf_log[hist_pdf_log == -np.inf] = np.min(hist_pdf_log[hist_pdf_log != -np.inf])
    
  tmp = np.mean(hist)*.01
  if tmp > minSamples: minSamples = tmp
  if np.max(hist) < minSamples: minSamples = int(np.mean(hist))
  
  extrap_weights = []
  
  # figure out to which bins the bins progress to...
  extrap = [0]
  
  for i in range(1, len(hist)-1):
    npre = len(np.where(arr[i][:i] > 0)[0])
    npost = len(np.where(arr[i][i+1:] > 0)[0])
    N = max(npre, npost)
    totgrad = 0.0
    totratio = 0.0
    nox = hist[i]
    if nox:
      if hist[i-1] + hist[i+1]:
        ratio = (float(arr[i][i-1] + arr[i][i+1]) / (hist[i-1] + hist[i+1])) * (float(arr[i][i-1] + arr[i][i+1]) / nox)
        totgrad += ((hist_pdf_log[i+1] - hist_pdf_log[i-1]) / (2.0*binSize)) * ratio
        totratio += ratio

      for j in range(i-N+1, i):
        dl = i-j
        if i-dl < 0 or i+dl > len(hist_pdf_log)-1 or not hist[i-dl] + hist[i+dl]: continue
        ratio = (float(arr[i][i-dl] + arr[i][i+dl]) / (hist[i-dl] + hist[i+dl])) * (float(arr[i][i-dl] + arr[i][i+dl]) / nox)
        totgrad += ((hist_pdf_log[i+dl] - hist_pdf_log[i-dl]) / (dl*2.0*binSize)) * ratio
        totratio += ratio
      if totratio > 0.0: totgrad /= totratio

    Dgamma[i] = convVal * initialTemp * totgrad
    if hist[i-1] > minSamples and hist[i+1] > minSamples: extrap_weights.append(1.0)
    else:
      extrap.append(i)
      extrap_weights.append(min(hist[i-1], hist[i+1])/minSamples)
  extrap.append(len(hist)-1)
  
  old_bins = np.copy(bins)
  old_gamma = np.copy(gamma)
  
  Dgamma[0] = ((convVal * initialTemp * (hist_pdf_log[1] - hist_pdf_log[0])) / (2.0 * binSize))
  Dgamma[-1] = ((convVal * initialTemp * (hist_pdf_log[-1] - hist_pdf_log[-2])) / (2.0 * binSize))
  extrap_weights.insert(0, min(hist[0], hist[1])/minSamples)
  extrap_weights.append(min(hist[-1], hist[-2])/minSamples)
    
  bincenters_old = bincenters

  try: fi_ = np.where((old_gamma) >= baka[0])[0][-1]
  except: fi_ = 0
  try: ti_ = np.where(old_gamma <= baka[1])[0][0]
  except: ti_ = len(old_gamma)-1

  stddev, coverage = np.std(hist_pdf_log[fi_:ti_]), float((initialTemp/gamma[ti_]) - (initialTemp/gamma[fi_])) / (maxTemp - minTemp)
  if np.abs(coverage-1.0) < 1e-4: coverage = 1.0
  
  hasDoneExtrapolation = False
  
  sf = 1.0
  if stddev > 1: sf = fbmd.get("early-dgamma-scaling", 1.0)
  Dgamma *= sf

  if coverage >= 1.0 and np.min(hist[fi_:ti_]) > minSamples: 
    Dgamma[np.array(extrap_weights, dtype=np.float32) < 1] = 0.0 # ignore any bins which have less than /minSamples/ number of samples after obtaining a somewhat flat distribution
  else: # extrapolation & smoothing of low-density data during the initial training phase (before obtaining a somewhat flat distribution)
    gamma_pre = gamma + Dgamma

    FP = np.where(old_gamma == baka[0])[0]
    FP = len(FP) and FP[-1] or -1
    LP = np.where(old_gamma == baka[1])[0]
    LP = len(LP) and LP[0] or 1e99

    order = 2

    if coverage < 1.0: # only extrapolate while the coverage < 100%
      mask = np.ones(gamma_pre.shape,dtype=bool) #np.ones_like(a,dtype=bool)
      mask[extrap] = False
      noe = int(np.round(fbmd.get("extrap-percentage", 0.0) * .01 * 0.5 * (np.abs(baka[1] - baka[0]) / (np.abs(min(gamma_pre[mask]) - max(gamma_pre[mask])) / np.abs(np.argmax(gamma_pre[mask]) - np.argmin(gamma_pre[mask]))))))

      # don't make it extrapolate too much in the beginning when there is not enough data...
      tmp = int(np.round(len(np.where(np.array(extrap_weights, dtype=np.float32) >= 1.0)[0])*.5))

      if noe > tmp: noe = tmp
      
      noeA = np.max([noe-np.where(hist > 0)[0][0], 0])
      noeB = np.max([noe-(len(hist)-np.where(hist > 0)[0][-1]), 0])
      
      if noeA > 0 or noeB > 0: hasDoneExtrapolation = True

      for i in range(len(extrap)): extrap[i] += noeA
      Dgamma = list(Dgamma)
      gamma = list(gamma)
      bins = list(bins)

      for i in range(noeA):
        Dgamma.insert(0, 0.0)
        extrap_weights.insert(0, 0.0)
        extrap.insert(0, i)
        gamma.insert(0, gamma[0])
        bins.insert(0, bins[0]-binSize)
        
      for i in range(noeB):
        Dgamma.append(0.0)
        extrap_weights.append(0.0)
        extrap.append(len(Dgamma)-1)
        gamma.append(gamma[-1])
        bins.append(bins[-1]+binSize)
        
      Dgamma = np.array(Dgamma, dtype=np.float32)
      gamma = np.array(gamma, dtype=np.float32)
      bins = np.array(bins, dtype=np.float32)
      
    gamma_pre = gamma + Dgamma
      
    B = bins[:-1]+(binSize*.5)
    G = np.copy(gamma_pre)
    W = np.array(extrap_weights, dtype=np.float32)
      
    mask = np.ones(gamma_pre.shape,dtype=bool) #np.ones_like(a,dtype=bool)
    mask[W < 1] = False
    N = int(.2 * (np.abs(baka[1] - baka[0]) / (np.abs(min(gamma_pre[mask]) - max(gamma_pre[mask])) / np.abs(np.argmax(gamma_pre[mask]) - np.argmin(gamma_pre[mask])))))
    if N > len(mask): order = 1

    bc = bins + (binSize*.5)
      
    for i in extrap:
      if i < FP and extrap_weights[i] < 1: continue
      if i > LP and extrap_weights[i] < 1: continue
      dsts = np.abs(B-bc[i]) # find the nearest bins
      dgroup = dsts.argsort()
      dgroup = dgroup[W[dgroup] > 0.0] #everything with W =< 0.0 should be excluded --> these are the extrapolated bins (with no information)
      xtra = len(np.where(W[dgroup[:N]] < 1.0)[0]) # make sure that there is enough high quality data...
      dgroup = dgroup[:N+xtra] # only use the local data
      polyFunc = np.poly1d(np.polyfit(B[dgroup], G[dgroup], order, w=W[dgroup])) # extrapolate using a 2nd order polynomial
      Dgamma[i] = (((polyFunc(bc[i])-gamma[i])*(1-extrap_weights[i])) + (Dgamma[i] * extrap_weights[i])) # and reweight between extrapolated data and measured data based on the estimated accuracy of the measured data

    if fbmd.get("extrap-percentage", 0.0) == 0: Dgamma[extrap_weights == 0] = 0
    bins = np.array(bins, dtype=np.float32)
    gamma = np.array(gamma, dtype=np.float32)
    Dgamma = np.array(Dgamma, dtype=np.float32)
    bincenters = 0.5*(bins[1:]+bins[:-1])

  for i in range(len(Dgamma)): gamma[i] += Dgamma[i]

  gamma = np.array(gamma, dtype=np.float32)
  
  try: fi_p = np.where(gamma >= baka[0])[0][-1]
  except: fi_p = 0
  try: ti_p = np.where(gamma <= baka[1])[0][0]+1
  except: ti_p = len(gamma)-1
  
  if gamma[-1] < baka[0]: gamma[np.where(gamma > baka[0])[0]] = baka[0]
  else: fi_p = 0
  if gamma[0] > baka[1]: gamma[np.where(gamma < baka[1])] = baka[1]
  else: ti_p = len(gamma)-1
  if ti_p > len(gamma)-1: ti_p = len(gamma)-1
  
  plt.suptitle("mdx-%s potential energy distribution plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.ylabel("ln(P)")
  
  stddev = np.std(hist_pdf_log[fi_:ti_])
  coverage = float((initialTemp/gamma[ti_p]) - (initialTemp/gamma[fi_p])) / (maxTemp - minTemp)
  if np.abs(coverage-1.0) < 1e-4: coverage = 1.0
  logger.write("step: %s coverage: %s stddev: %s\n"%(step, coverage, stddev))
  
  if hasDoneExtrapolation: plt.plot(bincenters_old, hist_pdf_log, linewidth=.33)
  else: plt.plot(bincenters_old[fi_:ti_], hist_pdf_log[fi_:ti_], linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.edpf.test.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  plt.suptitle("mdx-%s force bias plot"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\Delta$$\gamma$")
  
  plt.plot(bincenters[fi_p:ti_p], Dgamma[fi_p:ti_p], linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.dgamma.png"%step, dpi=600)
  plt.close()
  
  plt.suptitle("mdx-%s force bias plot (int)"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\Delta$$\gamma$")
  
  plt.plot(bincenters[fi_p:ti_p], np.cumsum(Dgamma[fi_p:ti_p]), linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.dgamma-cs.png"%step, dpi=600)
  plt.close()


  sz = np.ceil((ti_p-fi_p)*0.025).astype(np.int)
  fi_p = max(fi_p-sz, 0)
  ti_p = min(ti_p+sz, len(gamma)-1)
  gamma = gamma[fi_p:ti_p]
  bins = bins[fi_p:ti_p]

  plt.suptitle("mdx-%s force bias plot"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\gamma$")
  
  bincenters = bins+((bins[1]-bins[0])*.5)
  plt.plot(bincenters, gamma, linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.gamma.png"%step, dpi=600)
  plt.close()
  
  data = [bins[0], binSize, len(gamma)]+list(gamma)
  open(CWD+"histograms/mdx-%s.dat"%step, "wb").write(struct.pack("=ffi"+("f"*len(gamma)), *data))
  
  if "--preview" in sys.argv: return stddev, coverage, np.array(bins, dtype=np.float32), gamma, binSize
  
  return stddev, coverage, np.array(bins, dtype=np.float32), gamma, binSize

# run the next iteration (automatically determine the histogram mode)
def mdXrun(step):
  global __AVAIL_NODES__, GMX
  
  loadConfig()
  
  __AVAIL_NODES__ = __compute__.get("__AVAIL_NODES__", [])[:]
  
  fbmd = config["fbmd"]
  
  md = loadMD(open(CWD+__base__["base_inp"]).read())
  
  initialTemp = float(config["fbmd"]["initialTemp"])
  
  flatConditionAchieved = (RESTART.get("flat-condition") or 1e99) < step
  stableCondition = (RESTART.get("stable-condition") or 1e99) < step
  
  # find out a way to detect whether to go to the production phase or not...
  __continue__ = False
  productionPhase = (RESTART.get("production-phase") or 1e99) < step
  if productionPhase: __continue__ = True
  
  hostloc = CWD
  if __compute__["compute_mode"] == "remote": 
    hostloc = __compute__["remote_wd"]
    if hostloc[-1] != "/": hostloc += "/"
  
  # set restraints if required
  # both protein distance restraints & ligand - pocket distance restraints
  
  if "restraints" in fbmd and step > -1:
    if "posres-file" in fbmd["restraints"]:
      md["do-bekker-posres"] = "yes"
      md["bekker-posres-file"] = hostloc + fbmd["restraints"]["posres-file"]
    if "fb-posres-file" in fbmd["restraints"]:
      md["fb-posres-file"] = hostloc + fbmd["restraints"]["fb-posres-file"]
      md["fb-posres-lr"] = 500000
    if "disres-file" in fbmd["restraints"]:
      md["do-bekker-disres"] = "yes"
      md["bekker-disres-file"] = hostloc + fbmd["restraints"]["disres-file"]
    if "comres-file" in fbmd["restraints"]:
      md["do-bekker-comres"] = "yes"
      md["bekker-comres-file"] = hostloc + fbmd["restraints"]["comres-file"]
      if "comres-particle-k" in fbmd["restraints"]: md["comres-particle-k"] = fbmd["restraints"]["comres-particle-k"]
      if "comres-box-dmz" in fbmd["restraints"]: 
        md["comres-box-dmz"] = fbmd["restraints"]["comres-box-dmz"]
        if "comres-box-dmz-dims" in fbmd["restraints"]: md["comres-box-dmz-dims"] = fbmd["restraints"]["comres-box-dmz-dims"]
    if "path-file" in fbmd["restraints"]:
      md["do-bekker-cylinder"] = "yes"
    
      md["cylinder-group"] = fbmd["restraints"]["restrain-group"]
      md["cylinder-path-file"] = hostloc+fbmd["restraints"]["path-file"]
    
      md["cylinder-max-orth-dist"] = fbmd["restraints"]["orthogonal-range"]*.1
      if "orthogonal-range2" in fbmd["restraints"]: md["cylinder-max-orth-dist2"] = fbmd["restraints"]["orthogonal-range2"]*.1
      md["cylinder-orth-k"] = fbmd["restraints"]["orthogonal-force"]*CAL*100
    
      md["cylinder-lambda-range"] = "%s %s"%(fbmd["restraints"]["lambda-from"]*.1, fbmd["restraints"]["lambda-to"]*.1)
      md["cylinder-lambda-k"] = fbmd["restraints"]["lambda-force"]*CAL*100


  if step == -1:
    random.seed(fbmd["seeds-seed"])
    RESTART["real-seeds"] = random.sample(range(int(1e4), int(1e7)), fbmd["seeds"])
    RESTART["seeds"] = list(range(fbmd["seeds"]))
  
    loopli = .1 # 100 ps
    md["define"] = "-DPOSRES" # use position restraints during equilibration of the velocities
    coverage = 0.0
    
    RESTART["coverages"] = {}
    RESTART["stddevs"] = {}
  elif step == 0:
    if "fixed-simulation-protocol" in config["fbmd"]: loopli = config["fbmd"]["fixed-simulation-protocol"][0]
    else: loopli = fbmd["simlength-initial"]
    coverage = 0.0
  elif not __continue__:
    stddev, coverage, bins, gamma, binSize = buildHistogram(step-1)
    md["fb-histogram-file"] = hostloc+"histograms/mdx-%s.dat"%(step-1)
    if "fixed-simulation-protocol" in config["fbmd"]:
      if step < len(config["fbmd"]["fixed-simulation-protocol"]): loopli = config["fbmd"]["fixed-simulation-protocol"][step]
      else: 
        productionPhase = True # start the production phase after the fixed protocol has finished
        RESTART["production-phase"] = step
    else:
      loopli = config["fbmd"]["simlength"] * coverage

      RESTART["coverages"][step-1] = float(coverage)
      RESTART["stddevs"][step-1] = float(stddev)
    
      keys = RESTART["coverages"].keys()
      coverages = np.array([RESTART["coverages"][key] for key in keys])
      stddevs = np.array([RESTART["stddevs"][key] for key in keys])
      
      if stddev < 1.0: # do shorter simulations when the distribution is less flat (to more quickly update the bias)
        fastsim = loopli * .5**4 # lower
        longsim = loopli  # max
      
        A = 1.0
        B = config["fbmd"]["min-stddev"]
        
        if len(coverages) > 3 and coverages[-2] < 1.0 and stddev < B*2: longsim = loopli * .25 # make sure that the simulation length doesn't immediatly go to max after reaching full coverage...
    
        ratio = (stddev - B) / (A-B) # stddev:1.0 --> ratio:1.0 ; stddev:0.2 --> ratio:0.0
        ratio = 1-ratio # stddev:1.0 --> ratio:0.0 ; stddev:0.2 --> ratio:1.0
        ratio **= 2
      
        loopli = ((longsim-fastsim)*ratio) + fastsim
      
        if loopli > longsim: loopli = longsim
        elif loopli < fastsim: loopli = fastsim
      if coverage < 1.0 or stddev > 1.0: loopli = (config["fbmd"]["simlength"] * coverage) * .5**4
    
      refLL = (len(bins)*fbmd["minSamples"]*__base__["dt"]*.25)/1e6
      if loopli < refLL: loopli = refLL
  else: md["fb-histogram-file"] = hostloc+"histograms/mdx-%s.dat"%(RESTART["production-phase"]-1)
  
  if not "fixed-simulation-protocol" in config["fbmd"] and len(np.where(np.array(list(RESTART["coverages"].values())) >= 1.0)[0]) > 5:
    # but before moving to the production phase, make sure that the previous iteration was at least full length (i.e. 16ns)
    if not productionPhase and not flatConditionAchieved:
      if coverage >= 1.0 and RESTART["stddevs"].get(np.max(list(RESTART["stddevs"].keys()))-1, np.inf) < config["fbmd"]["min-stddev"] and stddev < config["fbmd"]["min-stddev"]:
        flatConditionAchieved = True
        RESTART["flat-condition"] = step
      elif step >= fbmd.get("go-anyway-step", np.inf):
        flatConditionAchieved = True
        RESTART["flat-condition"] = step
  
    if not productionPhase and not stableCondition and flatConditionAchieved: # do long simulations in order to do equilibration...
      if step-RESTART["flat-condition"] >= fbmd.get("mcmd-equil-stages", 0):
        stableCondition = True
        RESTART["stable-condition"] = step
      else: loopli = fbmd.get("mcmd-equil-simlength", config["fbmd"]["production"]["simlength"])
  
    if not productionPhase and stableCondition and flatConditionAchieved:
      productionPhase = True
      RESTART["production-phase"] = step

  use_restart = [step-1 for i in RESTART["seeds"]]
  
  if productionPhase:
    loopli = config["fbmd"]["production"]["simlength"]
    if "save-xtc-group" in config["fbmd"]["production"]: # save as xtc
      md["compressed-x-grps"] = config["fbmd"]["production"]["save-xtc-group"]
      md["nstxout-compressed"] = int(np.round(config["fbmd"]["production"]["trj-save-fs"] / __base__["dt"]))
    else: # save as trr
      md["nstxout"] = int(np.round(config["fbmd"]["production"]["trj-save-fs"] / __base__["dt"]))
  else: 
    RESTART["production-phase"] = None
  
  wt_nod = float(loopli/__compute__.get("performance", 0.1))
  
  ns2steps = 1e6/__base__["dt"]
  loopli = int(np.round(loopli*ns2steps))
  
  md["continuation"] = "yes"
  md["gen-vel"] = "no"
  
  md["dt"] = __base__["dt"]/1000.0
  md["ref_t"] = " ".join([str(initialTemp) for i in md["ref_t"].split()])
  
  md["do-fb-mcmd"] = "yes"
  
  if "restraints" in fbmd and fbmd["restraints"].get("disres-scaling") and step > -1: md["fb-disres-scaling"] = "yes"
  if step > -1 and fbmd.get("implicit-mcmd"): md["fb-implicit"] = "yes"
  
  if fbmd.get("include-restrain-epot"): md["fb-include-restrain-epot"] = "yes"

  tpl_file = hostloc+__base__["tpl"]
  index_file = __base__["index"] and hostloc+__base__["index"]
  
  if productionPhase and step >= RESTART["production-phase"]+config["fbmd"]["production"].get("phases", 1): return # exit...

  jobs = {}
  
  md["fb-base-temp"] = initialTemp
  
  jobCommands = {}
  targetFiles = {}

  os.mkdir(CWD+"mdx-%s"%step)
  os.chdir(CWD+"mdx-%s"%step) 

  # upgrade this to first do nvt eq with posres @ 300K (eq-mdx), also, use a .cpt file to start the simulation (to ensure a correct boxsize)
  
  for seed in RESTART["seeds"]:
    options = []
    nwarn = __base__.get("nwarn", 0)
    if index_file: options.append("-n %s"%index_file)
    if "fb-implicit" in md: options.append("-nb cpu") # this currently only works on the CPU...

    md["nsteps"] = loopli
    
    if step > 0:
      testStep = use_restart[seed] # to allow switching back to a stable state of this simulation...
      prev = False
      while True: # find the latest cpt file... --> needs to be done to deal with bad systems (i.e. CMC's VCC, tsubame)
        if os.path.exists(CWD+"mdx-%s/%s.cpt"%(testStep, seed)) and os.stat(CWD+"mdx-%s/%s.cpt"%(testStep, seed)).st_size > 0: break
        if os.path.exists(CWD+"mdx-%s/%s_prev.cpt"%(testStep, seed)) and os.stat(CWD+"mdx-%s/%s_prev.cpt"%(testStep, seed)).st_size > 0: 
          prev = True
          break
        testStep -= 1
      if testStep >= 0: 
        if prev: restart_file = hostloc+"mdx-%s/%s_prev.cpt"%(testStep, seed)
        else: restart_file = hostloc+"mdx-%s/%s.cpt"%(testStep, seed)
      else: 
        logger.write("Missing restart file for mdx-%s/%s\n"%(testStep, seed))
        exit()
        
      if "restraints" in fbmd and "fb-posres-file" in fbmd["restraints"]: md["fb-posres-restart"] = "../mdx-%s/%s.fb.prcpt"%(step-1, seed)
      
    elif step == -1: # equilibrate the velocities with position restraints using -DPOSRES
      if "cpt" in __base__:  restart_file = hostloc+__base__["cpt"]%{"seed": seed} # load a restart file to find accurate box information
      else: restart_file = None
      md["gen-vel"] = "yes"
      md["gen-seed"] = RESTART["real-seeds"][seed]
      md["continuation"] = "no"
      md["gen-vel"] = "yes"
      md["gen-temp"] = initialTemp
      options.append("-r %s"%(hostloc+__base__["pdb"]))
      
      try: del md["do-fb-mcmd"]
      except: pass
      try: del md["fb-include-restrain-epot"]
      except: pass
      try: del md["fb-base-temp"]
      except: pass
      try: del md["nstcalcenergy"]
      except: pass
      
    elif step == 0: # equilibrate the structures with McMD
      restart_file = hostloc+"mdx-%s/%s.cpt"%(-1, seed)
      md["fb-heating-temp"] = fbmd.get("center-temp", fbmd["maxTemp"])
      
    if step > -1 and "energy-group" in fbmd: 
      md["fb-energy-group"] = fbmd["energy-group"]
      GMX = __compute__["gmx-impl"]
    else: GMX = __compute__["gmx"]
    
    open(CWD+"mdx-%s/%s.mdp"%(step, seed), "w").write(saveMD(md))
    
    if nwarn: options.append("-maxwarn %s"%nwarn)
    
    struc_file = hostloc+__base__["pdb"]
    
    jobCommands[seed] = [hostloc+"mdx-%s/%s.mdp"%(step, seed), struc_file, restart_file, tpl_file, wt_nod, " ".join(options), CWD+"mdx-%s/"%step]
    if step == -1: targetFiles[seed] = CWD+"mdx-%s/%s.gro"%(step, seed)
    else: targetFiles[seed] = CWD+"mdx-%s/%s.fb.ene"%(step, seed)
    jobid = queueJob(hostloc+"mdx-%s/%s.mdp"%(step, seed), struc_file, restart_file, tpl_file, wt_nod, " ".join(options), CWD+"mdx-%s/"%step)
    jobs[jobid] = seed

  RESTART["CURRENT_JOBS"] = (jobs, jobCommands, targetFiles)
  
  if __compute__["compute_mode"] == "multi": paraQueueProc_queue.join()
  
  saveRESTART()
  
  if not productionPhase or step < RESTART["production-phase"]+config["fbmd"]["production"].get("phases", 1)-1: 
    if __compute__["compute_mode"] == "fugaku": time.sleep(30)
    startNext(jobs, mdXrun, [step+1], jobCommands, targetFiles)

if not os.path.exists(CWD+"histograms"): os.mkdir(CWD+"histograms")  

def prodPrep():
  os.chdir(CWD)
  folders = [i for i in os.listdir(CWD) if os.path.isdir(CWD+i)]
  x = -2
  for i in folders:
    if i[:4] == "mdx-" and int(i[4:]) > x: x = int(i[4:])

  stddev, coverage, bins, gamma, binSize = buildHistogram(x)
  
  
  

if "--prod-prep" in sys.argv:
  prodPrep()
  
if "--prod-run" in sys.argv:
  ""

if "--preview" in sys.argv:
  folders = [i for i in os.listdir(CWD) if os.path.isdir(CWD+i)]
  files = [i for i in os.listdir(CWD) if not os.path.isdir(CWD+i)]

  if "-x" in sys.argv:
    x = int(sys.argv[sys.argv.index("-x")+1])
  else:
    x = 0
    for i in folders:
      if i[:4] == "mdx-" and int(i[4:]) > x: x = int(i[4:])
  
  #setup fake logger to output to stdin
  logger = fauxLogger()
  
  stddev, coverage, bins, gamma, binSize = buildHistogram(x)
  
  sys.exit()

if "output-log" in __base__: logger = Logger(__base__["output-log"], "a")
else: logger = fauxLogger()
  
kill = False

try:
  pid = open("pid").read().strip()
  try: 
    os.kill(int(pid), 0)
    print("process %s is already running..."%pid)
    kill = True
  except: pass
except: pass

if kill: sys.exit()

open("pid", "w").write(str(os.getpid()))

# do auto NEW...
  
goNext()

