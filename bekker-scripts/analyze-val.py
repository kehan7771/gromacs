import os, mdtraj, numpy as np, itertools, sys
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology

import tools

def additionalArguments(optparser):
  optparser.add_argument("--force-repick", action="store_true")
  optparser.add_argument("--skip-300k", action="store_true")

tools.setup(additionalArguments)

import matplotlib as mpl
import matplotlib.pyplot as plt

topdir = tools.analFolder+"/stability2/"
systemList = ["r%s"%i for i in sorted([int(i[1:]) for i in os.listdir(topdir) if os.path.isdir(topdir+"%s"%i) and i.startswith("r")])]

Nbakapara = 10
minTime = 60.0 # ns

outputIndexesObj = tools.outputIndexes()
dockingMode = "ligand" in tools.config
if dockingMode:
  proteinSelect = outputIndexesObj.receptorAtoms
  ligandSelect = outputIndexesObj.ligandAtoms
else: proteinSelect = ligandSelect = outputIndexesObj.receptorAtoms
SIselect = outputIndexesObj.receptorCalpha

gmx = tools.config["compute"]["gmx"]

simplify = ["md.xtc", "simple.xtc"]

### general

BETA_CONST = 50  # 1/nm
LAMBDA_CONST = 1.8
NATIVE_CUTOFF = 0.45  # nanometers
refStrucs = {}

native = mdtraj.load(tools.analFolder+"ref.gro")

def setupRefStruc(loc):
  if not loc in refStrucs:
    ref = mdtraj.load(loc)
    ref.atom_slice(outputIndexesObj.raw, inplace=True)
    if tools.config["analysis"].get("cyclic"): heavy_pairs = tools.buildBondList()
    else: heavy_pairs = np.array([[i,j] for i,j in itertools.product(proteinSelect, ligandSelect) if np.abs(ref.top.atom(i).residue.index-ref.top.atom(j).residue.index) > 3])
    heavy_pairs_distances = mdtraj.compute_distances(ref[0], heavy_pairs)[0]
    initial_contacts = heavy_pairs[heavy_pairs_distances < NATIVE_CUTOFF]
    r0_initial = mdtraj.compute_distances(ref[0], initial_contacts)
    refStrucs[loc] = [ref, initial_contacts, r0_initial]
  return refStrucs[loc]


def calculate_Rvalue(xtcLoc, refLoc):
  if not os.path.exists(xtcLoc.replace(simplify[0], simplify[1])):
    if not os.path.exists(xtcLoc[:-3]+"gro"):
      print(xtcLoc, "not done")
      exit()
    ref, initial_contacts, r0_initial = setupRefStruc(refLoc)
    os.system("echo 'cleanedAtoms' | %s trjconv -f %s -s %s -o %s -pbc mol -n %s"%(gmx, xtcLoc, xtcLoc.replace(".xtc", ".tpr"), os.path.dirname(xtcLoc)+"/temp.xtc", tools.analFolder+"temp/index.temp.ndx"))
    traj = mdtraj.load(os.path.dirname(xtcLoc)+"/temp.xtc", top=ref.top)
    traj.superpose(ref, 0, SIselect)
    traj.save(xtcLoc.replace(simplify[0], simplify[1]))
    os.remove(os.path.dirname(xtcLoc)+"/temp.xtc")
  else:
    ref, initial_contacts, r0_initial = setupRefStruc(refLoc)
    traj = mdtraj.load(xtcLoc.replace(simplify[0], simplify[1]), top=ref.top)
  return np.mean(1.0 / (1 + np.exp(BETA_CONST * (mdtraj.compute_distances(traj, initial_contacts) - LAMBDA_CONST * r0_initial))), axis=1), traj.time, traj
  

### end general

if not os.path.exists(tools.analFolder+"q-structures"): os.mkdir(tools.analFolder+"q-structures")

forceRepick = tools.cli_args.force_repick
skip300K = tools.cli_args.skip_300k

if os.path.exists(topdir+"Rvalues.npy") and not forceRepick:
  data = np.load(topdir+"Rvalues.npy", allow_pickle=True)
  TIME = np.load(topdir+"TIME.npy")
else:
  data = []
  for sysID in systemList:
    refLoc = topdir+"%s/%s.gro"%(sysID, sysID)
    
    data.append([[], []])
    
    getRepr = not skip300K and (not os.path.exists(tools.analFolder+"q-structures/%s.gro"%(sysID.replace("r", "q"))) or forceRepick)
    if getRepr:
      final = None
      NBPID = None
    
    for i in range(Nbakapara): 
      print(sysID, i)
      if not skip300K:
        Rs, tm, traj = calculate_Rvalue(topdir+"%s/300K/%s/md.xtc"%(sysID, i), refLoc)
        data[-1][0].append(Rs)

        if getRepr:
          TIME = tm*.001
          tmp = traj[np.where(TIME > minTime)[0]]
          if i == 0: 
            final = tmp
            NBPID = np.zeros(Nbakapara*len(tmp), dtype=np.int32)
          else: final += tmp
          NBPID[i*len(tmp):(i+1)*len(tmp)] = i

      Rs, tm, traj = calculate_Rvalue(topdir+"%s/400K/%s/md.xtc"%(sysID, i), refLoc)
      data[-1][1].append(Rs)
      
    if getRepr:
      final.superpose(final[-1], 0)
      average = np.mean(final.xyz, axis=0)
      average = mdtraj.Trajectory(average, topology=final.top)
      final.superpose(average, 0)
      rmsds = mdtraj.rmsd(final, average, 0)
      nearest = np.argmin(rmsds)
      struc = final[nearest]
      struc.superpose(native, 0, type(SIselect) == "str" and struc.top.select(SIselect) or SIselect, type(SIselect) == "str" and native.top.select(SIselect) or SIselect)
      struc.save_gro(tools.analFolder+"q-structures/%s.gro"%(sysID.replace("r", "q")))
      III = NBPID[nearest]
      
      ref = mdtraj.load(refLoc)
      traj = mdtraj.load(topdir+"%s/300K/%s/md.xtc"%(sysID, III), top=ref.top)
      traj[np.where(traj.time == final[nearest].time[0])[0]].save_gro(topdir+"%s/repr.gro"%sysID)
      
  TIME = tm*.001
  data = np.array(data)
  np.save(topdir+"Rvalues.npy", data)
  np.save(topdir+"TIME.npy", TIME)

final40 = np.where(TIME > minTime)[0]

if skip300K: 
  min300K = 0
  min400K = np.min([np.min(i) for i in data[:,1]])
else: 
  min300K = np.min(data[:,0])
  min400K = np.min(data[:,1])

for sidx, sysID in enumerate(systemList):  # change plotting to include 2 plots per figure
  if skip300K:
    all_300K_avg = all_300K_std = 0.0
    f40_300K_avg = f40_300K_std = 0.0
  else:
    all_300K_avg = np.mean(data[sidx,0])
    all_300K_std = np.std(data[sidx,0])
    f40_300K_avg = np.mean(data[sidx,0,:,final40])
    f40_300K_std = np.std(data[sidx,0,:,final40])
  
  all_400K_avg = np.mean(data[sidx,1])
  all_400K_std = np.std(data[sidx,1])
  
  if skip300K: 
    f40_400K_avg = np.mean(np.array(data[sidx,1])[:,final40])
    f40_400K_std = np.std(np.array(data[sidx,1])[:,final40])
  else:
    f40_400K_avg = np.mean(data[sidx,1,:,final40])
    f40_400K_std = np.std(data[sidx,1,:,final40])

  print(sysID)
  print("all %.3f %.3f %.3f %.3f"%(all_300K_avg, all_300K_std, all_400K_avg, all_400K_std))
  print("f40 %.3f %.3f %.3f %.3f"%(f40_300K_avg, f40_300K_std, f40_400K_avg, f40_400K_std))
  if not skip300K: print(["%.3f"%np.mean(data[sidx,0,i,final40]) for i in range(Nbakapara)])
  print("")

  fig, axarr = plt.subplots(2, sharex=True)
  fig.set_size_inches((11.69, 8.27))
  
  for i in range(Nbakapara):
    if not skip300K: 
      axarr[0].plot(TIME, data[sidx,0,i], linewidth=0.25)
      axarr[1].plot(TIME, data[sidx,1,i], linewidth=0.25)
    else: axarr[1].plot(TIME, data[sidx,1][i], linewidth=0.25)

  axarr[0].set_xlim(0, 100)
  axarr[1].set_xlim(0, 100)
  
  axarr[0].set_ylim(min300K, 1)
  axarr[1].set_ylim(min400K, 1)
  
  axarr[0].set_ylabel("R (300 K)", fontsize=16)
  axarr[1].set_ylabel("R (400 K)", fontsize=16)
  
  axarr[-1].set_xlabel("Time (ns)", fontsize=16)
  
  fig.savefig(topdir+"%s.png"%(sysID), dpi=600, bbox_inches='tight')
  plt.close(fig)
