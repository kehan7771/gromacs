import struct, numpy as np, os, scipy, scipy.interpolate, json

import tools

tools.setup()
  
import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt

mdx = tools.RESTART["production-phase"]
Nbakapara = tools.config["fbmd"]["seeds"]
  
prodDat = tools.CWD+"histograms/mdx-%s.dat"%(mdx-1)

target = tools.analFolder+"distributions/"

def asciiPlot(fl, x, y, name):
  data = ["#%s"%name]
  for i in range(len(x)): data.append("%s %s"%(x[i], y[i]))
  open(fl, "w").write("\n".join(data))

def findPeakVal(tg, E, dE):
  while True:
    g = spline(E) # low temp: g is high; high temp: g is low
    if (g > tg and dE < 0) or (g < tg and dE > 0): dE = -dE*.5
    E += dE
    if np.abs(g-tg) < 1e-12: break
  return E
  

fbmd = tools.config["fbmd"]
binSize = float(tools.config["fbmd"]["binSize"])
initialTemp = float(tools.config["fbmd"]["initialTemp"])
minSamples = float(tools.config["fbmd"]["minSamples"]) * 10
minTemp = float(tools.config["fbmd"]["minTemp"])
maxTemp = float(tools.config["fbmd"]["maxTemp"])
kB = convVal =  0.008314462175 # kj/(mol*K)
dt = 0.002

gfp = open(prodDat, "rb")
chunk = gfp.read(12)
data = struct.unpack("=ffi", chunk)
binSize = data[1]
gamma = list(np.array(struct.unpack("f"*data[2], gfp.read(4*data[2]))))
    
minEnergyBin = data[0]
maxEnergyBin = data[0]+(data[1]*data[2])
bins = list(np.arange(minEnergyBin, maxEnergyBin, binSize))
hist = list(np.array([0 for i in range(len(bins))]))

nnn = int(len(bins)*.25)
for i in range(nnn):
 minEnergyBin -= binSize
 maxEnergyBin += binSize
 bins = [minEnergyBin] + bins + [maxEnergyBin]
 hist = [0] + hist + [0]
 gamma = [gamma[0]] + gamma + [gamma[-1]]

bins = np.array(bins)
hist = np.array(hist)
gamma = np.array(gamma)
bincenters = bins + (binSize*.5)

# range of the actual bias
first = np.where(gamma == gamma[0])[0][-1]
last = np.where(gamma == gamma[-1])[0][0]

fileinfo = tools.findProdData()
for seed in range(Nbakapara):
  seed = str(seed)
  print(seed)
  info = sorted(fileinfo[seed].items(), key=lambda i: i[1])

  for i, (file, startTime) in enumerate(info):
    ene = np.fromfile(file.replace(".xtc", ".fb.ene"), dtype=np.float32)
    if i < len(info)-1: 
      nextTime = info[i+1][1]
      nsteps = int((nextTime-startTime)/dt)+1
      if len(ene) <= nsteps: nsteps -= 1
      ene = ene[:nsteps]
    if len(ene):
      idxs = np.floor((ene-minEnergyBin)/binSize).astype(int)
      idxs = idxs[np.where((idxs > -1) & (idxs < len(bins)))[0]]
      np.add.at(hist, idxs, 1)

hist_pdf = hist/float(np.sum(hist))
hist_pdf_log = np.log(hist_pdf)

notInf = hist_pdf_log != -np.inf

hist_pdf_log[hist_pdf_log == -np.inf] = np.min(hist_pdf_log[hist_pdf_log != -np.inf])

for i in range(1, len(hist)-1): gamma[i] += ((convVal * initialTemp * (hist_pdf_log[i+1] - hist_pdf_log[i-1])) / (2.0 * binSize))

# reweighing

ll = np.where(hist > 0)[0][0]
ul = np.where(hist > 0)[0][-1]+1

w = hist/minSamples
w = np.clip(w, 0.0, 1.0)

t = np.poly1d(np.polyfit(bincenters, gamma, deg=2, w=w))
fake_gamma = t(bincenters)

n = np.where(w == 1)[0][0]
fake_gamma[:n] += gamma[n] - fake_gamma[n]
n = np.where(w == 1)[0][-1]
fake_gamma[n+1:] += gamma[n] - fake_gamma[n]

fake_gamma = (fake_gamma * (1-w)) + (gamma*w)

spline = scipy.interpolate.UnivariateSpline(bins, fake_gamma/(kB*initialTemp), s=0, k=2)


ad = spline.antiderivative()
ln_nE = ad(bins)

if not os.path.exists(target): os.mkdir(target)

infofl = ["#T E@Pmax gamma@Pmax"]

dT = 10

temps = {}
tempsOut = {}
tempsOut["energy-bins"] = list(bins[first:last-1])
tempsOut["internal"] = {"bins": list(bins), "fake_gamma": list(fake_gamma), "gamma": list(gamma)}

print("targetT, E, targetE, T, T-1, T+1")

for targetT in range(int(minTemp), int(maxTemp)+dT, dT):
  ln_PET = ln_nE - bins/(kB*targetT)
  
  tg = (initialTemp/targetT) / (kB*initialTemp)

  t = np.argmax(ln_PET)
  if targetT == minTemp: targetE = findPeakVal(tg, bins[t+1], binSize*.01)
  elif targetT == maxTemp: targetE = findPeakVal(tg, bins[t-1], binSize*.01)
  elif initialTemp/gamma[t] > initialTemp/targetT: targetE = findPeakVal(tg, bins[t-1], binSize)
  else: targetE = findPeakVal(tg, bins[t], binSize)
  
  mx_ln_PET = ad(targetE) - targetE/(kB*targetT)
  ln_PET -= mx_ln_PET
  
  tempsOut[targetT] = list(ln_PET[first:last-1])
  
  idx = np.argmax(ln_PET)
  print(targetT, bins[idx], targetE, initialTemp/fake_gamma[idx], initialTemp/fake_gamma[idx-1], initialTemp/fake_gamma[idx+1])
  
  infofl.append("%s %s %s %s"%(targetT, bins[idx], gamma[idx], fake_gamma[idx]))

  x, y = [], []
  for i in range(len(bins)):
    x.append(bins[i])
    y.append(ln_PET[i])

  entries = []
  for i in range(len(x)): entries.append("%s %s"%(x[i], repr(y[i])))
    
  temps[targetT] = [np.array(x), np.array(y)]


  open(target+"%s.pdf"%targetT, "w").write("\n".join(entries))

open(target+"info.txt", "w").write("\n".join(infofl))

open(target+"reweighting.json", "w").write(json.dumps(tempsOut))

mpl.rc('font', **{'size': 20})

plt.xlabel("E (kcal/mol)")
plt.ylabel("$\gamma$")

plt.plot(bins/4.184, fake_gamma, linewidth=.33)
plt.tight_layout()
plt.savefig(target+"gamma.png", dpi=600)
plt.close()

plt.xlabel("$E$ (kcal/mol)")
plt.ylabel("ln$P$")

edistr, = plt.plot(bins[notInf]/4.184, hist_pdf_log[notInf], linewidth=3.0, color='k')
k300, = plt.plot(temps[300][0]/4.184, temps[300][1], linewidth=3.0, label="300K", color='b')
k500, = plt.plot(temps[500][0]/4.184, temps[500][1], linewidth=3.0, label="500K", color='y')
k700, = plt.plot(temps[700][0]/4.184, temps[700][1], linewidth=3.0, label="700K", color='r')
lgd = plt.legend(handles=[k300, k500, k700], loc=3, bbox_to_anchor=(0., 1.02, 1., .102), ncol=3, mode="expand", borderaxespad=0., fontsize='small')

x1,x2,y1,y2 = plt.axis()

A = bins[np.where(hist > 0)[0][0]]/4.184
B = bins[np.where(hist > 0)[0][-1]]/4.184
dbs = (B-A) * .05

plt.axis((A - dbs, B + dbs, -22.5, 2.5))
plt.xticks(plt.xticks()[0][::2])

plt.tight_layout()
plt.savefig(target+"mcmd_distr.png", dpi=600, bbox_inches="tight")
plt.close()
    

asciiPlot(target+"mcmd_distr.txt", bins[notInf]/4.184, hist_pdf_log[notInf], "Energy distribution")

