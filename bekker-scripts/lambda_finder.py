import numpy as np, math, scipy, scipy.stats, argparse, os, sys

# example:
# python lambda_finder.py -f path/to/input.pdb -o path/to/output.pdb --with-mol2 path/to/compound.mol2 --eject-ligand

parser = argparse.ArgumentParser(description='Lambda finder input parser')
parser.add_argument('-f', help="Input pdb file", dest="pdb")
parser.add_argument('-o', help="Output pdb file", dest="outfile")
parser.add_argument('-receptorChains', help="List of receptor chains", dest="receptorChains", nargs="+")
parser.add_argument('-ligandChains', help="List of ligand chains", dest="ligandChains", nargs="+")

parser.add_argument('--with-mol2', help="Select a custom mol2 file", dest="mol2")
parser.add_argument('--eject-ligand', help="Translate the ligand into the bulk", dest="ejectLigand", const=True, default=False, action='store_const')
parser.add_argument('--bisector-lambda', help="Align the lambda with the bisector axis", dest="bisectorLambda", const=True, default=False, action='store_const')
parser.add_argument('--pocket-center', help="Select the pocket center as x,y,z", dest="pocketCenter")

parser.add_argument('--blocking-probe', help="Add blocking probe at x,y,z", dest="blockingProbes", action='append')

parser.add_argument('--near-vec', help="Near vector x,y,z", dest="nearVec")
parser.add_argument('--near-vec-val', help="Near vector dot product cutoff", dest="nearVecVal", type=float, default=0.5)

args = parser.parse_args()

pdb = args.pdb
outfile = args.outfile
receptorChains = args.receptorChains
ligandChains = args.ligandChains

mol2 = args.mol2
ejectLigand = args.ejectLigand
bisectorLambda = args.bisectorLambda
pocketCenter = args.pocketCenter

do_nearVec = False
if args.nearVec: 
  nearVec = np.array([float(j) for j in args.nearVec.strip().split(",")])
  nearVec /= np.linalg.norm(nearVec)
  do_nearVec = True
  
nearVecVal = args.nearVecVal

if args.blockingProbes: blockingProbes = [[float(j) for j in i.split(",")] for i in args.blockingProbes]
else: blockingProbes = []

def rotation_matrix(_axis, theta):
  """
  Return the rotation matrix associated with counterclockwise rotation about
  the given axis by theta radians.
  """
  _axis = np.asarray(_axis)
  _axis = _axis/math.sqrt(np.dot(_axis, _axis))
  a = math.cos(theta/2.0)
  b, c, d = -_axis*math.sin(theta/2.0)
  aa, bb, cc, dd = a*a, b*b, c*c, d*d
  bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
  return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)], [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)], [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

class Logger(object):
  def __init__(self):
    self.terminal = sys.stdout
    self.log = open((os.path.dirname(outfile)+"/lambda_finder.txt").strip("/"), "w")

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)  

sys.stdout = Logger()

print("This script was run using the following command:")
print(" ".join(sys.argv) + "\n")

dLambda = 1.0

ligand = []
heavyAtoms = []+blockingProbes

for line in open(pdb).readlines():
  if line[:4] != "ATOM": continue
  element = line[12:16].strip().strip("123")[0]
  chainName = line[21:22].strip()
  x = float(line[30:38].strip())
  y = float(line[38:46].strip())
  z = float(line[46:54].strip())
  if (receptorChains == None or chainName in receptorChains) and element != "H": heavyAtoms.append([x, y, z])
  elif ligandChains != None and chainName in ligandChains: ligand.append([x, y, z])
  
heavyAtoms = np.array(heavyAtoms)
  
if mol2:
  go = False
  for line in open(mol2).readlines():      
    if not line.strip(): continue
    if "@<TRIPOS>ATOM" in line: 
      go = True
      continue
    elif "@<TRIPOS>" in line: go = False
    
    if go:
      line = line.strip().split()
      x = float(line[2])
      y = float(line[3])
      z = float(line[4])
      
      ligand.append([x, y, z])
      
if pocketCenter: 
  pocketCenter = np.array([float(i) for i in pocketCenter.split(",")])
  ligandCenter = np.mean(ligand, axis=0)
else: ligandCenter = pocketCenter = np.mean(ligand, axis=0)

refPoints = []
data = []

prevVal = None

ligand = np.array(ligand)
lpc = np.mean(ligand, axis=0)
RG = np.sqrt(np.mean(np.linalg.norm(ligand-lpc, axis=1)**2))

cLambda = dLambda

refPoints.append([0.0, pocketCenter[0], pocketCenter[1], pocketCenter[2]])

d = RG + 10.0

print("Searching the space between %s and %s from the chosen pocket center"%(RG+2, d))

def outputGrid(grid, r, idx):
  out = "%s\n\n"%(len(grid))
  for i in range(len(grid)): 
    out += "%s %s %s %s\n"%(i == idx and "O" or "C", grid[i][0], grid[i][1], grid[i][2])
  open("r%s.xyz"%r, "w").write(out)
  #exit()
  

while True:
  samples = np.round(4 * math.pi * cLambda * cLambda).astype(int)
  print("%s %s"%(cLambda, samples), end="")
  
  offset = 2./samples
  increment = math.pi * (3. - math.sqrt(5.))
  
  grid = []
  for i in range(samples):
    y = ((i * offset) - 1) + (offset / 2);
    r = math.sqrt(1 - pow(y,2))

    phi = (i%samples) * increment

    x = math.cos(phi) * r
    z = math.sin(phi) * r

    if do_nearVec and np.dot([x, y, z], nearVec) < nearVecVal: continue
    
    grid.append([x*cLambda,y*cLambda,z*cLambda])
    
  grid = np.array(grid)
    
  grid += pocketCenter

  dists = np.array([np.min(np.linalg.norm(grid[i]-heavyAtoms, axis=1)) for i in range(len(grid))])
  
  # this keeps track of the previous position in order to minimize jumping...
  if len(refPoints):
    rp = np.array(refPoints[-1][1:])
    alt_dists = np.linalg.norm(grid-rp, axis=1)
    idx = np.argmax(dists-alt_dists)
  else: idx = np.argmax(dists)
  
  print(dists[idx])

  if dists[idx] > d: 
    #outputGrid(grid, cLambda, idx)
    break
  
  if dists[idx] > RG+2:
    #if len(refPoints) == 1: outputGrid(grid, cLambda, idx)
  
    gp = grid[idx]
    
    #print("[%s, %s, %s],"%(repr(gp[0]), repr(gp[1]), repr(gp[2])))
    
    refPoints.append([cLambda, gp[0], gp[1], gp[2]])
  
  cLambda += dLambda


out = "%s\n\n"%len(refPoints)
for i in refPoints: out += "C %s %s %s\n"%(i[1], i[2], i[3])
open(os.path.splitext(outfile)[0]+".xyz", "w").write(out)

refPoints = np.array(refPoints).T
  
X = scipy.stats.linregress(refPoints[0], refPoints[1])
Y = scipy.stats.linregress(refPoints[0], refPoints[2])
Z = scipy.stats.linregress(refPoints[0], refPoints[3])

axis = np.array([X[0], Y[0], Z[0]])
axis /= np.linalg.norm(axis)

zeroPoint = np.array([X[1], Y[1], Z[1]])

# set the zero point so that it aligns to the pocketCenter
zeroPoint += np.dot(pocketCenter-zeroPoint, axis)*axis

if ejectLigand: deltaLigandPos = zeroPoint-ligandCenter

#lambdaFar = refPoints[0][-1]
lambdaFar = cLambda
print("lambdaFar:", lambdaFar)

if bisectorLambda: 
  ori = np.array([1.0, 1.0, 1.0])
  
  centerPoint = np.mean(heavyAtoms, axis=0)
  farPoint = ((lambdaFar + RG + 4.0 + 9.0) * axis) + zeroPoint
  altPoint = farPoint + (centerPoint-zeroPoint)
  
  one = farPoint - zeroPoint
  one /= np.linalg.norm(one)
  two = altPoint - zeroPoint
  two /= np.linalg.norm(two)
  
  theta = np.dot(one, two)
  
  
  theta = np.arccos(np.dot(one, two))

  perp = np.cross(one, two)
  perp /= np.linalg.norm(perp)

  rmat = rotation_matrix(perp,theta)
  
  ori = np.dot(rmat, ori) + ori
else: ori = np.array([1.0, 0.0, 0.0])
ori /= np.linalg.norm(ori)

outPoint = axis*lambdaFar + zeroPoint
open(os.path.splitext(outfile)[0]+"-pre.path", "w").write("2\n%s,%s,%s\n%s,%s,%s\n"%(repr(zeroPoint[0]*.1), repr(zeroPoint[1]*.1), repr(zeroPoint[2]*.1), repr(outPoint[0]*.1), repr(outPoint[1]*.1), repr(outPoint[2]*.1)))

print("Aligning lambda with axis", ori)

theta = np.arccos(np.dot(axis, ori))

perp = np.cross(axis, ori)
perp /= np.linalg.norm(perp)

rmat = rotation_matrix(perp,theta)

startPoint = zeroPoint
endPoint = np.dot(rmat, (axis*lambdaFar))+zeroPoint
open(os.path.splitext(outfile)[0]+".path", "w").write("2\n%s,%s,%s\n%s,%s,%s\n"%(repr(startPoint[0]*.1), repr(startPoint[1]*.1), repr(startPoint[2]*.1), repr(endPoint[0]*.1), repr(endPoint[1]*.1), repr(endPoint[2]*.1)))

out = ""
for line in open(pdb).readlines():
  if line[:4] != "ATOM" and line[:6] != "HETATM": continue
  if len(line) > 75: element = line[77:].strip()
  
  xyz = np.array([float(line[30:38].strip()), float(line[38:46].strip()), float(line[46:54].strip())])
  
  # move the ligand outside the pocket...
  chainName = line[21:22].strip()
  if ejectLigand and ligandChains != None and chainName in ligandChains: xyz += deltaLigandPos + axis*lambdaFar
  x, y, z = np.dot(rmat, xyz-zeroPoint)+zeroPoint

  out += line[:30] + "{:8.3f}{:8.3f}{:8.3f}".format(x, y, z) + line[54:]
  
open(outfile, "w").write(out)

if mol2:
  out = ""
  for line in open(mol2).readlines():
    if "@<TRIPOS>ATOM" in line: 
      go = True
      out += line
      continue
    elif "@<TRIPOS>" in line: go = False
    
    if go and line.strip():
      tmp = line.strip().split()
      xyz = np.array([float(tmp[2]), float(tmp[3]), float(tmp[4])])
      
      if ejectLigand: xyz += deltaLigandPos + axis*lambdaFar
      x, y, z = np.dot(rmat, xyz-zeroPoint)+zeroPoint
      tmp[2] = "{:10.4f}".format(x)
      tmp[3] = "{:10.4f}".format(y)
      tmp[4] = "{:10.4f}".format(z)
      line = " ".join(tmp)+"\n"
    out += line
      
  open(os.path.splitext(outfile)[0]+".mol2", "w").write(out)

