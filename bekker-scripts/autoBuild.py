# Contact gertjan.bekker@protein.osaka-u.ac.jp for more info

# this script automatically builds a protein+ligand structure and does the following:
# - build topology (protein + ligand)
# - build box + waters + ions
# - energy minimization (2x; once on waters+hydrogens, once more on the entire system)
# - nvt 100ps
# - npt 100ps

# note: the script automatically continues where a previous run left off

# usage:
# python autoBuild.py path/to/config.yml

# to reset everything (i.e. to rerun the building process), use the following command:
# python autoBuild.py path/to/config.yml --rebuild

# note: when including ligand, make sure that the amber executables have been added to the path

import os, sys, subprocess, time, yaml, numpy as np, fnmatch, shutil, math, mdtraj, glob
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology

conf_file = sys.argv[1]

class Logger(object):
  def __init__(self):
    self.terminal = sys.stdout
    self.logfile = (os.path.dirname(conf_file)+"/build.txt").strip("/")
    self.log = open(self.logfile, "--rebuild" in sys.argv and "w" or "a")

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)

logger = Logger()
    
def execCommand(cmd): subprocess.call(cmd, stdout=logger.log, stderr=logger.log, shell=True)

# load config & init
CWD = os.path.abspath(os.path.dirname(os.getcwd()+os.sep+conf_file))+os.sep
conf_file = os.path.basename(conf_file)
os.chdir(CWD)

config = yaml.safe_load(open(CWD+conf_file))

gmx = config["gmx-path"]
gmx_d = config.get("gmx_d-path", gmx)

inpdb = config.get("protein")
inmol2 = config.get("ligand")
if inmol2 is None: inmol2 = []
if type(inmol2) == str: inmol2 = [inmol2]
includes = config.get("includes", [])

extraGroups = config.get("extra-groups", [])
extraGroups_empty = len(extraGroups) == 0

ntomp = config.get("ntomp", 1)
ntmpi = config.get("ntmpi", 1)
mdrunoptions = config.get("mdrunoptions", "")

r = config.get("box-r", 1.0)

customScripts = []
customClean = []
customSkip = config.get("custom-skip", [])
for i in config.get("custom-scripts", []):
  if len(i) > 2: customClean += i[2]
  customScripts.append(i[:2])

def executeCustomScripts(stage):
  for scriptStage, script in customScripts:
    if scriptStage == stage: 
      execCommand("python3 %s"%script)
      return True
  return False

if "lambda-file" in config:
  lambdaPath = np.array([[float(i) for i in line.strip().split(",")] for line in open(config["lambda-file"]).readlines()[1:]])
  lambdaAxis = lambdaPath[1]-lambdaPath[0]
  lambdaAxis /= np.linalg.norm(lambdaAxis)
  
def loadIndex(fname):
  index = {}
  for line in open(fname).readlines():
    line = line.strip()
    if not line: continue
    if line[0] == "[":
      name = line.replace("[", "").replace("]", "").strip()
      index[name] = []
    else: index[name] += [int(i)-1 for i in line.split()]
  for k,v in index.items(): index[k] = np.array(v)
  return index

def chunkList(l, n):
  for i in range(0, len(l), n): yield l[i:i+n]

def prepComres(name):
  receptorGroup = config["comres"]

  pdb = open(CWD+name+".gro").readlines()[2:-1]
  index = loadIndex(CWD+"index.ndx")
  
  atoms = set(config.get("comres-atoms", ["CA"]))
  
  coords = []
  for a in index[receptorGroup]:
    line = pdb[a]
    if not line[11:15].strip() in atoms: continue
    idx = int(line[15:20].strip())-1
    x = line[20:28].strip()
    y = line[28:36].strip()
    z = line[36:44].strip()
    coords.append("%s\t%s\t%s\t%s"%(idx, x, y, z))
  open(CWD+name+".comres", "w").write("%s\n"%len(coords) + "\n".join(coords))

def customIons():
  if os.path.exists("solv_ions.gro"): return
  
  ionconf = config["custom-ions"]
  if type(ionconf) != list: ionconf = [ionconf]

  ingro = "complex_solv.gro"
  tpr = "ions_0.tpr"
  if os.path.exists(tpr): os.remove(tpr)
  execCommand(gmx+" grompp -f ions.mdp -c %s -p topol.top -o %s"%(ingro, tpr))
  charge = 0
  tmp = open(logger.logfile).read().split("System has non-zero total charge:")
  if len(tmp) > 1: charge = np.round(float(tmp[-1].splitlines()[0].strip())).astype(np.int32)
  
  import mdtraj
  for idx, ion in enumerate(ionconf):
    final = idx == len(ionconf)-1
    conc = ion.get("ion-concentration", 0.1)
      
    pq = ion.get("positive-ion-charge", 1)
    nq = ion.get("negative-ion-charge", -1)
    pname = ion.get("positive-ion-name", "NA")
    nname = ion.get("negative-ion-name", "CL")
    
    struc = mdtraj.load("complex_solv.gro")
    Nwat = len(struc.top.select("water and symbol != H"))
    waterModel = config.get('water-model', 'tip3p')
    if waterModel == "tip3p": pass
    elif waterModel == "tip4p": Nwat *= 0.5
    pre_pp, pre_nn = len(struc.top.select("resn %s"%pname)), len(struc.top.select("resn %s"%nname))
    old_pp, old_nn = 0, 0
    pp = ion.get("positive-ion-n", 0)
    nn = ion.get("negative-ion-n", 0)
    if pp == 0 and nn == 0:
      N = 0
      while True:
        # calculate number of ions to add
        nn = pp = np.round(((Nwat-(old_pp+old_nn)) * conc) / 55.4)

        chargeratio = np.abs(pq, dtype=np.float32)/np.abs(nq, dtype=np.float32)
        
        if chargeratio > 1: nn *= chargeratio
        else: pp /= chargeratio
        
        nn = np.round(nn).astype(np.int32)
        pp = np.round(pp).astype(np.int32)
        
        # subtract ions already part of the system from the concentration amount
        pp -= pre_pp
        nn -= pre_nn

        if not final and conc != 0.0: break # only neutralize using the final ion setting...

        while True:
          chargebalance = np.round((nn*nq)+(pp*pq)+charge).astype(np.int32)
          if chargebalance < 0 and pq: pp += 1
          elif chargebalance > 0 and nq: nn += 1
          else: break
          
        if pq == 0: 
          pp = 0
          nn -= 1
        if nq == 0: 
          nn = 0
          pp -= 1
          
        if old_pp == pp and old_nn == nn: break
        N += 1
        if N > 1000: break # give up
        old_pp, old_nn = pp, nn
      
    ingro = "ions_%s.gro"%(idx-1)
    tpr = "ions_%s.tpr"%idx
    outgro = "ions_%s.gro"%idx
    if idx == 0: ingro = "complex_solv.gro"
    if final: outgro = "solv_ions.gro"

    if os.path.exists(outgro): os.remove(outgro)
    if idx > 0: 
      if os.path.exists(tpr): os.remove(tpr)
      execCommand(gmx+" grompp -f ions.mdp -c %s -p topol.top -o %s"%(ingro, tpr))

    genion_settings = "-pname %s -nname %s -np %s -nn %s -pq %s -nq %s"%(pname, nname, pp, nn, pq, nq)
    execCommand("echo 'SOL' | "+gmx+" genion -s %s -o %s -p topol.top "%(tpr, outgro)+genion_settings)
    
    charge += pp*pq + nn*nq # update total charge...
    
    time.sleep(1)

def buildLigandTop(mol2file):
  import parmed
  
  dirname = os.path.splitext(mol2file)[0]
  
  mol2file = os.path.abspath(mol2file)
  os.mkdir(dirname+".autoBuild")
  os.chdir(dirname+".autoBuild")

  execCommand(f"antechamber -fi mol2 -i {mol2file} -fo mol2 -o temp.mol2 -at gaff2")

  gaffParams = False
  execCommand("parmchk2 -i temp.mol2 -o temp.frcmod -f mol2 -p $AMBERHOME/dat/leap/parm/parm10.dat -pf 1")
  data = open("temp.frcmod").read()
  if "ATTN" in data:
    open("temp.frcmod", "w").write("\n".join([line for line in data.splitlines() if not "ATTN" in line]))
    execCommand("parmchk2 -i temp.mol2 -o temp_gaff.frcmod -f mol2 -s 2")
    gaffParams = True

  saa = """
  source leaprc.gaff2
  source oldff/leaprc.ff99SBildn
  LIG=loadmol2 temp.mol2"""+(gaffParams and "\nloadamberparams temp_gaff.frcmod\n" or "")+"""
  loadamberparams temp.frcmod
  saveamberparm LIG ligand.prmtop ligand.inpcrd
  Check LIG
  quit
  """
  saa = saa
  open("LIG.tleap", "w").write(saa)
  execCommand("tleap -f LIG.tleap")
  if os.path.exists("LIG.leap.log"): os.remove("LIG.leap.log")
  os.rename("leap.log", "LIG.leap.log")

  # load with parmed
  system = parmed.amber.AmberParm("ligand.prmtop")
  system.load_rst7("ligand.inpcrd")
  system.save("temp.top")
  
  OK = True
  output = []
  catname = None
  for line in open("temp.top"):
    _line = line.strip()
    
    if line.startswith("["):
      catname = line.split("[")[1].split("]")[0].strip()
      OK = not (catname in ["defaults", "system", "molecules"])
    elif catname == "molecules" and _line[:1] != ";" and _line:
      ligname = line.split()[0]
    if OK: output.append(line)
  
  open("%s.itp"%ligname, "w").write("".join(output))
  
  os.remove("temp.top")
  
  system.save("%s.gro"%ligname)

  os.chdir("..")
  
  return [ligname, "%s.autoBuild/%s.itp"%(dirname, ligname), "%s.autoBuild/%s.gro"%(dirname, ligname)]
  
def prepIncludes(includes):
  if len(includes) == 0: return []
  elif len(includes) == 1: return [[open(includes[0][0]).read(), includes[0][1], includes[0][2], includes[0][3]]]

  import parmed
  parmed.gromacs.GROMACS_TOPDIR = os.path.abspath(os.path.dirname(gmx)+"/../../")+"/share/top/"
  includes2 = [["", None, "parameters", 0]]
  
  atomtypes = {}
  
  for itp, gro, ligname, Nmols in includes:
    itpData, mode = [], None
    for line in open(itp).readlines():
      if line.startswith("["): 
        mode = line.strip()[1:-1].strip()
      if mode == "atomtypes":
        if not line.startswith("[") and not line.startswith(";") and line.strip():
          tmp = line.split()
          atn = tmp[0]
          if atn in atomtypes:
            tmp2 = atomtypes[atn].split()
            if np.abs(float(tmp[5])-float(tmp2[5])) > 1e-6 or np.abs(float(tmp[6])-float(tmp2[6])) > 1e-6:
              logger.log.write("ERROR: duplicate uses of atomtypes with the same name and different paremeters\n")
              exit()
          else: atomtypes[atn] = line
      else: itpData.append(line)
    
    includes2.append(["".join(itpData), gro, ligname, Nmols])

  includes2[0][0] = "[ atomtypes ]\n" + "".join(atomtypes.values())+"\n"

  return includes2 # itpData, gro, ligname, Nmols
  

def autoBuild():
  global inpdb
  
  if "--rebuild" in sys.argv or "--clean" in sys.argv:
    killFiles = ["protein_proc.gro", "complex.gro", "complex_solv.gro", "prebox.gro", "midbox.gro", "box.gro", "solv_ions.gro", "topol.top", "*.tpr", "#*", "mdout.mdp", "*.log", "*.edr", "*.trr", "*.xtc", "em0.gro", "em1.gro", "em2.gro", "nvt.gro", "npt.gro", "*.cpt", "*.ndx", "*.acpype", "*.comres", "*.itp", "*.autoBuild"]
    killFiles += customClean
    skipFiles = ["leap.log", "amber99sb-ildn-ions.minimal.itp"] + customSkip
    for itp, gro, name, N in includes:
      skipFiles.append(itp)
      skipFiles.append(gro)
    if config.get("equil-file"): killFiles.append(config.get("equil-file").replace(".mdp", ".*"))
    for fl in os.listdir("."):
      for fn in killFiles:
        if fnmatch.fnmatch(fl, fn) and fl != config.get("equil-file") and not fl in skipFiles:
          print(fl, fn)
          if os.path.isdir(fl):
            try: shutil.rmtree(fl)
            except: pass
          else:
            try: os.remove(fl)
            except: pass
          break
    if config.get("system"): execCommand("cp -a topol.top.bak topol.top")
          
  executeCustomScripts("clean")
  if config.get("kill-stage") == "clean" or "--clean" in sys.argv: return
  
  if not os.path.exists("protein_proc.gro") and inpdb: execCommand(f"{gmx} pdb2gmx -f {inpdb} -o protein_proc.gro -water {config.get('water-model', 'tip3p')} -ff {config['force-field']} {config['pdb2gmx-param']}")
  
  if not inpdb and config.get("ingmx") and not os.path.exists("protein_proc.gro"):
    tpl, gro, others = config.get("ingmx")
    shutil.copy(tpl, "topol.top")
    shutil.copy(gro, "protein_proc.gro")
    if type(others) == list:
      for i in others: 
        if "*" in i:
          for j in glob.glob(i): shutil.copy(j, ".")
        else: shutil.copy(i, ".")
    else: shutil.copy(others, ".")
    inpdb = True
  
  if (len(inmol2) or len(includes)) and not os.path.exists("complex.gro"):
    for lig in inmol2:
      if not config.get("acpype"):
        ligname, itp, gro = buildLigandTop(lig)
        includes.append([itp, gro, ligname, 1])
      else:
        print("echo '%s' | bash"%("python3 "+config["acpype-path"]+" -i "+lig+" -c user"))
        execCommand("echo '%s' | bash"%("python3 "+config["acpype-path"]+" -i "+lig+" -c "+config.get("acpype-charge", "user")+" -a "+config.get("acpype-gaff", "gaff")))
        ligname = os.path.basename(os.path.splitext(lig)[0])
        includes.append([ligname+".acpype/"+ligname+"_GMX.itp", ligname+".acpype/"+ligname+"_GMX.gro", ligname, 1])
    if not inpdb: 
      top = """;Generated by autoBuild.py

; Include forcefield parameters
#include "%s.ff/forcefield.itp"

; Include water topology
#include "%s.ff/tip3p.itp"

#ifdef POSRES_WATER
; Position restraint for each water oxygen
[ position_restraints ]
;  i funct       fcx        fcy        fcz
   1    1       1000       1000       1000
#endif

; Include topology for ions
#include "%s.ff/ions.itp"

[ system ]
; Name
Ligand

[ molecules ]
; Compound        #mols
"""%(config["force-field"], config["force-field"], config["force-field"])
      open("topol.top", "w").write(top)
    
    if inpdb: dataR = open("protein_proc.gro").read().rstrip().splitlines()
    else: dataR = ["title", "0", "   0.00000   0.00000   0.00000"]
    NOA = int(dataR[1].strip())
    
    topAdd = ""
    topNames = ""
    
    lastIdx = int(dataR[-2][:5].strip())

    extraGroup = ["Protein"]
    for itpData, gro, ligname, Nmols in prepIncludes(includes):
      with open("%s.itp"%ligname, "w") as fp: fp.write(itpData)
      
      if gro == None: 
        topAdd += """; Include extra parameters
#include "%s.itp"
"""%ligname
        continue

      dataL = open(gro).read().rstrip().splitlines()
      NOLA = int(dataL[1].strip())
      groName = dataL[2][5:10].strip()
      extraGroup.append(groName)
      
      # see if there are any waters (HOH/SOL) and place the ligand BEFORE these...
      insIdx = -1
      for idx, line in enumerate(dataR):
        if line[5:8] == "HOH" or line[5:8] == "SOL":
          insIdx = idx
          break
          
      if insIdx == -1: insIdx = len(dataR)-1
      
      prevId = None
      
      for i in dataL[2:-1]:
        id = int(i[:5].strip())
        if id != prevId:
          prevId = id
          lastIdx += 1
        if config.get("renum-includes"): dataR.insert(insIdx, str(lastIdx).rjust(5) + i[5:])
        else: dataR.insert(insIdx, i)
        insIdx += 1
      NOA += NOLA
      
      topAdd += """
; Include ligand topology
#include "%s.itp"

; Ligand position restraints
#ifdef POSRES
#include "posre_%s.itp"
#endif
"""%(ligname, ligname)

      topNames += ligname.ljust(16) + str(Nmols).rjust(5) + "\n"
      
      if os.path.exists("posre_%s.itp"%ligname): os.remove("posre_%s.itp"%ligname)
      execCommand("echo 0 | "+gmx+" genrestr -f %s -o posre_%s.itp -fc 1000 1000 1000"%(gro, ligname))

      nonH = []
      for line in dataL[2:-1]:
        if line[10:15].strip()[0] != "H": nonH.append(int(line[15:20].strip()))
      nonH = nonH[:int(len(nonH)/Nmols)] # deal with multiple molecules...
      
      go = False
      out = []
      for line in open("posre_%s.itp"%ligname, "rb").readlines():
        line = line.decode("utf-8", "ignore")
        if go and line[:1] != ";":
          if int(line[:5].strip()) in nonH: out.append(line)
        else: out.append(line)
        if "[ position_restraints ]" in line: go = True
  
      open("posre_%s.itp"%ligname, "w").write("".join(out))
    
    dataR[1] = str(NOA).rjust(5)
    open("complex.gro", "w").write("\n".join(dataR))
    
    topol = open("topol.top").read()
    
    if '#include "%s.ff/forcefield.itp"\n'%config["force-field"] in topol:
      topAdd = '#include "%s.ff/forcefield.itp"\n'%config["force-field"] + topAdd
      topol = topol.replace('#include "%s.ff/forcefield.itp"'%config["force-field"], topAdd)
    else: 
      topol = (topAdd+"\n\n[ moleculetype ]").join(topol.split("[ moleculetype ]", 1))
   
    # add these before SOL
    
    topol = topol.splitlines()
    
    moleculesMode = False
    insIdx = len(topol) # if there are no waters => just put it at the end..
    for idx, line in enumerate(topol):
      if moleculesMode:
        if line[:4] == "SOL ":
          insIdx = idx
          break
      if line.replace("[", "").replace("]", "").strip() == "molecules": moleculesMode = True
   
    topol.insert(insIdx, topNames.strip())
    topol = "\n".join(topol)

    execCommand("mv topol.top topol.top.bak")
    
    open("topol.top", "w").write(topol.strip()+"\n")
    
    if inpdb and extraGroups_empty: extraGroups.append(extraGroup)
    
  elif inmol2 or includes:
    for lig in inmol2:
      if not config.get("acpype"):
        dirname = os.path.splitext(mol2file)[0]
        ligname = os.path.basename(glob.glob("%s.autoBuild/*.gro"%(dirname))[0])[:-4]
        includes.append(["%s.autoBuild/%s.itp"%(dirname, ligname), "%s.autoBuild/%s.gro"%(dirname, ligname), ligname, 1])
      else:
        includes.append([lig+".acpype/"+lig+"_GMX.itp", lig+".acpype/"+lig+"_GMX.gro", lig, 1])

    if inpdb and extraGroups_empty:
      extraGroup = ["Protein"]
      for itp, gro, ligname, Nmols in includes: 
        if not gro: continue
        extraGroup.append(open(gro).read().rstrip().splitlines()[2][5:10].strip())
      extraGroups.append(extraGroup)
  
  executeCustomScripts("complex")
  if config.get("kill-stage") == "complex": return

  boxIn = config.get("system", len(includes) and "complex.gro" or "protein_proc.gro")
  
  if config.get("system"):
    if config.get("system"): execCommand("cp -a topol.top topol.top.bak")
    
    topol = open(config.get("system-top", "topol.top")).read()
    if ";;; posres" in topol:
      topol = topol.replace(';;; posres', """#ifdef POSRES
#include "posre_system.itp"
#endif""")
   
      topol = topol.rstrip()+"\n"
    
      open("topol.top", "w").write(topol)
    
      if os.path.exists("posre_system.itp"): os.remove("posre_system.itp")
      execCommand("echo 0 | "+gmx+" genrestr -f %s -o posre_system.itp -fc 1000 1000 1000"%(boxIn,))
    
      data = open(boxIn).read().rstrip().splitlines()
    
      nonH = []
      for line in data[2:-1]:
        if line[10:15].strip()[0] != "H": nonH.append(int(line[15:20].strip()))
      
      go = False
      out = []
      for line in open("posre_system.itp", "rb").readlines():
        line = line.decode("utf-8", "ignore")
        if go and line[:1] != ";":
          if int(line[:5].strip()) in nonH: out.append(line)
        else: out.append(line)
        if "[ position_restraints ]" in line: go = True
  
      open("posre_system.itp", "w").write("".join(out))
    else: open("topol.top", "w").write(topol)
      
  
  # box
  if not os.path.exists("box.gro"): 
  
    if "pre-editconf" in config:
      execCommand(config["pre-editconf"].get("pre", "")+gmx+" editconf -f "+boxIn+" -o prebox.gro "+config["pre-editconf"].get("post", ""))
      boxIn = "prebox.gro"
  
      if executeCustomScripts("pre-editconf"): boxIn = "box-preEC.gro"
    
    boxSize = config.get("box-size", "-d "+str(r*2./3))

    atoms = []
    for line in open(CWD+boxIn).readlines()[2:-1]:
      x, y, z = line[21:].split()[:3]
      atoms.append([float(x), float(y), float(z)])
    atoms = np.array(atoms)
    
    aT = atoms.T
    xLen = (np.max(aT[0])-np.min(aT[0]))
    yLen = (np.max(aT[1])-np.min(aT[1]))
    zLen = (np.max(aT[2])-np.min(aT[2]))
    
    atoms -= np.array([np.min(aT[0]), np.min(aT[1]), np.min(aT[2])]) + np.array([xLen*.5, yLen*.5, zLen*.5])
    gc_offset = np.mean(atoms, axis=0)
    
    if config.get("cylinder-box"): xLen += config["cylinder-box"]

    if config.get("lambda-shift-x"): xLen += config.get("lambda-shift-x")
    if config.get("lambda-shift-y"): yLen += config.get("lambda-shift-y")
    if config.get("lambda-shift-z"): zLen += config.get("lambda-shift-z")
    
    xLen += r*1.5
    yLen += r*1.5
    zLen += r*1.5
    
    centerX = xLen * 0.5
    centerY = yLen * 0.5
    centerZ = zLen * 0.5
    
    # correct for geometrical center which is not in the center of the box
    centerX += gc_offset[0]
    centerY += gc_offset[1]
    centerZ += gc_offset[2]
    
    if config.get("lambda-shift"):
      lambdaShift = config.get("lambda-shift")
      centerX -= lambdaShift * lambdaAxis[0]
      centerY -= lambdaShift * lambdaAxis[1]
      centerZ -= lambdaShift * lambdaAxis[2]
      
    if config.get("lambda-shift-x"): centerX -= config.get("lambda-shift-x")
    if config.get("lambda-shift-y"): centerX -= config.get("lambda-shift-y")
    if config.get("lambda-shift-z"): centerX -= config.get("lambda-shift-z")
        
    # add additional box (this doesn't affect the center; assuming lambda has been aligned with x, where the pocket is on the positive side)
    if config.get("box-x"): xLen += config.get("box-x")*.1
    
    if config.get("cylinder-box"): centerX -= config["cylinder-box"]*.5 
    centerX += config.get("center-dx", 0.0)*.5
    centerZ += config.get("center-dy", 0.0)*.5
    centerY += config.get("center-dz", 0.0)*.5
    
    boxSize = "-box %s %s %s -center %s %s %s"%(xLen, yLen, zLen, centerX, centerY, centerZ)
    
    if config.get("box-size"): boxSize = config["box-size"]
    
    boxSize = config.get("box-shape", "triclinic") + " " + boxSize
    
    boxOut = "box.gro"
    
    if "translate-group" in config or "center-group" in config: boxOut = "midbox.gro"
    
    if config.get("box-size") and config.get("center"):
      import mdtraj
      struc = mdtraj.load(boxIn)
      mx = np.max(struc.xyz, axis=1)[0]
      mn = np.min(struc.xyz, axis=1)[0]
      sz = (mx-mn)*.5
      boxSize += " -center %s %s %s"%(sz[0], sz[1], sz[2])
    
    execCommand(config.get("editconf-pre", "")+gmx+" editconf -f "+boxIn+" -o "+boxOut+" -c -bt "+boxSize)
    
    if "translate-group" in config or "center-group" in config: 
      import mdtraj
      struc = mdtraj.formats.gro.load_gro(boxOut)
      if "center-group" in config:
        sel = struc.top.select(config["center-group"])
        mn = np.min(struc.xyz[0, sel], axis=0)
        mx = np.max(struc.xyz[0, sel], axis=0)
        struc.xyz[0, sel] += ((struc.unitcell_lengths-(mx-mn))*.5)-mn
      if "translate-group" in config:
        sel = struc.top.select(config["translate-group"]["selection"])
        struc.xyz[0, sel] += np.array(config["translate-group"]["relative"])
      struc.save_gro("box.gro")

    if "lambda-file" in config:
      oldCenter, newCenter = None, None
      for line in open("build.txt").readlines():
        if line[:17] == "    center      :": oldCenter = [float(i) for i in line[18:].split()[:-1]]
        if line[:17] == "new center      :": newCenter = [float(i) for i in line[18:].split()[:-1]]
    
      tl = np.array(newCenter)-np.array(oldCenter)
      
      out = [""]
      for line in open(config.get("lambda-file")).readlines()[1:]:
        nc = np.array([float(i) for i in line.strip().split(",")]) + tl
        out.append("%s,%s,%s"%(repr(nc[0]), repr(nc[1]), repr(nc[2])))
      out[0] = str(len(out)-1)
      
      open("simple.path", "w").write("\n".join(out))

  executeCustomScripts("box")
  if config.get("kill-stage") == "box": return
  
  # water
  if not os.path.exists("complex_solv.gro"): execCommand(gmx+" solvate -cp box.gro -cs %s -p topol.top -o complex_solv.gro "%(config.get("solvate-file", "spc216.gro")) + config.get("solvate-param", ""))
  
  executeCustomScripts("solvate")
  if config.get("kill-stage") == "solvate": return
  
  # implement alternative ions version that depends on custom code (which can set the proper ion concentration, unlike genion...)
  
  # ions
  if config.get("custom-ions"): customIons()
  else:
    if not os.path.exists("ions.tpr"): execCommand(gmx+" grompp -f ions.mdp -c complex_solv.gro -p topol.top -o ions.tpr")
  
    if not os.path.exists("solv_ions.gro"): 
      genion_settings = config.get("genion", "-pname NA -nname CL -neutral -conc 0.1")
      execCommand("echo 'SOL' | "+gmx+" genion -s ions.tpr -o solv_ions.gro -p topol.top "+genion_settings)
      time.sleep(1)
    
  executeCustomScripts("ions")
  if config.get("kill-stage") == "ions": return
  
  # add support for atom ranges...
  if not os.path.exists("index.ndx"): 
    execCommand("echo q | "+gmx+" make_ndx -f solv_ions.gro -o index.ndx")
    
    id, index, groupnames = "", {}, set()
    for line in open("index.ndx").readlines():
      if line[:1] == "[":
        id = line.replace("[", "").replace("]", "").strip()
        groupnames.add(id)
        index[id] = []
        continue
      else: index[id] += [int(i) for i in line.split()]
      
    for groups in extraGroups:
      tmp = []
      for group in groups: tmp += index[group]
      index["_".join(groups)] = sorted(tmp)
      groupnames.add("_".join(groups))
    
    if "group-script" in config: 
      if not os.getcwd() in sys.path: sys.path.append(os.getcwd())
      __import__(config["group-script"]).update_groups(index, groupnames)
      
    manualGroups = config.get("manual-groups", [])
    if len(manualGroups):
      import mdtraj
      struc = mdtraj.load("solv_ions.gro")
      for name, sel in manualGroups: 
        index[name] = (struc.top.select(sel)+1).tolist()
        groupnames.add(name)
      
    for g1, g2, g3 in config.get("merge-add-groups", []):
      index[g3] = sorted(index[g1] + index[g2])
      groupnames.add(g3)
      
    for g1, g2, g3 in config.get("merge-groups", []):
      groupnames.remove(g1)
      groupnames.remove(g2)
      index[g3] = sorted(index[g1] + index[g2])
      groupnames.add(g3)
      
    out = []
    for k in groupnames:
      v = index[k]
      out.append("[ %s ]"%k)
      for sl in chunkList(v, 15): out.append(" ".join([str(i) for i in sl]))

    open("index.ndx", "w").write("\n".join(out)+"\n")
    
  executeCustomScripts("index")
  if config.get("kill-stage") == "index": return

  if config.get("do-em0") and os.path.exists("em0.mdp"):
    if not os.path.exists("em0.tpr"): execCommand(gmx_d+" grompp -f em0.mdp -c solv_ions.gro -r solv_ions.gro -p topol.top -o em0.tpr")
    if not os.path.exists("em0.gro"): execCommand(gmx_d+" mdrun -deffnm em0 -ntmpi %s -ntomp %s -notunepme %s"%(ntmpi, ntomp, mdrunoptions))
    if not os.path.exists("em1.tpr"): execCommand(gmx+" grompp -f em1.mdp -c em0.gro -t em0.trr -r solv_ions.gro -p topol.top -o em1.tpr")
  elif not os.path.exists("em1.tpr"): execCommand(gmx+" grompp -f em1.mdp -c solv_ions.gro -r solv_ions.gro -p topol.top -o em1.tpr")
  if not os.path.exists("em1.gro"): execCommand(gmx+" mdrun -deffnm em1 -ntmpi %s -ntomp %s -notunepme %s"%(ntmpi, ntomp, mdrunoptions))
  
  executeCustomScripts("em1")
  if config.get("kill-stage") == "em1": return

  if not os.path.exists("em2.tpr"): execCommand(gmx+" grompp -f em2.mdp -c em1.gro -t em1.trr -p topol.top -o em2.tpr")
  if not os.path.exists("em2.gro"): execCommand(gmx+" mdrun -deffnm em2 -ntmpi %s -ntomp %s -notunepme %s"%(ntmpi, ntomp, mdrunoptions))
  
  executeCustomScripts("em2")
  if config.get("kill-stage") == "em2": return
  
  if not os.path.exists("nvt.tpr"): 
    if config.get("restrain-initial"): execCommand(gmx+" grompp -f nvt.mdp -c em2.gro -r solv_ions.gro -t em2.trr -p topol.top -n index.ndx -o nvt.tpr")
    else: execCommand(gmx+" grompp -f nvt.mdp -c em2.gro -r em2.gro -t em2.trr -p topol.top -n index.ndx -o nvt.tpr")
  
  if not os.path.exists("nvt.gro"): execCommand(gmx+" mdrun -deffnm nvt -ntmpi %s -ntomp %s -notunepme %s"%(ntmpi, ntomp, mdrunoptions))
  
  executeCustomScripts("nvt")
  if config.get("kill-stage") == "nvt": return
  
  if not os.path.exists("npt.tpr"): 
    if config.get("restrain-initial"): execCommand(gmx+" grompp -f npt.mdp -c nvt.gro -r solv_ions.gro -t nvt.cpt -n index.ndx -o npt.tpr")
    else: execCommand(gmx+" grompp -f npt.mdp -c nvt.gro -r em2.gro -t nvt.cpt -n index.ndx -o npt.tpr")
  if not os.path.exists("npt.gro"): execCommand(gmx+" mdrun -deffnm npt -ntmpi %s -ntomp %s -notunepme %s"%(ntmpi, ntomp, mdrunoptions))
  
  if "comres" in config: prepComres("npt")
  
  executeCustomScripts("npt")
  if config.get("kill-stage") == "npt": return
  
  # add some code to figure out the COM of the ligand in em2.gro...
  if inmol2 and "lambda-file" in config:
    import mdtraj
    struc = mdtraj.load(CWD+"em2.gro")
    index = loadIndex(CWD+"index.ndx")
    ligandCOM = np.array([0., 0., 0., 0.])
    
    for i in index[config["lig-name"]]:
      ligandCOM[:3] += struc.xyz[0,i]*struc.top.atom(i).element.mass
      ligandCOM[3] += struc.top.atom(i).element.mass
    ligandCOM = ligandCOM[:3] / ligandCOM[3]

    open(CWD+"COM.path", "w").write("2\n%s,%s,%s\n%s,%s,%s"%(repr(ligandCOM[0]), repr(ligandCOM[1]), repr(ligandCOM[2]), repr(ligandCOM[0]), repr(ligandCOM[1]), repr(ligandCOM[2])))

  if config.get("equil-file"): 
    base = config.get("equil-file").replace(".mdp", "")
    if not os.path.exists(base+".tpr"): execCommand(gmx+" grompp -f %s.mdp -c npt.gro -r em2.gro -t npt.cpt -p topol.top -n index.ndx -o %s.tpr"%(base, base))
    if not os.path.exists(base+".gro"): execCommand(gmx+" mdrun -deffnm %s -ntmpi %s -ntomp %s -notunepme %s"%(base, ntmpi, ntomp, mdrunoptions))
    if "comres" in config: prepComres(base)
    
  executeCustomScripts("FINAL")
    
autoBuild()

# clean up gromacs backup files
for fl in os.listdir("."):
  if fnmatch.fnmatch(fl, "#*"):
    try: os.remove(fl)
    except: pass
