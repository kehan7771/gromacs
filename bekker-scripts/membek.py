import argparse, subprocess

parser = argparse.ArgumentParser(description='Membrane embedding script')
parser.add_argument('-c', help="Input gro file", dest="ingro")
parser.add_argument('-p', help="Input top file", dest="intop")
parser.add_argument('-f', help="Input mdp file (for the energy minimizations)", dest="inmdp")
parser.add_argument('-gmx', help="Location of gromacs executable", dest="gmx")
parser.add_argument('--n-iter', help="Number of iterations", dest="Niter", type=int, default=27)
parser.add_argument('--scale-initial', help="Initial scaling factor", dest="initialScale", type=float, default=4.0)
parser.add_argument('--scale-down', help="Down scaling factor", dest="downScale", type=float, default=0.95)
parser.add_argument('--extra-group', help="Other molecules to embed along with the protein", dest="extra", action="append", default=[])
parser.add_argument('--ntmpi', help="Gromacs ntomp settings", dest="ntmpi", type=int, default=1)
parser.add_argument('--ntomp', help="Gromacs ntomp settings", dest="ntomp", type=int, default=0)
parser.add_argument('--not-protein-groups', help="Identify residues that are not part of the protein (overwrite in case incorrectly identified)", dest="not_protein", action="append", default=[])
parser.add_argument('--mol-based', help="Molecule-based COM translation instead of atom-based downscaling", dest="molBased", action="store_true")

args = parser.parse_args()

ingro = args.ingro
intop = args.intop
inmdp = args.inmdp
gmx = args.gmx

Niter = args.Niter
extra = args.extra
ntmpi = args.ntmpi
ntomp = args.ntomp
not_protein = args.not_protein
initialScale = args.initialScale
downScale = args.downScale
do_molBased = args.molBased

import mdtraj, numpy as np, os, itertools
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology

ingro = os.path.abspath(ingro)
intop = os.path.abspath(intop)
inmdp = os.path.abspath(inmdp)
jobloc = os.getcwd()+"/jobdir/"

def cleanSystem(struc, protAtoms):
  other = []
  for a in struc.top.atoms:
    if a.index in protAtoms: continue
    if a.element.symbol == "H": continue
    if a.residue.n_atoms < 5: continue
    other.append(a.index)

  protAtomsHeavy = [a for a in protAtoms if struc.top.atom(a).element.symbol != "H"]
  
  pairs = np.array(list(itertools.product(other, protAtomsHeavy)))
  r = mdtraj.compute_distances(struc, pairs)[0]
  bad = np.where(r < 0.2)[0] #other atoms that are within 2A of protAtomsHeavy
  
  badrid = np.unique([struc.top.atom(a).residue.index for a in pairs.T[0][bad]])
  remLip = {}
  for rid in badrid:
    res = struc.top.residue(rid)
    if not res.name in remLip: remLip[res.name] = 0
    remLip[res.name] += 1
  
  atoms = [a.index for a in struc.top.atoms if not a.residue.index in badrid]
  
  struc.atom_slice(atoms, inplace=True)
  
  top = []
  moleculesMode = False
  for line in open(intop).readlines():
    if moleculesMode:
      linfo = line.split()
      if linfo[0] in remLip:
        nn = int(linfo[1])-remLip[linfo[0]]
        line = "%s   %s\n"%(linfo[0], nn)
    if line.startswith("[ molecules ]"): moleculesMode = True
    top.append(line)
  open(intop, "w").write("".join(top))

def scaleSystem(idx, struc, factor, molBased=False):
  molecules = [[]]
  for res in struc.top.residues:
    isProtein = res.is_protein
    if res.name == "HID" or res.name == "HIE" or res.name == "CYX": isProtein = True
    if (isProtein and not res.name in not_protein) or res.name in extra: molecules[0] += [i.index for i in res.atoms]
    else: molecules.append([i.index for i in res.atoms])

  if not molBased:
    mol = molecules[0]
    struc.xyz[0,mol,0] += np.mean(struc.xyz[0,mol,0])*(factor-1)
    struc.xyz[0,mol,1] += np.mean(struc.xyz[0,mol,1])*(factor-1)
    other = np.concatenate(molecules[1:])
    struc.xyz[0,other,0] *= factor
    struc.xyz[0,other,1] *= factor
  else:
    for mol in molecules:
      struc.xyz[0,mol,0] += np.mean(struc.xyz[0,mol,0])*(factor-1)
      struc.xyz[0,mol,1] += np.mean(struc.xyz[0,mol,1])*(factor-1)

  struc.unitcell_lengths[:,:2] *= factor
  
  if idx == 0: cleanSystem(struc, molecules[0])
  
  if not os.path.exists("iter-%s"%idx): os.mkdir("iter-%s"%idx)
  os.chdir("iter-%s"%idx)
  
  options = {}
  options["gmx"] = gmx
  options["mdp"] = inmdp
  options["topol"] = intop
  options["ntmpi"] = ntmpi
  options["ntomp"] = ntomp
  
  struc.save("struc.gro")
  
  oriStruc.superpose(struc, 0, molecules[0])
  struc.xyz[0,molecules[0]] = oriStruc.xyz[0,molecules[0]]
  struc.save("ref.gro")
  
  
  fp = open("grompp.log", "w")
  subprocess.run("%(gmx)s grompp -f %(mdp)s -c struc.gro -r ref.gro -p %(topol)s -o job.tpr"%options, shell=True, stdout=fp, stderr=fp)
  fp.close()
  os.system("%(gmx)s mdrun -deffnm job -ntmpi %(ntmpi)s -ntomp %(ntomp)s -v"%options)

  subprocess.run("echo 'System' | %(gmx)s trjconv -f job.gro -s job.tpr -pbc mol -o pbc.gro"%options, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
  
  prime = mdtraj.load("pbc.gro")
  
  os.chdir("..")
  
  return prime
  

if not os.path.exists("jobdir"): os.mkdir("jobdir")
os.chdir("jobdir")

struc = mdtraj.load(ingro)
oriStruc = struc[0]

struc = scaleSystem(0, struc, initialScale, True)

for i in range(Niter): struc = scaleSystem(i+1, struc, downScale, do_molBased)

struc.save("final.gro")
