import sys, os, yieldxtc, mdtraj, json, numpy as np
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""} # workaround for mdtraj idiocracy to rename atomnames in the topology

import tools

def additionalArguments(optparser):
  optparser.add_argument("-outgroup", type=str, required=False)

tools.setup(additionalArguments)

pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname

n_structs = tools.config["analysis"].get("N-rk", 1000)
outdir = tools.analFolder+f"structures_pca_final_{n_structs}_raw"+pname
psout = tools.config["fbmd"]["production"]["trj-save-fs"]*.001

outGroup = tools.cli_args.outgroup

def loadIndex(fn):
  index = {}
  for line in open(fn).readlines():
    line = line.strip()
    if line[:1] == "[":
      name = line.replace("[", "").replace("]", "").strip()
      index[name] = []
    elif line: index[name] += [int(i)-1 for i in line.split()]
  for k,v in index.items(): index[k] = np.array(v)
  return index

if not os.path.exists(outdir): os.mkdir(outdir)

info = {}

fileInfoList = tools.lineageRanges()

struc = tools.load(tools.CWD+tools.config["base"]["pdb"])

if outGroup:
  outIndexes = loadIndex(tools.CWD+tools.config["base"]["index"])[outGroup]
  struc.atom_slice(outIndexes, inplace=True)

pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname
struc_data = json.load(open(tools.analFolder+"structures_pca_repick_%s%s.json"%(n_structs, pname)))
RT = 0.008314462175 * 300 / 4.184
maxPMF = tools.config["analysis"]["pca"]["max-pmf"]
maxPMF_pick = tools.config["analysis"]["pca"].get("max-pmf-pick", maxPMF*.5)
for rk, seed, sidx, pc1, pc2, pc3, lnp, ln_pop in struc_data:
  fname = f"r{rk+1}.gro"
  pmf = (ln_pop-struc_data[0][7])*-RT
  if pmf > maxPMF_pick: continue
  if os.path.exists(outdir+"/"+fname): continue
  
  atTime = int(sidx)*psout
  fobj = tools.figureOutWhichFile(fileInfoList[int(seed)], atTime)
  fin = fobj[0]
  atTime -= fobj[3]-fobj[1]
  if not fin in info: info[fin] = {}
  info[fin][atTime] = fname


gmx = tools.config["compute"]["gmx"]
projectName = tools.config["base"]["name"]
tpr = sorted(tools.findProdData()[seed].items(), key=lambda i: i[1])[0][0].split(".part")[0].replace(".xtc", "")+".tpr"

for fin, v in info.items():
  fp = yieldxtc.XTCTrajectoryFile(fin)
  fp.__len__() # workaround to weird mdtraj/python/cython bug...
  
  tooutput = sorted(v.keys())
  next = tooutput.pop(0)
  print(fin, v)
  for xyz, time, step, box in fp.readyield():
    if np.abs(time-next) > 0.001: continue # deal with rounding errors...
    if outGroup: xyz = xyz[outIndexes]
    struc.xyz[0] = xyz
    
    tmploc = "/tmp/mcmd_processing_%s.gro"%(projectName)
    outloc = outdir+"/"+v[next]
    
    struc.save(tmploc)
    cmd = "echo 'System' | %s trjconv -f %s -s %s -o %s -pbc mol"%(gmx, tmploc, tpr, outloc)
    os.system(cmd)
    os.remove(tmploc)
    
    if len(tooutput) == 0: break # done
    next = tooutput.pop(0)
  fp.close()
