import os, sys, yaml, struct, numpy as np, json, mdtraj, collections, itertools, gro, argparse
mdtraj.formats.pdb.PDBTrajectoryFile._residueNameReplacements = {"baka-aho": ""}

import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
sys.path.append(os.getcwd())

scriptdir = os.path.dirname(os.path.realpath(__file__))

CAL = 4.184
RT = 0.008314462175 * 300.0

def setup(additionalArguments=None):
  global CWD, config, RESTART, __base__, __compute__, gmx, analFolder, refStrucFN, path, vec, cli_args
  
  parser = argparse.ArgumentParser(description="McMD analysis tools")
  parser.add_argument("config")
  if additionalArguments is not None: additionalArguments(parser)
  parser.add_argument("--setting", type=str, nargs=2, action="append", default=[])


  cli_args = parser.parse_args()
  
  conf_file = cli_args.config

  # load config & init
  CWD = os.path.abspath(os.path.dirname(conf_file))+os.sep
  conf_file = os.path.basename(conf_file)
  ###os.chdir(CWD)

  config = yaml.safe_load(open(CWD+conf_file).read())
  
  for where, what in cli_args.setting:
    sub = config
    x = where.split("/")
    for i in x[:-1]: sub = sub[i]
    sub[x[-1]] = yaml.safe_load(what)


  if "CWD" in config: CWD = config["CWD"]

  restart_file = CWD + "config.restart.yml"
  try: RESTART = yaml.safe_load(open(restart_file).read())
  except: 
    try: RESTART = yaml.full_load(open(restart_file).read())
    except: 
      print("Could not load restart.yml")
      exit()
      
  if RESTART["production-phase"] == None:
    maxIdx = -100
    for i in os.listdir(CWD+"histograms"):
      if i.startswith("mdx-") and i.endswith(".dat"):
        idx = int(i[4:-4])
        if idx > maxIdx: maxIdx = idx
    RESTART["production-phase"] = maxIdx+1

  __base__ = config["base"]
  __compute__ = config["compute"]

  gmx = __compute__.get("gmx-local", __compute__["gmx"])

  analFolder = CWD+"anal/"
  if "anal-folder" in config: analFolder = config["anal-folder"].rstrip("/") + os.sep
  if not os.path.exists(analFolder): os.mkdir(analFolder)

  try: refStrucFN = CWD+config["analysis"]["ref-struc"]
  except: refStrucFN = CWD+config["base"]["pdb"]

  try:
    path = np.array([[float(j) for j in i.strip().split(",")] for i in open(CWD+config["fbmd"]["restraints"]["path-file"]).readlines()[1:]])

    # for now only support a simple cylindrical path

    vec = path[1]-path[0]
    vec /= np.linalg.norm(vec)
  except: pass # no path file..


def loadIndex(fname):
  index = {}
  for line in open(fname).readlines():
    line = line.strip()
    if not line: continue
    if line[0] == "[":
      name = line.replace("[", "").replace("]", "").strip()
      index[name] = []
    else: index[name] += [int(i)-1 for i in line.split()]
  for k,v in index.items(): index[k] = np.array(v)
  return index

def loadTOP(loc, bin=None):
  initial = False
  if bin == None:
    initial = True
    bin = {}
    bin["__atoms"] = []
    bin["__bonds"] = []
    bin["__RAW"] = []
  path_alt = os.path.abspath(os.path.dirname(gmx)+"/../..") + "/share/top/"

  path = os.path.dirname(loc)+os.sep
  mode, MT = None, None
  for line in open(loc).readlines():
    if initial: bin["__RAW"].append(line)
    ls = line.strip()
    if not ls: continue
    if ls[:8] == "#include":
      inc = line[9:].replace("\"", "").strip()
      if os.path.exists(path+inc): loadTOP(path+inc, bin)
      elif os.path.exists(path_alt+inc): loadTOP(path_alt+inc, bin)
    elif line[:1] != ";":
      if line[0] == "[":
        mode = ls.strip("[]").strip()
        continue
      if mode == "moleculetype":
        MT = line.split()[0]
        bin[MT] = {"N": 0}
      if mode == "atoms" and ls[0] != "#":
        if not "atoms" in bin[MT]: bin[MT]["atoms"] = []
        bin[MT]["atoms"].append(ls.split())
      if mode == "pairs" and ls[0] != "#":
        if not "pairs" in bin[MT]: bin[MT]["pairs"] = []
        bin[MT]["pairs"].append(ls.split())
      if mode == "bonds" and ls[0] != "#":
        if not "bonds" in bin[MT]: bin[MT]["bonds"] = []
        bin[MT]["bonds"].append(ls.split())
      if mode == "molecules":
        tmp = line.strip().split()
        offset = len(bin["__atoms"])
        natoms = len(bin[tmp[0]]["atoms"])
        bin[tmp[0]]["N"] += int(tmp[1])
        for i in range(int(tmp[1])): bin["__atoms"] += bin[tmp[0]]["atoms"]
        if "bonds" in bin[tmp[0]]:
          for i in range(int(tmp[1])):
            bin["__bonds"] += [[int(i[0])+offset-1, int(i[1])+offset-1] for i in bin[tmp[0]]["bonds"]]
            offset += natoms
  return bin
  
def findNearby(i, i_ori, steps, done, ref_bonds, nearby):
  if steps == 0 or i in done: return
  done.add(i)
  for j in ref_bonds[i]:
    nearby[i_ori].add(j)
    nearby[j].add(i_ori)
    findNearby(j, i_ori, steps-1, done, ref_bonds, nearby)
  
def buildBondList(Nsteps=15, i_must=None, ij_must=None):
  oidxs = outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms
  ref = baseStruct.atom_slice(oidxs.raw)

  top = loadTOP(os.path.abspath(config["CWD"]+config["base"]["tpl"]))
  ref_bonds = {}
  for i,j in top["__bonds"]:
    try: 
      i = np.where(oidxs.raw == i)[0][0]
      j = np.where(oidxs.raw == j)[0][0]
      if not i in ref_bonds: ref_bonds[i] = []
      if not j in ref_bonds: ref_bonds[j] = []
      ref_bonds[i].append(j)
      ref_bonds[j].append(i)
    except: pass

  nearby = {}

  for i in range(ref.n_atoms): nearby[i] = set()
  for i in range(ref.n_atoms): 
    findNearby(i, i, Nsteps, set(), ref_bonds, nearby)

  pairs = []
  for i in range(ref.n_atoms):
    if i_must is not None and not i in i_must: continue
    if ij_must is not None and not i in ij_must: continue
    for j in range(i+1, ref.n_atoms):
      if j in nearby[i]: continue
      if ij_must is not None and not j in ij_must: continue
      pairs.append([i,j])
  
  return np.array(pairs)

baseStruct = None
def initStructure():
  global index, baseStruct
  if baseStruct != None: return
  
  index = loadIndex(CWD+config["base"]["index"])
  baseStruct = load(refStrucFN)

def getReceptorList():
  if baseStruct == None: initStructure()
  arr = []
  if "receptor" in config["fbmd"]:
    if type(config["fbmd"]["receptor"]) == str: sel = [config["fbmd"]["receptor"]]
    else: sel = config["fbmd"]["receptor"]
    for grp in sel: arr += index[grp].tolist()
  return np.unique(arr)

def getGroup(grp, renum=True):
  if outputIndexesObj == None: outputIndexes()
  idxs = index[grp]
  if not renum: return idxs
  idxs_ = np.intersect1d(idxs, outputIndexesObj.raw)
  idxs_ = np.sort([np.where(outputIndexesObj.raw == i)[0][0] for i in idxs_])
  return idxs_

outputIndexesObj = None
def outputIndexes(): # assume System was outputted
  global outputIndexesObj
  
  if baseStruct == None: initStructure()
  
  dockingMode = "ligand" in config
  heavyIdxs = baseStruct.top.select("symbol != 'H'")
  
  if dockingMode: 
    ligandIdxs = []
    if "names" in config["ligand"]: 
      for name in config["ligand"]["names"]: ligandIdxs += index[name].tolist()
    else: ligandIdxs = index[config["ligand"]["name"]].tolist()
    outIndexes = ligandIdxs + getReceptorList().tolist()
    ligandIdxs = np.sort(np.unique(ligandIdxs))
  else: 
    if "Water_and_ions" in index: nonBulk = np.setdiff1d(index["System"], index["Water_and_ions"])
    else: nonBulk = index["System"]
    outIndexes = np.copy(nonBulk).tolist()
  if "extra-groups" in config["analysis"]:
    egroups = type(config["analysis"]["extra-groups"]) == list and config["analysis"]["extra-groups"] or [config["analysis"]["extra-groups"]]
    for group in egroups: outIndexes += index[group].tolist()
  outIndexes = np.sort([i for i in np.unique(outIndexes) if i in heavyIdxs])

  if dockingMode: receptorCalpha = np.intersect1d(getReceptorList(), index.get("C-alpha", []))
  else: receptorCalpha = np.intersect1d(nonBulk, index.get("C-alpha", []))
  if len(receptorCalpha) == 0 and "receptor" in config["fbmd"]: receptorCalpha = np.intersect1d(baseStruct.top.select(config["analysis"]["receptor-CA"]), index[config["fbmd"]["receptor"]])
  receptorCalpha = np.sort([np.where(outIndexes == i)[0][0] for i in receptorCalpha])

  if dockingMode: 
    receptorAtoms = np.intersect1d(getReceptorList(), heavyIdxs)
    receptorAtoms = np.sort([np.where(outIndexes == i)[0][0] for i in receptorAtoms])
    ligandAtoms = np.intersect1d(ligandIdxs, heavyIdxs)
    ligandAtoms = np.sort([np.where(outIndexes == i)[0][0] for i in ligandAtoms])
  else:
    receptorAtoms = np.arange(len(outIndexes))
    ligandAtoms = np.array([], dtype=np.int32)
  
  if dockingMode and "names" in config["ligand"]:
    ligands = {}
    for name in config["ligand"]["names"]:
      idxs = np.sort(np.unique(index[name].tolist()))
      atoms = np.intersect1d(idxs, heavyIdxs)
      ligands[name] = np.sort([np.where(outIndexes == i)[0][0] for i in atoms])
    obj = collections.namedtuple('outputIndexes', 'raw receptorCalpha receptorAtoms ligandAtoms ligands')
    outputIndexesObj = obj(raw=outIndexes, receptorCalpha=receptorCalpha, receptorAtoms=receptorAtoms, ligandAtoms=ligandAtoms, ligands=ligands)
  else:
    obj = collections.namedtuple('outputIndexes', 'raw receptorCalpha receptorAtoms ligandAtoms')
    outputIndexesObj = obj(raw=outIndexes, receptorCalpha=receptorCalpha, receptorAtoms=receptorAtoms, ligandAtoms=ligandAtoms)
  return outputIndexesObj
  
def figureOutWhichFile(finfo, atTime):
  info = sorted(finfo, key=lambda i: i[3])
  times = np.array([i[3] for i in info])
  return info[np.where(times <= atTime)[0][-1]]
  
def findProdData():
  if not os.path.exists(analFolder+"temp/"): os.mkdir(analFolder+"temp/")
  if os.path.exists(analFolder+"temp/file-info.json"): return json.loads(open(analFolder+"temp/file-info.json").read())

  Nbakapara = config["fbmd"]["seeds"]
  
  xtcinfo = {}
  for seed in range(Nbakapara): xtcinfo[str(seed)] = {}

  if "target-folders" in config["fbmd"]["production"]:
    for folder in config["fbmd"]["production"]["target-folders"]:
      folder = CWD+folder+"/"
      for seed in range(Nbakapara):
        seed = str(seed)
        for file in os.listdir(folder+seed):
          if file[-4:] != ".xtc": continue
          name = file[:-4]
          logfl = open(folder+seed+"/"+name+".log").read()
          if not "Reading checkpoint file %s.cpt"%(name.split(".")[0]) in logfl: fromTime = 0
          else: fromTime = logfl.split("Reading checkpoint file %s.cpt"%(name.split(".")[0]))[1].split("  time:")[1].split("\n")[0].strip()
          xtcinfo[seed][folder+seed+"/"+file] = float(fromTime)
  else:
    mdx = RESTART["production-phase"]
    for seed in range(Nbakapara): xtcinfo[str(seed)][CWD+"mdx-%s/%s.xtc"%(mdx, seed)] = 0.0  
  
  open(analFolder+"temp/file-info.json", "w").write(json.dumps(xtcinfo))

  return xtcinfo
  
def lineageRanges():
  Nbakapara = config["fbmd"]["seeds"]
  finfo = findProdData()
  files = {}
  
  if not "target-folders" in config["fbmd"]["production"]:
    for seed in range(Nbakapara):
      files[seed] = []
      for file, startTime in finfo[str(seed)].items(): files[seed].append([file, startTime, None, startTime])
  else:
    for seed in range(Nbakapara):
      info = finfo[str(seed)].items()
      files[seed] = []
      lineages = {}
    
      for file, startTime in info:
        lineage = os.path.basename(file).split(".")[0]
        if not lineage in lineages: lineages[lineage] = []
        lineages[lineage].append([file, startTime])
    
      rinfo = config["fbmd"]["production"].get("run-info", ["run"])
      for lidx, lineage in enumerate(rinfo):
        runTime = config["fbmd"]["production"].get("run-time", [None])[lidx]
        info = sorted(lineages[lineage], key=lambda i: i[1])
        for i, (file, startTime) in enumerate(info):
          if i < len(info)-1: endTime = info[i+1][1]
          elif len(rinfo)-1 == lidx: endTime = None
          else: endTime = (runTime*1000)-(config["fbmd"]["production"]["trj-save-fs"]*1e-3)
          accStart = startTime
          if lidx > 0: accStart += config["fbmd"]["production"].get("run-time", [None])[lidx-1]*1000
          files[seed].append([file, startTime, endTime, accStart])

  return files
  

def getReprForProtein(ref, groupIdxs, ct):
  selectType = ct.get("select-type", "repr")
  skippies = ct.get("skip-residues", [])
  CAs = []
  sidechainRepr = []

  selectors = {}
  if selectType == "CB":
    selectors["ALA"] = "CB"
    selectors["CYS"] = "CB"
    selectors["ASP"] = "CB"
    selectors["GLU"] = "CB"
    selectors["PHE"] = "CB"
    selectors["GLY"] = "CA"
    selectors["HIS"] = "CB"
    selectors["ILE"] = "CB"
    selectors["LYS"] = "CB"
    selectors["LEU"] = "CB"
    selectors["MET"] = "CB"
    selectors["ASN"] = "CB"
    selectors["PRO"] = "CB"
    selectors["GLN"] = "CB"
    selectors["ARG"] = "CB"
    selectors["SER"] = "CB"
    selectors["THR"] = "CB"
    selectors["VAL"] = "CB"
    selectors["TRP"] = "CB"
    selectors["TYR"] = "CB"
  elif selectType == "repr":
    selectors["ALA"] = "CB"
    selectors["CYS"] = "SG"
    selectors["ASP"] = "CG"
    selectors["GLU"] = "CD"
    selectors["PHE"] = "CZ"
    #selectors["GLY"] = None
    selectors["HIS"] = "NE2"
    selectors["ILE"] = ["CD1", "CD"]
    selectors["LYS"] = "NZ"
    selectors["LEU"] = "CG"
    selectors["MET"] = "CE"
    selectors["ASN"] = "CG"
    selectors["PRO"] = "CG"
    selectors["GLN"] = "CD"
    selectors["ARG"] = "CZ"
    selectors["SER"] = "OG"
    selectors["THR"] = "CB"
    selectors["VAL"] = "CB"
    selectors["TRP"] = "CZ3"
    selectors["TYR"] = "OH"
    
  for k,v in ct.get("custom-selectors", {}).items(): selectors[k] = v

  for k,v in selectors.items():
    if type(v) == str: v = [v]
    selectors[k] = set(v)

  reindexer = {}

  for a in groupIdxs:
    atom = ref.top.atom(a)
    if str(atom.residue) in skippies: continue
    if atom.name == "CA": CAs.append(a)
    reindexer[atom.residue] = len(reindexer.keys())
    #print(atom.name, selectors.get(atom.residue.name))
    if atom.name in selectors.get(atom.residue.name, []): 
      sidechainRepr.append(a)

  return CAs, sidechainRepr, reindexer

def pcaSelectProtein():
  # the goal of this script, is to select which atoms to select from the protein (assuming foldering/sampling mode)

  oIdxs = outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms

  ref = load(analFolder+"ref.gro")
  ct = config["analysis"]["pca"].get("auto-select", {})

  CAs, sidechainRepr, reindexer = getReprForProtein(ref, oIdxs.receptorAtoms, ct)

  restrainedRes = set()
  prfile = config["fbmd"].get("restraints", {}).get("posres-file")
  if prfile: 
    for line in open(CWD+prfile).readlines()[1:]:
      if not line.strip(): continue
      line = line.split()
      restrainedRes.add(ref.top.atom(np.where(int(line[0]) == oIdxs.raw)[0][0]).residue.index)
  drfile = config["fbmd"].get("restraints", {}).get("disres-file")
  if drfile:
    for line in open(CWD+drfile).readlines()[1:]:
      if not line.strip(): continue
      line = line.split()
      restrainedRes.add(ref.top.atom(np.where(int(line[0]) == oIdxs.raw)[0][0]).residue.index)
      restrainedRes.add(ref.top.atom(np.where(int(line[1]) == oIdxs.raw)[0][0]).residue.index)

  resSize = {}
  for i in sidechainRepr:
    j = ref.top.atom(i).residue.atom("CA").index
    resSize[i] = np.linalg.norm(ref.xyz[0,i]-ref.xyz[0,j]) 
  
  avg = np.mean(ref.xyz[0], axis=0)
  tmp = [i for i in CAs if ref.top.atom(i).residue.index in restrainedRes]
  refCAs = [tmp[np.argmin([np.linalg.norm(avg-ref.xyz[0,i]) for i in tmp])]]
  
  # center CA and then 2 more CAs that are spread out
  for i in range(2): refCAs.append(tmp[np.argmax([np.min([np.linalg.norm(ref.xyz[0,j]-ref.xyz[0,k]) for k in refCAs]) for j in tmp])])

  pairs = []
  for i,j in itertools.product(refCAs, CAs+sidechainRepr): # general structure of CAs
    ri = ref.top.atom(i).residue
    rj = ref.top.atom(j).residue
    if j in CAs and rj.index in restrainedRes: continue # skip if j is restrained (very unlikely to move, so can be skipped)
    ri = reindexer[ri]
    rj = reindexer[rj]
    if np.abs(ri-rj) < 4: continue
    pairs.append([i,j])

  for i,j in itertools.combinations(sidechainRepr, 2): # packing/interactions
    ri = ref.top.atom(i).residue
    rj = ref.top.atom(j).residue
    rref = 0.0
    if not ri.index in restrainedRes: rref += 0.4
    if not rj.index in restrainedRes: rref += 0.4
    ri = reindexer[ri]
    rj = reindexer[rj]
    if np.abs(ri-rj) < 4: continue
    rref += resSize[i] # add radius for residue i
    rref += resSize[j] # add radius for residue j
    r = np.linalg.norm(ref.xyz[0,i]-ref.xyz[0,j])
    if r > rref: continue
    pairs.append([i,j])
    
  return np.array(pairs).T

def pcaSelectProteinLigand(doIntra=True):
  # the goal of this script, is to select which atoms to select from the ligand, assuming it's a peptide...

  oidxs = outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms

  ref = load(analFolder+"ref.gro")
  ct = config["analysis"]["pca"].get("auto-select", {})

  CAs, sidechainRepr, reindexer = getReprForProtein(ref, oidxs.ligandAtoms, ct)
  
  inter = CAs
  intra = []

  if doIntra:
    for i,j in itertools.combinations(CAs+sidechainRepr, 2):
      ri = reindexer[ref.top.atom(i).residue]
      rj = reindexer[ref.top.atom(j).residue]
      if np.abs(ri-rj) < 3: continue
      intra.append([i,j])
  
  receptor = []
  for a in oidxs.receptorAtoms:
    if ref.top.atom(a).name == "CA": receptor.append(a)
  
  return np.array(list(itertools.product(receptor, inter)) + intra).T

def getReweigingProb(T):
  jso = json.loads(open(analFolder+"distributions/reweighting.json").read())
  return np.array(jso["energy-bins"]), np.array(jso[str(T)])

  
def getTrajectoryRaw(seed, time):
  info = sorted(findProdData()[str(seed)].items(), key=lambda i: i[1])
  
  files = [i[0] for i in info]
  startTimes = np.array([i[1] for i in info])
  
  idxs = np.where(startTimes <= time)[0]
  return CWD+files[idxs[-1]]
  
def pca_distance_pairs(struc):
  pairsA = []
  pairsB = []
  if "auto-select" in config["analysis"]["pca"]: 
    initStructure()
    if "ligand" in config: pairsA, pairsB = pcaSelectProteinLigand()
    else: pairsA, pairsB = pcaSelectProtein()
  elif "lig-external" in config["analysis"]["pca"]:
    protExternal = config["analysis"]["pca"]["prot-external"]
    ligexternal = config["analysis"]["pca"]["lig-external"]
    liginternal = config["analysis"]["pca"].get("lig-internal", [])
    if "names" in config["ligand"] and "--ligand" in sys.argv: ligName = sys.argv[sys.argv.index("--ligand")+1]
    else: ligName = config["ligand"]["name"]
    
    if type(protExternal) == list: protCalpha = protExternal
    else: protCalpha = struc.top.select(protExternal)
    
    if config["analysis"]["pca"].get("prot-filter-si"):
      prfile = config["fbmd"].get("restraints", {}).get("posres-file")
      if not prfile: 
        print("No position restraints used, while si-posres was set...")
        exit()
      oIdxs = outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms
      indicesSI = []
      for line in open(CWD+prfile).readlines()[1:]:
        if not line.strip(): continue
        line = line.split()
        indicesSI.append(np.where(int(line[0]) == oIdxs.raw)[0][0])
      indicesSI = np.array(indicesSI)
      oldN = len(protCalpha)
      protCalpha = np.array([i for i in protCalpha if i in indicesSI], dtype=np.int32)
      print("Filtered pca atoms from %s to %s by posres'ed atoms"%(oldN, len(protCalpha)))
      
    if type(ligexternal) == list: 
      if type(ligexternal[0]) == int: ligexternal_ = ligexternal
      else: ligexternal_ = [struc.top.select("resname '%s' and name '%s'"%(ligName, i))[0] for i in ligexternal]
    else: ligexternal_ = struc.top.select(ligexternal)
    if type(liginternal) == list: 
      if len(liginternal):
        if type(liginternal[0][0]) == int: liginternal_ = liginternal
        else: liginternal_ = [[struc.top.select("resname '%s' and name '%s'"%(ligName, i))[0], struc.top.select("resname '%s' and name '%s'"%(ligName, j))[0]] for i,j in liginternal]
      else: liginternal_ = []
    else: liginternal_ = list(itertools.product(struc.top.select(liginternal), struc.top.select(liginternal)))
  
    pairsA, pairsB = np.array(list(itertools.product(protCalpha, ligexternal_)) + liginternal_).T
  elif "distance-selection" in config["analysis"]["pca"]:
    distanceSelection = config["analysis"]["pca"]["distance-selection"]
    selection = struc.top.select(distanceSelection)
    pairsA, pairsB = np.array(list(itertools.combinations(selection, 2))).T
    
  if "custom-pairs" in config["analysis"]["pca"]: 
    if type(config["analysis"]["pca"]["custom-pairs"]) == str: pairsA_, pairsB_ = __import__(config["analysis"]["pca"]["custom-pairs"]).get_pca_pairs(sys.modules[__name__], struc)
    else: pairsA_, pairsB_ = np.array(config["analysis"]["pca"]["custom-pairs"]).T
    pairsA = np.array(list(pairsA) + list(pairsA_))
    pairsB = np.array(list(pairsB) + list(pairsB_))
  if "custom-selection" in config["analysis"]["pca"]:
    for a, b in config["analysis"]["pca"]["custom-selection"]:
      a = struc.top.select(a)
      b = struc.top.select(b)
      pairsA_, pairsB_ = np.array(list(itertools.product(a, b))).T
      pairsA = np.array(list(pairsA) + list(pairsA_))
      pairsB = np.array(list(pairsB) + list(pairsB_))
  
  pairs = np.array([pairsA, pairsB]).T
  return pairs
  
  
def load(filename_or_filenames, discard_overlapping_frames=False, **kwargs):
  if filename_or_filenames.endswith(".gro"): return gro.load_gro(filename_or_filenames, **kwargs)
  else: return mdtraj.load(filename_or_filenames, discard_overlapping_frames, **kwargs)
