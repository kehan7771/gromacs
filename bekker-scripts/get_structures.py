import sys, os, yieldxtc, mdtraj, json, numpy as np

cwd = os.getcwd()+os.sep
import tools

tools.setup()

mdx = tools.RESTART["production-phase"]

xtcLoc = tools.analFolder+"../mdx-%(mdx)s/%(seed)s.xtc"


pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname

n_structs = tools.config["analysis"].get("N-rk", 1000)
outdir = tools.analFolder+f"structures_pca_final_{n_structs}"+pname

if not os.path.exists(outdir): os.mkdir(outdir)

info = {}

ref = tools.load(tools.analFolder+"ref.gro")
struc = tools.load(tools.analFolder+"ref.gro")

outputIndexes = tools.outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms
indicesSI = outputIndexes.receptorCalpha
if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  print(len(indicesSI), "atoms selected for superposition")

pname = tools.config["analysis"]["pca"].get("pca-alt-name", "")
if pname != "": pname = "_"+pname
struc_data = json.load(open(tools.analFolder+"structures_pca_repick_%s%s.json"%(n_structs, pname)))
RT = 0.008314462175 * 300 / 4.184
maxPMF = tools.config["analysis"]["pca"]["max-pmf"]
maxPMF_pick = tools.config["analysis"]["pca"].get("max-pmf-pick", maxPMF*.5)
for rk, seed, sidx, pc1, pc2, pc3, lnp, ln_pop in struc_data:
  fname = f"r{rk+1}.gro"
  pmf = (ln_pop-struc_data[0][7])*-RT
  if pmf > maxPMF_pick: continue

  if not seed in info: info[seed] = {}
  info[seed][int(sidx)] = fname

subsel = None
if "ligand" in tools.config and "names" in tools.config["ligand"]: subsel = np.sort(np.concatenate([outputIndexes.receptorAtoms, outputIndexes.ligands[pname[1:]]]))
  
  

for seed, v in info.items():
  fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed)
  
  tooutput = sorted(v.keys())
  next = tooutput.pop(0)
  for i, (xyz, time, step, box) in enumerate(fp.readyield()):
    if i != next: continue
    
    struc.xyz[0] = xyz
    struc.superpose(ref, 0, indicesSI)
    if subsel is None: struc.save(outdir+"/"+v[next].replace(".gro", ".pdb"))
    else: struc.atom_slice(subsel, inplace=False).save(outdir+"/"+v[next].replace(".gro", ".pdb"))
    
    if len(tooutput) == 0: break # done
    next = tooutput.pop(0)
  fp.close()
