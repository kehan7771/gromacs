// the goal of this function is to:
//  - restrain the COM of the ligand to within X angstrom of the axis of the path
//  - track & log the COM of the ligand


#include "bekker.h"

#include "gromacs/domdec/localatomset.h"
#include "gromacs/domdec/localatomsetmanager.h"
#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

#include <stdlib.h>

std::vector<FILE*> mc_fps;

void bekker_init_cylinder(t_cylinder_bag *cylinder_bag, int nfile, const t_filenm fnm[], rvec *x, const t_mdatoms *md, PbcType ePBC, const matrix box, const t_commrec *cr) {
  int i, ii, d, l;
  
  for (l=0; l<cylinder_bag->ngroups; l++) {
    cylinder_bag->ligands[l].groupAtomLocs.resize(cylinder_bag->ligands[l].nat);
    cylinder_bag->ligands[l].masses.resize(cylinder_bag->ligands[l].nat);
  }
  
  if (MASTER(cr)) {
    mc_fps.resize(cylinder_bag->ngroups);
    
    std::string base(opt2fn("-g", nfile, fnm)); base.erase(base.length()-4);
    char mc_fn[STRLEN];
    
    t_pbc  pbc;
    set_pbc(&pbc, ePBC, box);
    
    rvec dx, tmp;
    real best_lambda = 1e99; ii = 0;
    rvec lambda_3d, dir; real lambda_t; int idx;
    
    for (l=0; l<cylinder_bag->ngroups; l++) {
      if (l == 0) sprintf(mc_fn, "%s.cylinder.mc", base.c_str());
      else sprintf(mc_fn, "%s.%i.cylinder.mc", base.c_str(), l);
      
      cylinder_bag->ligands[l].tmass = 0.0;
      for (i=0; i<cylinder_bag->ligands[l].nat; i++) {
        ii = cylinder_bag->ligands[l].global_atoms[i];
        cylinder_bag->ligands[l].masses[i] = md->massT[ii];
        cylinder_bag->ligands[l].tmass += md->massT[ii];
      }
      cylinder_bag->ligands[l].invtm = 1.0 / cylinder_bag->ligands[l].tmass;

      bekker_calc_initial_com(&cylinder_bag->ligands[l], x, ePBC, box);
      
      // now find out which point on the path is the closest, then calculate the COM position back from that point
      
      mc_fps[l] = fopen(mc_fn, "w");
      
      project2Path(&cylinder_bag->path_bag, cylinder_bag->ligands[l].com, &lambda_3d, &lambda_t, &dir, &idx);

      if (lambda_t < cylinder_bag->path_bag.waypoints_lambda[0] || lambda_t > cylinder_bag->path_bag.waypoints_lambda[cylinder_bag->path_bag.nPath]) {
        for (i=0; i<cylinder_bag->path_bag.nPath+1; i++) {
          pbc_dx(&pbc, cylinder_bag->ligands[l].com, cylinder_bag->path_bag.path[i], dx);
        
          for (d=0; d<DIM; d++) tmp[d] = cylinder_bag->path_bag.path[i][d] + dx[d];
          project2Path(&cylinder_bag->path_bag, tmp, &lambda_3d, &lambda_t, &dir, &idx);
          lambda_t = fabs(cylinder_bag->path_bag.waypoints_lambda[i]-lambda_t);

          if (lambda_t < best_lambda) {
            best_lambda = lambda_t;
            ii = i;
          }
        }

        i = ii;
        pbc_dx(&pbc, cylinder_bag->ligands[l].com, cylinder_bag->path_bag.path[i], dx);
        for (d=0; d<DIM; d++) cylinder_bag->ligands[l].com[d] = cylinder_bag->path_bag.path[i][d] + dx[d];
      }
    }
  }

  if (PAR(cr)) {
    for (l=0; l<cylinder_bag->ngroups; l++) {
      gmx_bcast(sizeof(real)*3, &cylinder_bag->ligands[l].com[0], cr->mpi_comm_mygroup);
      gmx_bcast(sizeof(real)*cylinder_bag->ligands[l].nat, &cylinder_bag->ligands[l].masses[0], cr->mpi_comm_mygroup);
      gmx_bcast(sizeof(real), &cylinder_bag->ligands[l].tmass, cr->mpi_comm_mygroup);
      gmx_bcast(sizeof(double), &cylinder_bag->ligands[l].invtm, cr->mpi_comm_mygroup);
    }
  }

}

void bekker_finish_cylinder(t_cylinder_bag *cylinder_bag) {
  for (int l=0; l<cylinder_bag->ngroups; l++) fclose(mc_fps[l]);
}

//static void apply_forces(struct pull_t * pull, t_mdatoms * md, rvec *f)

void cylinder_put_coms_in_box(t_cylinder_bag *cylinder_bag, PbcType ePBC, const matrix box) {
  for (int l=0; l<cylinder_bag->ngroups; l++) put_point_in_box(ePBC, box, cylinder_bag->ligands[l].com);
}

void apply_cylinder(t_cylinder_bag *cylinder_bag, rvec *f, rvec *x, real *bekker_epot, PbcType ePBC, const matrix box, int64_t step, gmx_bool doFlush, const t_commrec *cr) {
  int ii, d, idx, gidx, l;

  real lambda_t, lc_r;
  rvec dir, force, lambda_3d, lc_vec;

  for (l=0; l<cylinder_bag->ngroups; l++) {
    auto localAtomIndices = cylinder_bag->ligands[l].atomSet->localIndex();
    auto search = cylinder_bag->ligands[l].atomSet->collectiveIndex();

    // do minimal amount of work when no force is used (just track the position of the COM)
    if (cylinder_bag->lambda_force == 0 && cylinder_bag->orth_force == 0) {
      for (d=0; d<DIM; d++) dir[d] = cylinder_bag->ligands[l].com[d];
      put_point_in_box(ePBC, box, dir); // make sure that the point is inside the box
      
      if (MASTER(cr)) {
        fwrite(dir, sizeof(rvec), 1, mc_fps[l]);
        if (doFlush) fflush(mc_fps[l]);
      }
      
      //if (step%5000 == 0) fprintf(stdout, "comreg: %ld %f %f %f\n", step, dir[0], dir[1], dir[2]);
      
      for (size_t i=0; i<localAtomIndices.size(); i++) {
        ii = localAtomIndices[i];
        gidx = search[i];
        for (d=0; d<DIM; d++) cylinder_bag->ligands[l].groupAtomLocs[gidx][d] = x[ii][d];
      }

      if (cylinder_bag->terminate_mode) {
        project2Path(&cylinder_bag->path_bag, cylinder_bag->ligands[l].com, &lambda_3d, &lambda_t, &dir, &idx);
        if (lambda_t > cylinder_bag->terminate_lambda) cylinder_bag->terminateMD = true;
      } 
      
      continue;
    }
    
    if (MASTER(cr)) {
      fwrite(cylinder_bag->ligands[l].com, sizeof(rvec), 1, mc_fps[l]);
      if (doFlush) fflush(mc_fps[l]);
    }

    project2Path(&cylinder_bag->path_bag, cylinder_bag->ligands[l].com, &lambda_3d, &lambda_t, &dir, &idx);
      
    dir[0] = cylinder_bag->path_bag.pathDirections[idx][0];
    dir[1] = cylinder_bag->path_bag.pathDirections[idx][1];
    dir[2] = cylinder_bag->path_bag.pathDirections[idx][2];
    
    for (d=0; d<DIM; d++) force[d] = 0.0;
    
    if (lambda_t < cylinder_bag->lambda_from) {
      if (MASTER(cr)) *bekker_epot += .5 * cylinder_bag->lambda_force*(cylinder_bag->lambda_from - lambda_t)*(cylinder_bag->lambda_from - lambda_t);
      svmul((cylinder_bag->lambda_from - lambda_t) * -cylinder_bag->lambda_force, dir, force);
    }
    else if (lambda_t > cylinder_bag->lambda_to) {
      if (MASTER(cr)) *bekker_epot += .5 * cylinder_bag->lambda_force*(cylinder_bag->lambda_to - lambda_t)*(cylinder_bag->lambda_to - lambda_t);
      svmul((cylinder_bag->lambda_to - lambda_t) * -cylinder_bag->lambda_force, dir, force);
    }
    
    real mx_orth_dist = cylinder_bag->mx_orth_dist;
    
    // linear interpolation between 0 and maxLambda
    
    if (lambda_t > 0) {
      if (lambda_t < cylinder_bag->lambda_to) mx_orth_dist = cylinder_bag->mx_orth_dist + ((cylinder_bag->mx_orth_dist2-cylinder_bag->mx_orth_dist)*(lambda_t/cylinder_bag->lambda_to));
      else mx_orth_dist = cylinder_bag->mx_orth_dist2;
    }
    
    // bias to keep it within range of the path (perpendicular to lambda)
    rvec_sub(cylinder_bag->ligands[l].com, lambda_3d, lc_vec);
    lc_r = norm(lc_vec);
    if (lc_r > mx_orth_dist) {
      if (MASTER(cr)) *bekker_epot += .5 * cylinder_bag->orth_force*(lc_r - mx_orth_dist)*(lc_r - mx_orth_dist);
      svmul((1./lc_r)*-cylinder_bag->orth_force, lc_vec, lc_vec);
      svmul(lc_r - mx_orth_dist, lc_vec, lc_vec);
      rvec_sub(force, lc_vec, force);
    }
    
    svmul(cylinder_bag->ligands[l].invtm, force, force);
    for (size_t i=0; i<localAtomIndices.size(); i++) {
      ii = localAtomIndices[i];
      gidx = search[i];
      for (d=0; d<DIM; d++) {
        f[ii][d] -= force[d] * cylinder_bag->ligands[l].masses[gidx];
        cylinder_bag->ligands[l].groupAtomLocs[gidx][d] = x[ii][d];
      }
    }

    if (cylinder_bag->terminate_mode && lambda_t > cylinder_bag->terminate_lambda) cylinder_bag->terminateMD = true;
  }
}
