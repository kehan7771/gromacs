#include "bekker.h"
#include <cmath>

#include "gromacs/mdtypes/commrec.h"
#include "gromacs/domdec/domdec_struct.h"
#include "gromacs/gmxlib/network.h"

void bekker_apply_disres_normal(const t_disres_bag *disres_bag, real *bekker_epot, t_pbc& pbc, const rvec *x, rvec *f);
void bekker_apply_disres_cr(const t_disres_bag *disres_bag, real *bekker_epot, t_pbc& pbc, const rvec *x, rvec *f, const t_commrec *cr);

void bekker_apply_posres(const t_posres_bag *posres_bag, const rvec *x, rvec *f, real *bekker_epot, PbcType ePBC, const matrix box) {
  if (posres_bag->group.nat == 0) return;
  int ii, d, gidx;
  rvec force, vec;
  real r, invr;
  
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);
  
  auto localAtomIndices = posres_bag->group.atomSet->localIndex();
  auto search = posres_bag->group.atomSet->collectiveIndex();
  
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];

    for (d=0; d<DIM; d++) force[d] = std::isnan(posres_bag->refpoints[gidx][d]) ? x[ii][d] : posres_bag->refpoints[gidx][d];
    
    pbc_dx(&pbc, x[ii], force, vec);
    invr = gmx::invsqrt(norm2(vec)); r = invr*norm2(vec);
    if (posres_bag->flatRanges[gidx] < 0) {
      if (r < -posres_bag->flatRanges[gidx]) {
        if (r < -posres_bag->rmax[gidx]) r = -posres_bag->rmax[gidx];
        r = -posres_bag->flatRanges[gidx]-r;
        *bekker_epot += .5 * posres_bag->forceConstants[gidx]*r*r;
        svmul(invr*posres_bag->forceConstants[gidx], vec, force);
        svmul(r, force, force);
        for (d=0; d<DIM; d++) f[ii][d] += force[d];
      }
    }
    else if (r > posres_bag->flatRanges[gidx]) {
      if (r > posres_bag->rmax[gidx]) r = posres_bag->rmax[gidx];
      r = r-posres_bag->flatRanges[gidx];
      *bekker_epot += .5 * posres_bag->forceConstants[gidx]*r*r;
      svmul(invr*posres_bag->forceConstants[gidx], vec, force);
      svmul(r, force, force);
      for (d=0; d<DIM; d++) f[ii][d] -= force[d];
    }
  }
}

inline void disres_pair(int aid1, int aid2, int i, const t_disres_bag *disres_bag, real *bekker_epot, t_pbc& pbc, const rvec *x, rvec *f) {
  int d; rvec vec; real k;
  pbc_dx(&pbc, x[aid1], x[aid2], vec);
  real invr = gmx::invsqrt(norm2(vec)); 
  real r = invr*norm2(vec);
  
  r -= disres_bag->r[i];
  real sign = r < 0 ? -1 : 1;
  
  r *= sign; // fabs
  
  if (disres_bag->r_free[i] < 0) {
    if (r < -disres_bag->r_free[i]) {
      if (r < -disres_bag->rmax[i]) r = -disres_bag->rmax[i];
      r += disres_bag->r_free[i];
      svmul(invr, vec, vec);
      k = disres_bag->k[i];
      *bekker_epot += 0.5 * k * r * r;
      for (d=0; d<DIM; d++) {
        f[aid1][d] -= vec[d] * r * k * sign;
        f[aid2][d] += vec[d] * r * k * sign;
      }
    }
  }
  else if (r > disres_bag->r_free[i]) {
    if (r > disres_bag->rmax[i]) r = disres_bag->rmax[i];
    r -= disres_bag->r_free[i];
    svmul(invr, vec, vec);
    k = disres_bag->k[i];
    *bekker_epot += 0.5 * k * r * r;
    for (d=0; d<DIM; d++) {
      f[aid1][d] -= vec[d] * r * k * sign;
      f[aid2][d] += vec[d] * r * k * sign;
    }
  }
}

void bekker_apply_disres_normal(const t_disres_bag *disres_bag, real *bekker_epot, t_pbc& pbc, const rvec *x, rvec *f) {
  for (int i=0; i<disres_bag->nrestr; i++) {
    int aid1 = disres_bag->atm1[i];
    int aid2 = disres_bag->atm2[i];
    disres_pair(aid1, aid2, i, disres_bag, bekker_epot, pbc, x, f);
  }
}

void bekker_apply_disres_cr(const t_disres_bag *disres_bag, real *bekker_epot, t_pbc& pbc, const rvec *x, rvec *f, const t_commrec *cr) {
  int *trash_check, i;
  snew(trash_check, disres_bag->nrestr);
  
  for (i=0; i<disres_bag->nrestr; i++) {
    int aid1 = disres_bag->atm1[i];
    int aid2 = disres_bag->atm2[i];
    trash_check[i] = 0;

    const auto* const e1 = cr->dd->ga2la->find(aid1);
    const auto* const e2 = cr->dd->ga2la->find(aid2);
    if (e1 == nullptr || e2 == nullptr) continue;
    if (e1->cell != 0 && e2->cell != 0) continue;
    aid1 = e1->la;
    aid2 = e2->la;
    
    disres_pair(aid1, aid2, i, disres_bag, bekker_epot, pbc, x, f);
    trash_check[i] = 1;
  }

  // the below code is all there to deal with long-range restraints when using domain decomposition...

  gmx_sumi(disres_bag->nrestr, trash_check, cr);
  
  int Ntrash = 0;
  
  for (i=0; i<disres_bag->nrestr; i++) if (trash_check[i] == 0) Ntrash++;

  if (Ntrash == 0) {
    sfree(trash_check);
    return; // no more annoying things to do
  }
  
  int j, d;
  
  rvec *trash_coords;
  snew(trash_coords, Ntrash*2);
  for (i=0; i<Ntrash*2; i++) clear_rvec(trash_coords[i]);
  
  for (i=0, j=0; i<disres_bag->nrestr; i++) {
    if (trash_check[i]) continue;
    
    int aid1 = disres_bag->atm1[i];
    int aid2 = disres_bag->atm2[i];
    
    const auto* const e1 = cr->dd->ga2la->find(aid1);
    const auto* const e2 = cr->dd->ga2la->find(aid2);
    
    if (e1 != nullptr && e1->cell == 0) for (d=0; d<DIM; d++) trash_coords[j*2][d] = x[e1->la][d];
    else if (e2 != nullptr && e2->cell == 0) for (d=0; d<DIM; d++) trash_coords[(j*2)+1][d] = x[e2->la][d];
    
    j++;
  }

  gmx_sum(Ntrash*3*2, &trash_coords[0][0], cr);
  
  rvec *trash_forces;
  snew(trash_forces, Ntrash);
  
  if (MASTER(cr)) {
    rvec vec; real k;
    for (i=0, j=0; i<disres_bag->nrestr; i++) {
      if (trash_check[i]) continue;
      pbc_dx(&pbc, trash_coords[j*2], trash_coords[(j*2)+1], vec);
      
      real invr = gmx::invsqrt(norm2(vec)); 
      real r = invr*norm2(vec);

      r -= disres_bag->r[i];
      real sign = r < 0 ? -1 : 1;
      
      r *= sign; // fabs
      
      if (disres_bag->r_free[i] < 0) {
        if (r < -disres_bag->r_free[i]) {
          if (r < -disres_bag->rmax[i]) r = -disres_bag->rmax[i];
          r += disres_bag->r_free[i];
          svmul(invr, vec, vec);
          k = disres_bag->k[i];
          *bekker_epot += 0.5 * k * r * r;
          for (d=0; d<DIM; d++) trash_forces[j][d] = vec[d] * r * k * sign;
        }
      }
      else if (r > disres_bag->r_free[i]) {
        if (r > disres_bag->rmax[i]) r = disres_bag->rmax[i];
        r -= disres_bag->r_free[i];
        svmul(invr, vec, vec);
        k = disres_bag->k[i];
        *bekker_epot += 0.5 * k * r * r;
        for (d=0; d<DIM; d++) trash_forces[j][d] = vec[d] * r * k * sign;
      }
      
      j++;
    }
  }
  
  gmx_bcast(sizeof(rvec)*Ntrash, &trash_forces[0][0], cr->mpi_comm_mygroup);
  
  for (i=0, j=0; i<disres_bag->nrestr; i++) {
    if (trash_check[i]) continue;
    
    int aid1 = disres_bag->atm1[i];
    int aid2 = disres_bag->atm2[i];
    
    const auto* const e1 = cr->dd->ga2la->find(aid1);
    const auto* const e2 = cr->dd->ga2la->find(aid2);
    
    if (e1 != nullptr && e1->cell == 0) for (d=0; d<DIM; d++) f[e1->la][d] -= trash_forces[j][d];
    else if (e2 != nullptr && e2->cell == 0) for (d=0; d<DIM; d++) f[e2->la][d] += trash_forces[j][d];
    
    j++;
  }

  sfree(trash_check);
  sfree(trash_coords);
  sfree(trash_forces);
}

void bekker_apply_disres(const t_disres_bag *disres_bag, real *bekker_epot, PbcType ePBC, const matrix box, const rvec *x, rvec *f, const t_commrec *cr) {
  if (disres_bag->nrestr == 0) return;
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);
  
  if (PAR(cr)) bekker_apply_disres_cr(disres_bag, bekker_epot, pbc, x, f, cr);
  else bekker_apply_disres_normal(disres_bag, bekker_epot, pbc, x, f);
}
