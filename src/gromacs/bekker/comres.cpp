#include "bekker.h"

#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

void bekker_init_comres(t_comres_bag *comres_bag, const t_mdatoms *md, const rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr) {
  double mass = 0.0; int i, ii, d;
  
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);
  
  rvec vec, vec2;
  
  snew(comres_bag->xprime, comres_bag->group.nat);
  snew(comres_bag->delta_temp, comres_bag->group.nat);

  comres_bag->group.groupAtomLocs.resize(comres_bag->group.nat);
  comres_bag->group.masses.resize(comres_bag->group.nat);

  if (MASTER(cr)) {
    comres_bag->group.tmass = 0.0;
    for (i=0; i<comres_bag->group.nat; i++) {
      ii = comres_bag->group.global_atoms[i];
      comres_bag->group.masses[i] = md->massT[ii];
      comres_bag->group.tmass += md->massT[ii];
    }
    comres_bag->group.invtm = 1.0 / comres_bag->group.tmass;

    // calculate COM of reference structure
    rvec com; clear_rvec(com);
    for (i=0; i<comres_bag->group.nat; i++) {
      ii = comres_bag->group.global_atoms[i];
      mass = comres_bag->group.masses[i] * comres_bag->group.invtm;
      for (d=0; d<DIM; d++) com[d] += comres_bag->refpoints[i][d] * mass;
    }

    // find the atom of the input structure closest to the COM of the reference structure
    real r, best_r = 1e99; int best_r_i = 0;
    for (i=0; i<comres_bag->group.nat; i++) {
      ii = comres_bag->group.global_atoms[i];
      pbc_dx(&pbc, x[ii], com, vec);
      for (d=0; d<DIM; d++) comres_bag->xprime[i][d] = com[d]+vec[d];
      r = norm2(vec);
      if (r < best_r) {
        best_r = r;
        best_r_i = i;
      }
    }
    
    if (! comres_bag->skipWhole) {
      int initialIdx = best_r_i;
      // make the input structure whole, this doesn't work very well for HUGE systems with multiple chains...
      for (i=initialIdx+1; i<comres_bag->group.nat; i++) {
        ii = comres_bag->group.global_atoms[i];
        pbc_dx(&pbc, x[ii], comres_bag->xprime[i-1], vec);
        pbc_dx(&pbc, x[ii], comres_bag->xprime[initialIdx], vec2);
        if (norm2(vec2) < norm2(vec)) {
          for (d=0; d<DIM; d++) comres_bag->xprime[i][d] = comres_bag->xprime[initialIdx][d]+vec2[d];
        }
        else {
          for (d=0; d<DIM; d++) comres_bag->xprime[i][d] = comres_bag->xprime[i-1][d]+vec[d];
        }
      }

      for (i=initialIdx-1; i>-1; i--) {
        ii = comres_bag->group.global_atoms[i];
        pbc_dx(&pbc, x[ii], comres_bag->xprime[i+1], vec);
        pbc_dx(&pbc, x[ii], comres_bag->xprime[initialIdx], vec2);
        if (norm2(vec2) < norm2(vec)) {
          for (d=0; d<DIM; d++) comres_bag->xprime[i][d] = comres_bag->xprime[initialIdx][d]+vec2[d];
        }
        else {
          for (d=0; d<DIM; d++) comres_bag->xprime[i][d] = comres_bag->xprime[i+1][d]+vec[d];
        }
      }
    }
  }

  if (PAR(cr)) {
    gmx_bcast(sizeof(real)*comres_bag->group.nat, &comres_bag->group.masses[0], cr->mpi_comm_mygroup);
    gmx_bcast(sizeof(real)*comres_bag->group.nat*3, &comres_bag->xprime[0], cr->mpi_comm_mygroup);
    gmx_bcast(sizeof(real), &comres_bag->group.tmass, cr->mpi_comm_mygroup);
    comres_bag->group.invtm = 1.0 / comres_bag->group.tmass;
  }
}

// and update the molecule accordingly (so we always have a whole with respect to the reference structure)
void bekker_comres_update(t_comres_bag *comres_bag, rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr) {
  int ii, d, gidx;
  auto localAtomIndices = comres_bag->group.atomSet->localIndex();
  auto search = comres_bag->group.atomSet->collectiveIndex();

  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);

  for (int i=0; i<comres_bag->group.nat; i++) for (d=0; d<DIM; d++) comres_bag->delta_temp[i][d] = 0.0;
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    pbc_dx(&pbc, x[ii], comres_bag->group.groupAtomLocs[gidx], comres_bag->delta_temp[gidx]);
  }

  if (PAR(cr)) gmx_sum(3*comres_bag->group.nat, &comres_bag->delta_temp[0][0], cr);
  for (int i=0; i<comres_bag->group.nat; i++) for (d=0; d<DIM; d++) comres_bag->xprime[i][d] += comres_bag->delta_temp[i][d];
}

void bekker_apply_comres(t_comres_bag *comres_bag, const rvec *x, rvec *f, real *bekker_epot, const matrix box, const t_commrec *cr) {
  int ii, d, gidx;
  double wmass;

  dvec COM, COM_momentum;
  clear_dvec(COM); clear_dvec(COM_momentum);
  
  double mat2[6] = {0, 0, 0, 0, 0, 0}; // ixx, iyy, izz, ixy, iyz, izx
  auto localAtomIndices = comres_bag->group.atomSet->localIndex();
  auto search = comres_bag->group.atomSet->collectiveIndex();
  
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    wmass = comres_bag->group.masses[gidx] * comres_bag->group.invtm;
    for (d=0; d<DIM; d++) {
      COM[d] += wmass * comres_bag->xprime[gidx][d];
      COM_momentum[d] += wmass * (comres_bag->xprime[gidx][d]-comres_bag->refpoints[gidx][d]);
    }
  }
  
  if (PAR(cr)) {
    gmx_sumd(3, &COM[0], cr);
    gmx_sumd(3, &COM_momentum[0], cr);
  }
  
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    wmass = comres_bag->group.masses[gidx] * comres_bag->group.invtm;
    
    mat2[0] += wmass * ( pow(comres_bag->xprime[gidx][1] - COM[1], 2) + pow(comres_bag->xprime[gidx][2] - COM[2], 2) );
    mat2[1] += wmass * ( pow(comres_bag->xprime[gidx][2] - COM[2], 2) + pow(comres_bag->xprime[gidx][0] - COM[0], 2) );
    mat2[2] += wmass * ( pow(comres_bag->xprime[gidx][0] - COM[0], 2) + pow(comres_bag->xprime[gidx][1] - COM[1], 2) );
    
    mat2[3] -= wmass * ( (comres_bag->xprime[gidx][0] - COM[0]) * (comres_bag->xprime[gidx][1] - COM[1]) );
    mat2[4] -= wmass * ( (comres_bag->xprime[gidx][1] - COM[1]) * (comres_bag->xprime[gidx][2] - COM[2]) );
    mat2[5] -= wmass * ( (comres_bag->xprime[gidx][2] - COM[2]) * (comres_bag->xprime[gidx][0] - COM[0]) );
  }
  
  if (PAR(cr)) gmx_sumd(6, &mat2[0], cr);
  
  double determinant = mat2[0] * mat2[1] * mat2[2] + 2.0 * mat2[3] * mat2[4] * mat2[5] - mat2[4] * mat2[4] * mat2[0] - mat2[5] * mat2[5] * mat2[1] - mat2[3] * mat2[3] * mat2[2];
  
  double matrix[3][3];
  dvec COM_rotate;
  dvec a, b, c;
  clear_dvec(COM_rotate);
  real tmp;
  
  if (determinant != 0.0) {
  
    determinant = 1.0/determinant;
  
    matrix[0][0] = (mat2[1] * mat2[2] - mat2[4] * mat2[4]) * determinant;
    matrix[0][1] = (mat2[4] * mat2[5] - mat2[2] * mat2[3]) * determinant;
    matrix[0][2] = (mat2[3] * mat2[4] - mat2[5] * mat2[1]) * determinant;

    matrix[1][0] = matrix[0][1];
    matrix[1][1] = (mat2[2] * mat2[0] - mat2[5] * mat2[5]) * determinant;
    matrix[1][2] = (mat2[5] * mat2[3] - mat2[0] * mat2[4]) * determinant;

    matrix[2][0] = matrix[0][2];
    matrix[2][1] = matrix[1][2];
    matrix[2][2] = (mat2[0] * mat2[1] - mat2[3] * mat2[3]) * determinant;

    for (size_t i=0; i<localAtomIndices.size(); i++) {
      ii = localAtomIndices[i];
      gidx = search[i];
      wmass = comres_bag->group.masses[gidx] * comres_bag->group.invtm;
    
      for (d=0; d<DIM; d++) {
        a[d] = comres_bag->xprime[gidx][d] - COM[d];
        b[d] = comres_bag->xprime[gidx][d] - comres_bag->refpoints[gidx][d];
      }
    
      dcprod(a, b, c);
    
      for (d=0; d<DIM; d++) COM_rotate[d] += wmass * diprod(matrix[d], c);
    }
    
    if (PAR(cr)) gmx_sumd(3, &COM_rotate[0], cr);
  }

  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    wmass = comres_bag->group.masses[gidx] * comres_bag->group.invtm;

    for (d=0; d<DIM; d++) a[d] = comres_bag->xprime[gidx][d] - COM[d];
    dcprod(COM_rotate, a, c);
    for (d=0; d<DIM; d++) {
      tmp = 0.0;
      if (comres_bag->dotrans[d]) tmp += COM_momentum[d];
      if (comres_bag->dorot[d]) tmp += c[d];
      f[ii][d] -= tmp * comres_bag->fc;
      comres_bag->group.groupAtomLocs[gidx][d] = x[ii][d];
      *bekker_epot += .5 * comres_bag->fc * tmp*tmp;
    }
  }
  
  if (comres_bag->boxDMZ > 0) {
    rvec upper;
    for (d=0; d<DIM; d++) upper[d] = box[d][d]-comres_bag->boxDMZ;
    
    for (size_t i=0; i<localAtomIndices.size(); i++) {
      ii = localAtomIndices[i];
      for (d=0; d<DIM; d++) {
        if (! comres_bag->boxDMZdims[d]) continue;
        if (x[ii][d] < comres_bag->boxDMZ) {
          tmp = x[ii][d]-comres_bag->boxDMZ;
          f[ii][d] -= tmp * comres_bag->fc;
          *bekker_epot += 0.5*tmp*tmp*comres_bag->fc;
        }
        else if (x[ii][d] > upper[d]) {
          tmp = x[ii][d]-upper[d];
          f[ii][d] -= tmp * comres_bag->fc;
          *bekker_epot += 0.5*tmp*tmp*comres_bag->fc;
        }
      }
    }
  }
}
