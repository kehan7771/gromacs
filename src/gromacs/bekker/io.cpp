#include "bekker.h"

#include "gromacs/gmxpreprocess/readir.h"
#include "gromacs/mdtypes/inputrec.h"
#include "gromacs/topology/topology.h"
#include "gromacs/utility/inmemoryserializer.h"

struct t_loadBag {
  char group_nameABF[STRLEN], group_nameGMD[STRLEN], group_name_cyl[STRLEN], group_name_boxsizer[STRLEN], group_name_comball[STRLEN];
  rvec gmd_direction;
  gmx_bool read_gmd_direction;
} loadBag;

void mat4Rotate (real out[16], real a[16], real rad, rvec axis);
void vec3TransformMat4(rvec out, rvec a, real m[16]);

// hypersound

void read_hypersound_params(std::vector<t_inpfile> *inp, t_hypersound_bag *hypersound_bag, warninp_t wi) {
  hypersound_bag->N = get_eint(inp, "hypersound-N-steps", 50, wi);
  hypersound_bag->shock_steps = get_eint(inp, "hypersound-shock-steps", 80, wi);
  hypersound_bag->max_vel = get_ereal(inp, "hypersound-max-vel", 0.4, wi);
  hypersound_bag->border = get_ereal(inp, "hypersound-border", 1.0, wi);
  setStringEntry(inp, "hypersound-group", hypersound_bag->group_name, "Water_and_ions");
}

void configure_hypersound(t_hypersound_bag *hypersound_bag, const t_blocka *grps, char **gnames) {
  int ig, i;
  ig = search_string(hypersound_bag->group_name, grps->nr, gnames);
  hypersound_bag->solvent.nat = grps->index[ig+1] - grps->index[ig];
  hypersound_bag->solvent.global_atoms.resize(hypersound_bag->solvent.nat);
  for (i=0; i<hypersound_bag->solvent.nat; i++) hypersound_bag->solvent.global_atoms[i] = grps->a[grps->index[ig]+i];
}

void bekker_hypersound_io(gmx::ISerializer* serializer, t_hypersound_bag *hypersound_bag) {
  serializer->doInt(&hypersound_bag->N);
  serializer->doInt(&hypersound_bag->shock_steps);
  serializer->doReal(&hypersound_bag->max_vel);
  serializer->doReal(&hypersound_bag->border);
  
  serializer->doInt(&hypersound_bag->solvent.nat);
  if (serializer->reading()) hypersound_bag->solvent.global_atoms.resize(hypersound_bag->solvent.nat);
  serializer->doIntArray(hypersound_bag->solvent.global_atoms.data(), hypersound_bag->solvent.nat);
}

// fb-mcmd

// set initialTemp from the gromacs config...

void read_fb_params(std::vector<t_inpfile> *inp, t_fb_bag *fb_bag, warninp_t wi) {
  char fbfile[STRLEN]; fbfile[0] = '\0';

  printStringNoNewline(inp, "The file which lists the biasing data (histogram)");
  printStringNoNewline(inp, "The base temperature at which to apply FB-McMD (the corresponding temperature coupling groups should be set to the same temperature)");
  fb_bag->initialTemp = get_ereal(inp, "fb-base-temp", 300, wi);
  setStringEntry(inp, "fb-histogram-file", fbfile, nullptr);
  size_t saa;
  if (fbfile[0] != '\0') {
    FILE *fp = fopen(fbfile, "rb");
    saa = fread(&fb_bag->minEnergyBin, sizeof(real), 1, fp);
    saa = fread(&fb_bag->binSize, sizeof(real), 1, fp);
    saa = fread(&fb_bag->nBins, sizeof(int), 1, fp);
    
    snew(fb_bag->scaleFactor, fb_bag->nBins);
    saa = fread(fb_bag->scaleFactor, sizeof(real), fb_bag->nBins, fp);
    fclose(fp);
  }
  else fb_bag->nBins = 0;
  printStringNoNewline(inp, "FB-McMD heating mode");
  fb_bag->heatingTemp = get_ereal(inp, "fb-heating-temp", -1, wi);
  fb_bag->heatingMode = fb_bag->heatingTemp >= 0;

  fb_bag->includeRestrEPOT = (getEnum<Boolean>(inp, "fb-include-restrain-epot", wi) != Boolean::No);
}

void bekker_fb_io(gmx::ISerializer* serializer, t_fb_bag *fb_bag) {
  serializer->doReal(&fb_bag->initialTemp);
  serializer->doReal(&fb_bag->minEnergyBin);
  serializer->doReal(&fb_bag->binSize);
  serializer->doInt(&fb_bag->nBins);
  if (serializer->reading()) snew(fb_bag->scaleFactor, fb_bag->nBins);
  serializer->doRealArray(fb_bag->scaleFactor, fb_bag->nBins);
  
  serializer->doBool(&fb_bag->heatingMode);
  serializer->doReal(&fb_bag->heatingTemp);
  serializer->doBool(&fb_bag->includeRestrEPOT);
}

// US

void read_us_params(std::vector<t_inpfile> *inp, t_us_container *us_container, warninp_t wi) {
  int i;
  char tempstr[STRLEN], IDstr[STRLEN];
  t_us_bag *us_bag;

  us_container->nfiles = get_eint(inp, "us-ngroups", 1, wi);
  us_container->bags.resize(us_container->nfiles);
  
  for (int f=0; f<us_container->nfiles; f++) {
    us_bag = &us_container->bags[f];
    
    if (f == 0) {
      printStringNoNewline(inp, "Which group to perform the simulation on (ligand)");
      setStringEntry(inp, "us-group", us_bag->group_name, nullptr);
      setStringEntry(inp, "us-grouptwo", us_bag->group_name2, nullptr);
      if (us_bag->group_name2[0] != 0) us_bag->distance_mode = true;

      printStringNoNewline(inp, "Force constant (kj/mol/nm/nm)");
      us_bag->k = get_ereal(inp, "us-force-constant", 0, wi);
      
      tempstr[0] = '\0';
      setStringEntry(inp, "us-window-xyz", tempstr, nullptr);
      i = sscanf(tempstr, "%f,%f,%f", &us_bag->windowCenter[0], &us_bag->windowCenter[1], &us_bag->windowCenter[2]);

      tempstr[0] = '\0';
      setStringEntry(inp, "us-local-axis", tempstr, nullptr);
      i = sscanf(tempstr, "%f,%f,%f", &us_bag->localAxis[0], &us_bag->localAxis[1], &us_bag->localAxis[2]);

      if (us_bag->distance_mode) {
        if (i == -1) us_bag->localAxis[0] = us_bag->localAxis[1] = us_bag->localAxis[2] = 1;
        us_bag->signed_distance_mode = (getEnum<Boolean>(inp, "us-signed-mode", wi) != Boolean::No);
      }
      else us_bag->do1D = i == 3;
      

      us_bag->mx_orth_dist = get_ereal(inp, "us-max-orth-dist", 0.0, wi);
      us_bag->orth_force = get_ereal(inp, "us-orth-k", 0.0, wi);
      
      us_bag->smd_speed = get_ereal(inp, "us-smd-speed", 0.0, wi);

      us_bag->outSteps = get_eint(inp, "us-out-steps", 0, wi); // 0 means always
    }
    else {
      printStringNoNewline(inp, "Which group to perform the simulation on (ligand)");
      sprintf(IDstr, "us-group-%d", f); setStringEntry(inp, IDstr, us_bag->group_name, nullptr);
      sprintf(IDstr, "us-grouptwo-%d", f); setStringEntry(inp, IDstr, us_bag->group_name2, nullptr);
      if (us_bag->group_name2[0] != 0) us_bag->distance_mode = true;

      printStringNoNewline(inp, "Force constant (kj/mol/nm/nm)");
      
      sprintf(IDstr, "us-force-constant-%d", f); us_bag->k = get_ereal(inp, IDstr, 0.0, wi);
      
      tempstr[0] = '\0';
      sprintf(IDstr, "us-window-xyz-%d", f); setStringEntry(inp, IDstr, tempstr, nullptr);
      i = sscanf(tempstr, "%f,%f,%f", &us_bag->windowCenter[0], &us_bag->windowCenter[1], &us_bag->windowCenter[2]);

      tempstr[0] = '\0';
      sprintf(IDstr, "us-local-axis-%d", f); setStringEntry(inp, IDstr, tempstr, nullptr);
      i = sscanf(tempstr, "%f,%f,%f", &us_bag->localAxis[0], &us_bag->localAxis[1], &us_bag->localAxis[2]);
      if (us_bag->distance_mode) {
        if (i == -1) us_bag->localAxis[0] = us_bag->localAxis[1] = us_bag->localAxis[2] = 1;
        us_bag->signed_distance_mode = (getEnum<Boolean>(inp, "us-signed-mode", wi) != Boolean::No);
      }
      else us_bag->do1D = i == 3;
      

      sprintf(IDstr, "us-max-orth-dist-%d", f); us_bag->mx_orth_dist = get_ereal(inp, IDstr, 0.0, wi);
      sprintf(IDstr, "us-orth-k-%d", f); us_bag->orth_force = get_ereal(inp, IDstr, 0.0, wi);
      
      sprintf(IDstr, "us-smd-speed-%d", f); us_bag->smd_speed = get_ereal(inp, IDstr, 0.0, wi);
      
      sprintf(IDstr, "us-out-steps-%d", f); us_bag->outSteps = get_eint(inp, IDstr, 0, wi); // 0 means always
    }
    
    if (us_bag->distance_mode || us_bag->do1D) {
      double invr = 1/norm(us_bag->localAxis);
      svmul(invr, us_bag->localAxis, us_bag->localAxis);
    }
  }
}

void configure_us(t_us_container *us_container, const t_blocka *grps, char **gnames) {
  int ig, i;
  t_us_bag *us_bag;
  
  for (int f=0; f<us_container->nfiles; f++) {
    us_bag = &us_container->bags[f];
  
    ig = search_string(us_bag->group_name, grps->nr, gnames);
    us_bag->ligand.nat = grps->index[ig+1] - grps->index[ig];
    us_bag->ligand.global_atoms.resize(us_bag->ligand.nat);
    for (i=0; i<us_bag->ligand.nat; i++) us_bag->ligand.global_atoms[i] = grps->a[grps->index[ig]+i];
    
    if (us_bag->distance_mode) {
      ig = search_string(us_bag->group_name2, grps->nr, gnames);
      us_bag->ligand2.nat = grps->index[ig+1] - grps->index[ig];
      us_bag->ligand2.global_atoms.resize(us_bag->ligand2.nat);
      for (i=0; i<us_bag->ligand2.nat; i++) us_bag->ligand2.global_atoms[i] = grps->a[grps->index[ig]+i];
    }
  }
}

void bekker_us_io(gmx::ISerializer* serializer, t_us_container *us_container) {
  t_us_bag *us_bag;
  
  serializer->doInt(&us_container->nfiles);
  if (serializer->reading()) us_container->bags.resize(us_container->nfiles);
  
  for (int f=0; f<us_container->nfiles; f++) {
    us_bag = &us_container->bags[f];
    
    serializer->doInt(&us_bag->ligand.nat);
    if (serializer->reading()) us_bag->ligand.global_atoms.resize(us_bag->ligand.nat);
    serializer->doIntArray(us_bag->ligand.global_atoms.data(), us_bag->ligand.nat);

    serializer->doRealArray(us_bag->windowCenter, 3);
    serializer->doReal(&us_bag->k);

    serializer->doRealArray(us_bag->localAxis, 3);
    serializer->doBool(&us_bag->do1D);

    serializer->doReal(&us_bag->mx_orth_dist);
    serializer->doReal(&us_bag->orth_force);
    
    serializer->doReal(&us_bag->smd_speed);

    serializer->doInt(&us_bag->outSteps);
    
    serializer->doBool(&us_bag->distance_mode);
    serializer->doBool(&us_bag->signed_distance_mode);
    if (us_bag->distance_mode) {
      serializer->doInt(&us_bag->ligand2.nat);
      if (serializer->reading()) us_bag->ligand2.global_atoms.resize(us_bag->ligand2.nat);
      serializer->doIntArray(us_bag->ligand2.global_atoms.data(), us_bag->ligand2.nat);
    }
  }
}

// posres

void read_posres_params(std::vector<t_inpfile> *inp, t_posres_bag *posres_bag) {
  int j = 0;
  char tempstr[STRLEN], line[STRLEN];
  size_t saa;

  printStringNoNewline(inp, "POSRES definition file");
  setStringEntry(inp, "bekker-posres-file", tempstr, nullptr);

  FILE *fp = fopen(tempstr, "r");

  // use a simpler format;
  // number of restraints
  // aid x y z r_free kx ky kz
  
  saa = fscanf(fp, "%d\n", &posres_bag->group.nat);
  posres_bag->group.global_atoms.resize(posres_bag->group.nat); snew(posres_bag->refpoints, posres_bag->group.nat); snew(posres_bag->forceConstants, posres_bag->group.nat); snew(posres_bag->flatRanges, posres_bag->group.nat); snew(posres_bag->rmax, posres_bag->group.nat);
  while (fgets(line, STRLEN, fp) != NULL) {
    saa = sscanf(line, "%d\t%f\t%f\t%f\t%f\t%f\t%f", &posres_bag->group.global_atoms[j], &posres_bag->forceConstants[j], &posres_bag->flatRanges[j], &posres_bag->refpoints[j][0], &posres_bag->refpoints[j][1], &posres_bag->refpoints[j][2], &posres_bag->rmax[j]);
    if (saa == 6) posres_bag->rmax[j] = 1e99;
    j++;
  }
  
  fclose(fp);
}

void bekker_posres_io(gmx::ISerializer* serializer, t_posres_bag *posres_bag) {
  serializer->doInt(&posres_bag->group.nat);
  if (serializer->reading()) {posres_bag->group.global_atoms.resize(posres_bag->group.nat); snew(posres_bag->forceConstants, posres_bag->group.nat); snew(posres_bag->flatRanges, posres_bag->group.nat); snew(posres_bag->refpoints, posres_bag->group.nat); snew(posres_bag->rmax, posres_bag->group.nat);}

  serializer->doIntArray(posres_bag->group.global_atoms.data(), posres_bag->group.nat);
  serializer->doRealArray(posres_bag->forceConstants, posres_bag->group.nat);
  serializer->doRealArray(posres_bag->flatRanges, posres_bag->group.nat);
  serializer->doRealArray(*posres_bag->refpoints, (posres_bag->group.nat)*3);
  serializer->doRealArray(posres_bag->rmax, posres_bag->group.nat);
}

// disres

void read_disres_params(std::vector<t_inpfile> *inp, t_disres_bag *disres_bag) {
  int i = 0;
  char tempstr[STRLEN], line[STRLEN];
  size_t saa;

  printStringNoNewline(inp, "DISRES definition file");
  setStringEntry(inp, "bekker-disres-file", tempstr, nullptr);
  
  FILE *fp = fopen(tempstr, "r");
  saa = fscanf(fp, "%d\n", &disres_bag->nrestr);
  snew(disres_bag->atm1, disres_bag->nrestr);
  snew(disres_bag->atm2, disres_bag->nrestr);
  snew(disres_bag->r, disres_bag->nrestr);
  snew(disres_bag->r_free, disres_bag->nrestr);
  snew(disres_bag->k, disres_bag->nrestr);
  snew(disres_bag->rmax, disres_bag->nrestr);
  
  while (fgets(line, STRLEN, fp) != NULL) {
    saa = sscanf(line, "%d %d %f %f %f %f", &disres_bag->atm1[i], &disres_bag->atm2[i], &disres_bag->r[i], &disres_bag->r_free[i], &disres_bag->k[i], &disres_bag->rmax[i]);
    if (saa == 5) disres_bag->rmax[i] = 1e99;
    i++;
  }
  
  fclose(fp);
}

void bekker_disres_io(gmx::ISerializer* serializer, t_disres_bag *disres_bag) {
  serializer->doInt(&disres_bag->nrestr);
  if (serializer->reading()) {
    snew(disres_bag->atm1, disres_bag->nrestr);
    snew(disres_bag->atm2, disres_bag->nrestr);
    snew(disres_bag->r, disres_bag->nrestr);
    snew(disres_bag->r_free, disres_bag->nrestr);
    snew(disres_bag->k, disres_bag->nrestr);
    snew(disres_bag->rmax, disres_bag->nrestr);
  }

  serializer->doIntArray(disres_bag->atm1, disres_bag->nrestr);
  serializer->doIntArray(disres_bag->atm2, disres_bag->nrestr);
  serializer->doRealArray(disres_bag->r, disres_bag->nrestr);
  serializer->doRealArray(disres_bag->r_free, disres_bag->nrestr);
  serializer->doRealArray(disres_bag->k, disres_bag->nrestr);
  serializer->doRealArray(disres_bag->rmax, disres_bag->nrestr);
}

// cylinder


void read_cylinder_params(std::vector<t_inpfile> *inp, t_cylinder_bag *cylinder_bag, warninp_t wi) {

  int i;
  char tempstr[STRLEN];
  
  printStringNoNewline(inp, "Which group to track & restrain (ligand)");
  setStringEntry(inp, "cylinder-group", loadBag.group_name_cyl, nullptr);

  printStringNoNewline(inp, "How far is the ligand allowed to deviate from the axis of the reaction path and how much force should be used to push it back in");
  cylinder_bag->mx_orth_dist = get_ereal(inp, "cylinder-max-orth-dist", 0.0, wi);
  cylinder_bag->mx_orth_dist2 = get_ereal(inp, "cylinder-max-orth-dist2", cylinder_bag->mx_orth_dist, wi); // second radius --> produces cone
  cylinder_bag->orth_force = get_ereal(inp, "cylinder-orth-k", 0.0, wi);

  setStringEntry(inp, "cylinder-lambda-range", tempstr, nullptr);
  i = sscanf(tempstr, "%f %f", &cylinder_bag->lambda_from, &cylinder_bag->lambda_to);

  cylinder_bag->lambda_force = get_ereal(inp, "cylinder-lambda-k", 0.0, wi);

  printStringNoNewline(inp, "The file which lists the path");
  setStringEntry(inp, "cylinder-path-file", tempstr, nullptr);
  
  cylinder_bag->terminate_mode = (getEnum<Boolean>(inp, "cylinder-terminate", wi) != Boolean::No);
  
  cylinder_bag->terminate_lambda = get_ereal(inp, "cylinder-terminate-lambda", 0, wi);

  FILE *fp = fopen(tempstr, "r");
  if (fscanf(fp, "%d", &cylinder_bag->path_bag.nPath) != 1) fprintf(stderr, "Error reading path file");
  snew(cylinder_bag->path_bag.path, cylinder_bag->path_bag.nPath);
  for (i=0; i<cylinder_bag->path_bag.nPath; i++) if (fscanf(fp, "%f,%f,%f", &cylinder_bag->path_bag.path[i][0], &cylinder_bag->path_bag.path[i][1], &cylinder_bag->path_bag.path[i][2]) != 3) break;
  fclose(fp);
  cylinder_bag->path_bag.nPath -= 1;
}

void configure_cylinder(t_cylinder_bag *cylinder_bag, const t_blocka *grps, char **gnames) {
  int ig, i, j;

  auto groupnames = gmx::splitString(loadBag.group_name_cyl);
  cylinder_bag->ngroups = groupnames.size();
  cylinder_bag->ligands.resize(cylinder_bag->ngroups);
  
  for (i=0; i<cylinder_bag->ngroups; i++) {
    ig = search_string(groupnames[i].c_str(), grps->nr, gnames);
    cylinder_bag->ligands[i].nat = grps->index[ig+1] - grps->index[ig];
    cylinder_bag->ligands[i].global_atoms.resize(cylinder_bag->ligands[i].nat);
    for (j=0; j<cylinder_bag->ligands[i].nat; j++) cylinder_bag->ligands[i].global_atoms[j] = grps->a[grps->index[ig]+j];
  }
}

void bekker_cylinder_io(gmx::ISerializer* serializer, t_cylinder_bag *cylinder_bag) {
  serializer->doInt(&cylinder_bag->ngroups);
  if (serializer->reading()) cylinder_bag->ligands.resize(cylinder_bag->ngroups);
  
  for (int i=0; i<cylinder_bag->ngroups; i++) {
    serializer->doInt(&cylinder_bag->ligands[i].nat);
    if (serializer->reading()) cylinder_bag->ligands[i].global_atoms.resize(cylinder_bag->ligands[i].nat);
    serializer->doIntArray(cylinder_bag->ligands[i].global_atoms.data(), cylinder_bag->ligands[i].nat);
  }

  serializer->doReal(&cylinder_bag->mx_orth_dist);
  serializer->doReal(&cylinder_bag->mx_orth_dist2);
  serializer->doReal(&cylinder_bag->orth_force);

  serializer->doReal(&cylinder_bag->lambda_from);
  serializer->doReal(&cylinder_bag->lambda_to);
  serializer->doReal(&cylinder_bag->lambda_force);

  serializer->doBool(&cylinder_bag->terminate_mode);
  serializer->doReal(&cylinder_bag->terminate_lambda);

  serializer->doInt(&cylinder_bag->path_bag.nPath);
  if (serializer->reading()) snew(cylinder_bag->path_bag.path, cylinder_bag->path_bag.nPath+1);
  serializer->doRealArray(*cylinder_bag->path_bag.path, (cylinder_bag->path_bag.nPath+1)*3);

  if (serializer->reading()) {
    setupPath(&cylinder_bag->path_bag);
    cylinder_bag->terminateMD = false;
  }
}

// comres

void read_comres_params(std::vector<t_inpfile> *inp, t_comres_container *comres_container, warninp_t wi) {

  int i, j;
  size_t saa;
  char tempstr[STRLEN], IDstr[STRLEN];
  FILE *fp;
  t_comres_bag *comres_bag;

  printStringNoNewline(inp, "comres definition files");
  
  comres_container->nfiles = get_eint(inp, "comres-nfiles", 1, wi);
  comres_container->bags.resize(comres_container->nfiles);
  
  for (i=0; i<comres_container->nfiles; i++) {
    comres_bag = &comres_container->bags[i];
    
    if (i == 0) {
      comres_bag->boxDMZ = get_ereal(inp, "comres-box-dmz", 0.0, wi); // 0 -> OFF
      setStringEntry(inp, "comres-box-dmz-dims", tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->boxDMZdims[0], &comres_bag->boxDMZdims[1], &comres_bag->boxDMZdims[2]);
      
      setStringEntry(inp, "comres-trans-dims", tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->dotrans[0], &comres_bag->dotrans[1], &comres_bag->dotrans[2]);
      setStringEntry(inp, "comres-rot-dims", tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->dorot[0], &comres_bag->dorot[1], &comres_bag->dorot[2]);
  
      comres_bag->fc = get_ereal(inp, "comres-particle-k", 418.4, wi); // 1 kcal/mol/A/A per particle
      setStringEntry(inp, "bekker-comres-file", tempstr, nullptr);
  
      fp = fopen(tempstr, "r");
      saa = fscanf(fp, "%d", &comres_bag->group.nat);
      
      comres_bag->group.global_atoms.resize(comres_bag->group.nat);
      snew(comres_bag->refpoints, comres_bag->group.nat);
      for (j=0; j<comres_bag->group.nat; j++) saa = fscanf(fp, "%d\t%f\t%f\t%f", &comres_bag->group.global_atoms[j], &comres_bag->refpoints[j][0], &comres_bag->refpoints[j][1], &comres_bag->refpoints[j][2]);
      fclose(fp);
    }
    else {
      sprintf(IDstr, "comres-box-dmz-%d", i); comres_bag->boxDMZ = get_ereal(inp, IDstr, 0.0, wi); // 0 -> OFF
      sprintf(IDstr, "comres-box-dmz-dims-%d", i); setStringEntry(inp, IDstr, tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->boxDMZdims[0], &comres_bag->boxDMZdims[1], &comres_bag->boxDMZdims[2]);
      
      sprintf(IDstr, "comres-trans-dims-%d", i); setStringEntry(inp, IDstr, tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->dotrans[0], &comres_bag->dotrans[1], &comres_bag->dotrans[2]);
      sprintf(IDstr, "comres-rot-dims-%d", i); setStringEntry(inp, IDstr, tempstr, "1 1 1");
      sscanf(tempstr, "%d %d %d", &comres_bag->dorot[0], &comres_bag->dorot[1], &comres_bag->dorot[2]);
      
      sprintf(IDstr, "comres-particle-k-%d", i); comres_bag->fc = get_ereal(inp, IDstr, 418.4, wi); // 1 kcal/mol/A/A per particle
      sprintf(IDstr, "bekker-comres-file-%d", i); setStringEntry(inp, IDstr, tempstr, nullptr);

      fp = fopen(tempstr, "r");
      saa = fscanf(fp, "%d", &comres_bag->group.nat);
      comres_bag->group.global_atoms.resize(comres_bag->group.nat); snew(comres_bag->refpoints, comres_bag->group.nat);
      for (j=0; j<comres_bag->group.nat; j++) saa = fscanf(fp, "%d\t%f\t%f\t%f", &comres_bag->group.global_atoms[j], &comres_bag->refpoints[j][0], &comres_bag->refpoints[j][1], &comres_bag->refpoints[j][2]);
      fclose(fp);
    }
    comres_bag->skipWhole = (getEnum<Boolean>(inp, "comres-skip-whole", wi) != Boolean::No);
  }
}

void bekker_comres_io(gmx::ISerializer* serializer, t_comres_container *comres_container) {
  int i;
  t_comres_bag *comres_bag;
  
  serializer->doInt(&comres_container->nfiles);
  if (serializer->reading()) comres_container->bags.resize(comres_container->nfiles);
  
  for (i=0; i<comres_container->nfiles; i++) {
    comres_bag = &comres_container->bags[i];
    
    serializer->doInt(&comres_bag->group.nat);
    if (serializer->reading()) {comres_bag->group.global_atoms.resize(comres_bag->group.nat); snew(comres_bag->refpoints, comres_bag->group.nat);}
    serializer->doReal(&comres_bag->fc);
    serializer->doReal(&comres_bag->boxDMZ);
    serializer->doIntArray(comres_bag->boxDMZdims, 3);
    serializer->doIntArray(comres_bag->dotrans, 3);
    serializer->doIntArray(comres_bag->dorot, 3);
    serializer->doIntArray(comres_bag->group.global_atoms.data(), comres_bag->group.nat);
    serializer->doRealArray(*comres_bag->refpoints, (comres_bag->group.nat)*3);
    serializer->doBool(&comres_bag->skipWhole);
  }
  
}

// com ball restraint
void read_comball_params(std::vector<t_inpfile> *inp, t_comball_bag *comball_bag, warninp_t wi) {
  printStringNoNewline(inp, "comball definition file");
  setStringEntry(inp, "bekker-comball-group", loadBag.group_name_comball, nullptr);
  comball_bag->radius = get_ereal(inp, "bekker-comball-radius", 1.0, wi); // = 10 A
  comball_bag->fc = get_ereal(inp, "bekker-comball-fc", 4180.4, wi); // = 10 kcal/mol/A/A per particle
}

void configure_comball(t_comball_bag *comball_bag, const t_blocka *grps, char **gnames) {
  int ig, i;

  ig = search_string(loadBag.group_name_comball, grps->nr, gnames);
  comball_bag->ligand.nat = grps->index[ig+1] - grps->index[ig];
  comball_bag->ligand.global_atoms.resize(comball_bag->ligand.nat);

  for (i=0; i<comball_bag->ligand.nat; i++) comball_bag->ligand.global_atoms[i] = grps->a[grps->index[ig]+i];
}

void bekker_comball_io(gmx::ISerializer* serializer, t_comball_bag *comball_bag) {
  serializer->doInt(&comball_bag->ligand.nat);
  if (serializer->reading()) comball_bag->ligand.global_atoms.resize(comball_bag->ligand.nat);
  serializer->doIntArray(comball_bag->ligand.global_atoms.data(), comball_bag->ligand.nat);

  serializer->doReal(&comball_bag->fc);
  serializer->doReal(&comball_bag->radius);
}

// delete whatever is not required...

void mat4Rotate (real out[16], real a[16], real rad, rvec axis) {
        real x = axis[0], y = axis[1], z = axis[2],
  len = sqrt(x * x + y * y + z * z),
  s, c, t,
  a00, a01, a02, a03,
  a10, a11, a12, a13,
        a20, a21, a22, a23,
        b00, b01, b02,
        b10, b11, b12,
        b20, b21, b22;

        if (abs(len) < 1e-6) return;

        len = 1 / len;
        x *= len;
        y *= len;
        z *= len;

        s = sin(rad);
        c = cos(rad);
        t = 1 - c;

        a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
        a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
        a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];

        // Construct the elements of the rotation matrix
        b00 = x * x * t + c; b01 = y * x * t + z * s; b02 = z * x * t - y * s;
        b10 = x * y * t - z * s; b11 = y * y * t + c; b12 = z * y * t + x * s;
        b20 = x * z * t + y * s; b21 = y * z * t - x * s; b22 = z * z * t + c;

        // Perform rotation-specific matrix multiplication
        out[0] = a00 * b00 + a10 * b01 + a20 * b02;
        out[1] = a01 * b00 + a11 * b01 + a21 * b02;
        out[2] = a02 * b00 + a12 * b01 + a22 * b02;
        out[3] = a03 * b00 + a13 * b01 + a23 * b02;
        out[4] = a00 * b10 + a10 * b11 + a20 * b12;
        out[5] = a01 * b10 + a11 * b11 + a21 * b12;
        out[6] = a02 * b10 + a12 * b11 + a22 * b12;
        out[7] = a03 * b10 + a13 * b11 + a23 * b12;
        out[8] = a00 * b20 + a10 * b21 + a20 * b22;
        out[9] = a01 * b20 + a11 * b21 + a21 * b22;
        out[10] = a02 * b20 + a12 * b21 + a22 * b22;
        out[11] = a03 * b20 + a13 * b21 + a23 * b22;

  if (a != out) { // If the source and destination differ, copy the unchanged last row
          out[12] = a[12];
          out[13] = a[13];
          out[14] = a[14];
          out[15] = a[15];
        }
};

void vec3TransformMat4(rvec out, rvec a, real m[16]) {
        real x = a[0], y = a[1], z = a[2],
          w = m[3] * x + m[7] * y + m[11] * z + m[15];
        w = w || 1.0;
        out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
        out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
        out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
};


void setupPath(t_path_bag *path_bag) {
  int i, d;
  real r;
  rvec *pathVectors, *centerVecs;

  snew(path_bag->pathDirections, path_bag->nPath+1);
  snew(path_bag->waypoints_lambda, path_bag->nPath+1); path_bag->waypoints_lambda[0] = 0.0;
  snew(path_bag->pathVectorsMagnitude, path_bag->nPath);
  snew(path_bag->tangents, path_bag->nPath+1);
  snew(path_bag->maxAngles, path_bag->nPath+1);
  snew(path_bag->centerPoints, path_bag->nPath+1);
  snew(path_bag->refVectors, path_bag->nPath+1);
  snew(path_bag->planeVectors, path_bag->nPath+1);
  snew(path_bag->linear, path_bag->nPath+1);

  snew(pathVectors, path_bag->nPath);
  snew(centerVecs, path_bag->nPath+1);

  for (i=1; i<path_bag->nPath+1; i++) {
    rvec_sub(path_bag->path[i], path_bag->path[i-1], pathVectors[i-1]);

    r = norm(pathVectors[i-1]);
    svmul(1./r, pathVectors[i-1], path_bag->pathDirections[i-1]);
    path_bag->waypoints_lambda[i] = path_bag->waypoints_lambda[i-1]+r;

    path_bag->pathVectorsMagnitude[i-1] = r;
  }
  copy_rvec(path_bag->pathDirections[path_bag->nPath-1], path_bag->pathDirections[path_bag->nPath]);

  for (i=1; i<path_bag->nPath; i++) {
    rvec_sub(path_bag->path[i+1], path_bag->path[i-1], path_bag->tangents[i]);
    svmul(gmx::invsqrt(norm2(path_bag->tangents[i])), path_bag->tangents[i], path_bag->tangents[i]);
  }

  copy_rvec(path_bag->pathDirections[0], path_bag->tangents[0]);
  copy_rvec(path_bag->pathDirections[path_bag->nPath-1], path_bag->tangents[path_bag->nPath]);


  real smallest;

  rvec N, tmp;

  // how about just using PTF...

  smallest = 1e99;
  if (path_bag->tangents[0][0] < smallest) {
    smallest = path_bag->tangents[0][0];
    N[0] = 1; N[1] = 0; N[2] = 0;
  }
  if (path_bag->tangents[0][1] < smallest) {
    smallest = path_bag->tangents[0][0];
    N[0] = 0; N[1] = 1; N[2] = 0;
  }
  if (path_bag->tangents[0][2] < smallest) {
    smallest = path_bag->tangents[0][0];
    N[0] = 0; N[1] = 0; N[2] = 1;
  }

  cprod(path_bag->tangents[0], N, tmp);
  svmul(gmx::invsqrt(norm2(tmp)), tmp, tmp);

  cprod(path_bag->tangents[0], tmp, N);
  for (d=0; d<DIM; d++) centerVecs[0][d] = N[d];

  real identityMatrix[] = {1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.};
  real rotationMatrix[] = {1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.};
  real theta;

  for (i=1; i<path_bag->nPath+1; i++) {
    cprod(path_bag->tangents[i-1], path_bag->tangents[i], tmp);
    if (norm(tmp) > 1e-6) {
      svmul(gmx::invsqrt(norm2(tmp)), tmp, tmp);
      theta = acos(iprod(path_bag->tangents[i-1], path_bag->tangents[i]));
      mat4Rotate(rotationMatrix, identityMatrix, theta, tmp);
      vec3TransformMat4(N, N, rotationMatrix);
    }
    else path_bag->linear[i-1] = 1;
    for (d=0; d<DIM; d++) centerVecs[0][d] = N[d];
  }

  real fac;
  rvec CP, one, two;

  for (i=0; i<path_bag->nPath; i++) {
    if (path_bag->linear[i]) continue;

    fac = -iprod(path_bag->tangents[i], pathVectors[i]);
    for (d=0; d<DIM; d++) CP[d] = (centerVecs[i+1][d] * fac) + path_bag->path[i+1][d];

    rvec_sub(path_bag->path[i], CP, one);
    svmul(gmx::invsqrt(norm2(one)), one, one);

    rvec_sub(path_bag->path[i+1], CP, two);
    svmul(gmx::invsqrt(norm2(two)), two, two);

    cprod(one, two, N);
    svmul(gmx::invsqrt(norm2(N)), N, N);

    path_bag->maxAngles[i] = 1 - (iprod(one, two)+1)*.5;

    for (d=0; d<DIM; d++) {
      path_bag->centerPoints[i][d] = CP[d];
      path_bag->refVectors[i][d] = one[d];
      path_bag->planeVectors[i][d] = N[d];
    }
  }

  sfree(pathVectors);
}

