#include "bekker.h"

#include "gromacs/domdec/localatomset.h"
#include "gromacs/domdec/localatomsetmanager.h"
#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

void bekker_comball_init(t_comball_bag *comball_bag, rvec *x, const t_mdatoms *md, PbcType ePBC, matrix box, const t_commrec *cr) {
  int i, ii;
  comball_bag->ligand.groupAtomLocs.resize(comball_bag->ligand.nat);
  comball_bag->ligand.masses.resize(comball_bag->ligand.nat);
  
  if (MASTER(cr)) {
    comball_bag->ligand.tmass = 0.0;
    for (i=0; i<comball_bag->ligand.nat; i++) {
      ii = comball_bag->ligand.global_atoms[i];
      comball_bag->ligand.masses[i] = md->massT[ii];
      comball_bag->ligand.tmass += md->massT[ii];
    }
    comball_bag->ligand.invtm = 1.0 / comball_bag->ligand.tmass;

    bekker_calc_initial_com(&comball_bag->ligand, x, ePBC, box);
  }

  if (PAR(cr)) {
    gmx_bcast(sizeof(real)*3, &comball_bag->ligand.com[0], cr->mpi_comm_mygroup);
    gmx_bcast(sizeof(real)*comball_bag->ligand.nat, &comball_bag->ligand.masses[0], cr->mpi_comm_mygroup);
    gmx_bcast(sizeof(real), &comball_bag->ligand.tmass, cr->mpi_comm_mygroup);
  }
  comball_bag->ligand.invtm = 1.0 / comball_bag->ligand.tmass;
}

void bekker_apply_comball(t_comball_bag *comball_bag, rvec *f, rvec *x, real *bekker_epot, PbcType ePBC, const matrix box) {
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);
  
  int d, ii, gidx;
  rvec vec;
  real r, k, invr;
  
  auto localAtomIndices = comball_bag->ligand.atomSet->localIndex();
  auto search = comball_bag->ligand.atomSet->collectiveIndex();
  
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    
    pbc_dx(&pbc, x[ii], comball_bag->ligand.com, vec);

    invr = gmx::invsqrt(norm2(vec)); r = invr*norm2(vec);

    if (r > comball_bag->radius) {
      r -= comball_bag->radius;
      svmul(invr, vec, vec);
      k = comball_bag->fc;
      *bekker_epot += .5 * k * r*r;
      for (d=0; d<DIM; d++) f[ii][d] -= vec[d] * r * k;
    }

    for (d=0; d<DIM; d++) comball_bag->ligand.groupAtomLocs[gidx][d] = x[ii][d];
  }
}
