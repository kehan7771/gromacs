
#include <map>

#include "gromacs/domdec/localatomsetmanager.h"
#include "gromacs/domdec/localatomset.h"

typedef struct {
  int nat;
  std::vector<int> global_atoms;
    
  /* Variables not present in mdp, but used at run time */
  double        invtm;      /* inverse total mass of the group: 1/wscale sum w m */
  real        tmass;
  rvec        com;        /* com of group */
  rvec        initial_com;    /* com of group at start of simulation*/
  
  std::vector<real> masses;
  std::vector<gmx::RVec> groupAtomLocs;
  
  gmx::LocalAtomSet *atomSet;
  std::vector<gmx::LocalAtomSet> atomSet_baka;
} t_bekker_group;

typedef struct {
  t_bekker_group     group;
  rvec              *refpoints;
  real              *forceConstants;
  real              *flatRanges;
  real              *rmax;
} t_posres_bag;

typedef struct {
  int                nrestr;
  int               *atm1;
  int               *atm2;
  real              *r;
  real              *r_free;
  real              *k;
  real              *rmax;
} t_disres_bag;

typedef struct {
  int               nPath;
  rvec             *path;
  rvec             *pathDirections;
  real             *waypoints_lambda;
  real             *pathVectorsMagnitude;
  rvec             *tangents;

  real             *maxAngles;
  rvec             *centerPoints;
  rvec             *refVectors;
  rvec             *planeVectors;
  gmx_bool         *linear;

} t_path_bag;

typedef struct {
  real              lambda_from, lambda_to;
  real              lambda_force;
  real              orth_force;
  real              mx_orth_dist;
  real              mx_orth_dist2;

  gmx_bool          terminate_mode, terminateMD;
  real              terminate_lambda;
  
  t_path_bag        path_bag;
  
  int               ngroups;
  
  std::vector<t_bekker_group> ligands;
} t_cylinder_bag;


typedef struct {
  t_bekker_group    group;
  
  rvec              *refpoints, *xprime, *delta_temp;
	
  real               fc;
  
  real               boxDMZ;           
  ivec               boxDMZdims;
  
  ivec               dotrans, dorot;
  
  gmx_bool           skipWhole;
} t_comres_bag;

typedef struct {
  int                nfiles;
  std::vector<t_comres_bag>   bags;
} t_comres_container;

typedef struct {
  t_bekker_group     ligand;
	
  real               fc;
  real               radius;
  
} t_comball_bag;


typedef struct {
  gmx_bool          heatingMode, includeRestrEPOT;
  real              initialTemp, heatingTemp;
  real              minEnergyBin;
  real              binSize;
  real             *scaleFactor;
  int               nBins;
} t_fb_bag;

typedef struct {
  rvec              windowCenter, windowCenter_initial;
  rvec              localAxis;
  real              orth_force;
  real              mx_orth_dist;
  gmx_bool          do1D, distance_mode, signed_distance_mode;
  real              k;
  t_bekker_group    ligand, ligand2;
  int               outSteps;
  real              smd_speed;
  
  char              group_name[4096], group_name2[4096];
  FILE              *forces_fp, 
                    *mc_fp,
                    *forces2_fp,
                    *mc2_fp,
                    *r_fp,
                    *time_fp;
} t_us_bag;

typedef struct {
  int                nfiles;
  std::vector<t_us_bag>   bags;
} t_us_container;

typedef struct {
  int N;
  int shock_steps;
  real max_vel;
  real border;
  char              group_name[4096];
  t_bekker_group    solvent;
  
} t_hypersound_bag;

typedef struct {
  
  gmx_bool        do_fb;
  t_fb_bag       *fb_bag;
    
  gmx_bool        do_us;
  t_us_container *us_container;

  gmx_bool        do_posres;
  t_posres_bag   *posres_bag;

  gmx_bool        do_disres;
  t_disres_bag   *disres_bag;

  gmx_bool        do_cylinder;
  t_cylinder_bag *cylinder_bag;

  gmx_bool        do_hypersound;
  t_hypersound_bag *hypersound_bag;
    
  gmx_bool        do_comres;
  t_comres_container *comres_container;
  
  gmx_bool        do_comball;
  t_comball_bag  *comball_bag;
  
  gmx_bool        isCheckpointingStep;
  
  real epot = 0;
  
} t_bekker_data;

// end bekker

