
#ifndef GMX_BEKKER_H
#define GMX_BEKKER_H

#include "gromacs/fileio/readinp.h"

#include "gromacs/utility/cstringutil.h"
#include "gromacs/math/vec.h"
#include "gromacs/topology/index.h"

#include "gromacs/mdtypes/mdatom.h"
#include "gromacs/pbcutil/pbc.h"
#include "gromacs/commandline/filenm.h"
#include "gromacs/mdtypes/enerdata.h"

#include "gromacs/mdtypes/inputrec.h"
#include "gromacs/utility/inmemoryserializer.h"

#include "gromacs/mdtypes/state.h"

#include "bekker.h"
#include "gromacs/domdec/ga2la.h"


//for hypersound
void read_hypersound_params(std::vector<t_inpfile> *inp, t_hypersound_bag *hypersound_bag, warninp_t wi);
void configure_hypersound(t_hypersound_bag *hypersound_bag, const t_blocka *grps, char **gnames);
void bekker_hypersound_io(gmx::ISerializer* serializer, t_hypersound_bag *hypersound_bag);

void araki_apply_hypersound(t_hypersound_bag *hypersound_bag, rvec *x, rvec *v, const matrix box, int64_t step, const t_commrec *cr);
//end hypersound

// fb-mcmd
void read_fb_params(std::vector<t_inpfile> *inp, t_fb_bag *fb_bag, warninp_t wi);
void bekker_fb_io(gmx::ISerializer* serializer, t_fb_bag *fb_bag);

void bekker_fb_init(int nfile, const t_filenm fnm[]);
void bekker_fb_finish();
real apply_fb(t_fb_bag *fb_bag, const t_mdatoms *md, rvec *f, real *bekker_epot, gmx_enerdata_t *enerd, int64_t step, gmx_bool doFlush, gmx_bool fullEpot, const t_commrec *cr);

// US

void read_us_params(std::vector<t_inpfile> *inp, t_us_container *us_container, warninp_t wi);
void configure_us(t_us_container *us_container, const t_blocka *grps, char **gnames);
void bekker_us_io(gmx::ISerializer* serializer, t_us_container *us_container);

void bekker_us_init(t_us_container *us_container, int nfile, const t_filenm fnm[], rvec *x, const t_mdatoms *md, PbcType ePBC, const matrix box, const t_commrec *cr);
void bekker_us_finish(t_us_container *us_container);
void apply_us(t_us_container *us_container, rvec *f, rvec *x, int64_t step, double dt, PbcType ePBC, const matrix box, gmx_bool doFlush, const t_commrec *cr);
void us_put_coms_in_box(t_us_container *us_container, PbcType ePBC, const matrix box);

// posres

void read_posres_params(std::vector<t_inpfile> *inp, t_posres_bag *posres_bag);
void bekker_posres_io(gmx::ISerializer* serializer, t_posres_bag *posres_bag);
void bekker_apply_posres(const t_posres_bag *posres_bag, const rvec *x, rvec *f, real *bekker_epot, PbcType ePBC, const matrix box);

// disres

void read_disres_params(std::vector<t_inpfile> *inp, t_disres_bag *disres_bag);
void bekker_disres_io(gmx::ISerializer* serializer, t_disres_bag *disres_bag);
void bekker_apply_disres(const t_disres_bag *disres_bag, real *bekker_epot, PbcType ePBC, const matrix box, const rvec *x, rvec *f, const t_commrec *cr);

// cylinder

void read_cylinder_params(std::vector<t_inpfile> *inp, t_cylinder_bag *cylinder_bag, warninp_t wi);
void configure_cylinder(t_cylinder_bag *cylinder_bag, const t_blocka *grps, char **gnames);
void bekker_cylinder_io(gmx::ISerializer* serializer, t_cylinder_bag *cylinder_bag);

void bekker_init_cylinder(t_cylinder_bag *cylinder_bag, int nfile, const t_filenm fnm[], rvec *x, const t_mdatoms *md, PbcType ePBC, const matrix box, const t_commrec *cr);
void bekker_finish_cylinder(t_cylinder_bag *cylinder_bag);
void apply_cylinder(t_cylinder_bag *cylinder_bag, rvec *f, rvec *x, real *bekker_epot, PbcType ePBC, const matrix box, int64_t step, gmx_bool doFlush, const t_commrec *cr);
void cylinder_put_coms_in_box(t_cylinder_bag *cylinder_bag, PbcType ePBC, const matrix box);

// comres
void read_comres_params(std::vector<t_inpfile> *inp, t_comres_container *comres_container, warninp_t wi);
void bekker_comres_io(gmx::ISerializer* serializer, t_comres_container *comres_container);

void bekker_init_comres(t_comres_bag *comres_bag, const t_mdatoms *md, const rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr);
void bekker_apply_comres(t_comres_bag *comres_bag, const rvec *x, rvec *f, real *bekker_epot, const matrix box, const t_commrec *cr);
void bekker_comres_update(t_comres_bag *comres_bag, rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr);

// com ball
void read_comball_params(std::vector<t_inpfile> *inp, t_comball_bag *comball_bag, warninp_t wi);
void configure_comball(t_comball_bag *comball_bag, const t_blocka *grps, char **gnames);
void bekker_comball_io(gmx::ISerializer* serializer, t_comball_bag *comball_bag);
void bekker_comball_init(t_comball_bag *comball_bag, rvec *x, const t_mdatoms *md, PbcType ePBC, matrix box, const t_commrec *cr);
void bekker_apply_comball(t_comball_bag *comball_bag, rvec *f, rvec *x, real *bekker_epot, PbcType ePBC, const matrix box);

// general
void bekker_calc_com(t_bekker_group *bgrp, rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr);
void bekker_calc_initial_com(t_bekker_group *bgrp, rvec *x, PbcType ePBC, const matrix box, rvec refPoint=NULL);
void put_point_in_box(PbcType ePBC, const matrix box, rvec x);
void project2Path(const t_path_bag *path_bag, const rvec com, rvec *lambda_3d, real *lambda, rvec *dir, int *idx_out);
void setupPath(t_path_bag *path_bag);

// inline

inline void bekker_distribute_force(t_bekker_group *bgrp, rvec *f, const rvec force, int sign) {
  int ii, d, gidx;
  
  auto localAtomIndices = bgrp->atomSet->localIndex();
  auto search = bgrp->atomSet->collectiveIndex();
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    for (d=0; d<DIM; d++) f[ii][d] -= sign * bgrp->invtm * force[d] * bgrp->masses[gidx];
  }
};

inline void bekker_distribute_force_update_bgrp(t_bekker_group *bgrp, const rvec *x, rvec *f, const rvec force, int sign) {
  int ii, d, gidx;
  auto localAtomIndices = bgrp->atomSet->localIndex();
  auto search = bgrp->atomSet->collectiveIndex();
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    for (d=0; d<DIM; d++) {
      f[ii][d] -= sign * bgrp->invtm * force[d] * bgrp->masses[gidx];
      bgrp->groupAtomLocs[gidx][d] = x[ii][d];
    }
  }
};

inline void bekker_update_bgrp(t_bekker_group *bgrp, const rvec *x) {
  int ii, d, gidx;
  auto localAtomIndices = bgrp->atomSet->localIndex();
  auto search = bgrp->atomSet->collectiveIndex();
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    for (d=0; d<DIM; d++) bgrp->groupAtomLocs[gidx][d] = x[ii][d];
  }
};


void dd_register_group(t_bekker_group *ligand, gmx::LocalAtomSetManager* atomSets);
#endif
