// make a version of US that can handle both COM based reference point and landmark-atom based reference point
// add option to do SMD (so make the particle move at constant speed along lambda)

#include "bekker.h"
#include <stdbool.h>
#include <stdlib.h>

#include "gromacs/domdec/localatomset.h"
#include "gromacs/domdec/localatomsetmanager.h"
#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

#include <iostream>

void bekker_us_init(t_us_container *us_container, int nfile, const t_filenm fnm[], rvec *x, const t_mdatoms *md, PbcType ePBC, const matrix box, const t_commrec *cr) { 
  int i, ii;
  t_us_bag *us_bag;
  
  std::string base(opt2fn("-g", nfile, fnm)); base.erase(base.length()-4);
  char forces_fn[STRLEN], mc_fn[STRLEN], time_fn[STRLEN];

  for (int f=0; f<us_container->nfiles; f++) {
    us_bag = &us_container->bags[f];

    us_bag->ligand.groupAtomLocs.resize(us_bag->ligand.nat);
    us_bag->ligand.masses.resize(us_bag->ligand.nat);
    
    if (us_bag->distance_mode) {
      us_bag->ligand2.groupAtomLocs.resize(us_bag->ligand2.nat);
      us_bag->ligand2.masses.resize(us_bag->ligand2.nat);
    }

    if (MASTER(cr)) {
      us_bag->ligand.tmass = 0.0;
      for (i=0; i<us_bag->ligand.nat; i++) {
        ii = us_bag->ligand.global_atoms[i];
        us_bag->ligand.masses[i] = md->massT[ii];
        us_bag->ligand.tmass += md->massT[ii];
      }
      us_bag->ligand.invtm = 1.0 / us_bag->ligand.tmass;
      
      if (us_bag->distance_mode) {
        us_bag->ligand2.tmass = 0.0;
        for (i=0; i<us_bag->ligand2.nat; i++) {
          ii = us_bag->ligand2.global_atoms[i];
          us_bag->ligand2.masses[i] = md->massT[ii];
          us_bag->ligand2.tmass += md->massT[ii];
        }
        us_bag->ligand2.invtm = 1.0 / us_bag->ligand2.tmass;

        bekker_calc_initial_com(&us_bag->ligand, x, ePBC, box);
        bekker_calc_initial_com(&us_bag->ligand2, x, ePBC, box);
      }
      else {
        if (us_bag->smd_speed != 0.0) bekker_calc_initial_com(&us_bag->ligand, x, ePBC, box);
        else {
          rvec tmp; tmp[0] = us_bag->windowCenter[0]; tmp[1] = us_bag->windowCenter[1]; tmp[2] = us_bag->windowCenter[2];
          bekker_calc_initial_com(&us_bag->ligand, x, ePBC, box, tmp);
        }
      }
      
      sprintf(forces_fn, "%s.%d.us.forces", base.c_str(), f);
      sprintf(mc_fn, "%s.%d.us.mc", base.c_str(), f);
      sprintf(time_fn, "%s.%d.us.time", base.c_str(), f);
      
      us_bag->forces_fp = fopen(forces_fn, "w");
      us_bag->mc_fp = fopen(mc_fn, "w");
      us_bag->time_fp = fopen(time_fn, "w");
      
      if (us_bag->distance_mode) {
        sprintf(mc_fn, "%s.%d.us.forces2", base.c_str(), f);
        us_bag->forces2_fp = fopen(mc_fn, "w");
        
        sprintf(mc_fn, "%s.%d.us.mc2", base.c_str(), f);
        us_bag->mc2_fp = fopen(mc_fn, "w");

        sprintf(mc_fn, "%s.%d.us.r", base.c_str(), f);
        us_bag->r_fp = fopen(mc_fn, "w");
      }
      
    }

    if (PAR(cr)) {
      gmx_bcast(sizeof(real)*3, &us_bag->ligand.com[0], cr->mpi_comm_mygroup);
      gmx_bcast(sizeof(real)*us_bag->ligand.nat, &us_bag->ligand.masses[0], cr->mpi_comm_mygroup);
      gmx_bcast(sizeof(real), &us_bag->ligand.tmass, cr->mpi_comm_mygroup);
      
      if (us_bag->distance_mode) {
        gmx_bcast(sizeof(real)*3, &us_bag->ligand2.com[0], cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(real)*us_bag->ligand2.nat, &us_bag->ligand2.masses[0], cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(real), &us_bag->ligand2.tmass, cr->mpi_comm_mygroup);
      }
    }
    
    us_bag->ligand.invtm = 1.0 / us_bag->ligand.tmass;
    if (us_bag->distance_mode) us_bag->ligand2.invtm = 1.0 / us_bag->ligand2.tmass;
    
    if (us_bag->smd_speed != 0.0) {
      if (us_bag->distance_mode) {
        t_pbc  pbc; set_pbc(&pbc, ePBC, box);
        rvec vec;
        pbc_dx(&pbc, us_bag->ligand.com, us_bag->ligand2.com, vec);
        us_bag->windowCenter_initial[0] = us_bag->windowCenter[0] = iprod(vec, us_bag->localAxis);
      }
      else {
        us_bag->windowCenter_initial[0] = us_bag->windowCenter[0] = us_bag->ligand.com[0];
        us_bag->windowCenter_initial[1] = us_bag->windowCenter[1] = us_bag->ligand.com[1];
        us_bag->windowCenter_initial[2] = us_bag->windowCenter[2] = us_bag->ligand.com[2];
      }
    }
  }
}

void bekker_us_finish(t_us_container *us_container) {
  t_us_bag *us_bag;
  for (int f=0; f<us_container->nfiles; f++) {
    us_bag = &us_container->bags[f];
    fclose(us_bag->forces_fp);
    fclose(us_bag->mc_fp);
    fclose(us_bag->time_fp);
    if (us_bag->distance_mode) {
      fclose(us_bag->forces2_fp);
      fclose(us_bag->mc2_fp);
      fclose(us_bag->r_fp);
    }
  }
}

void us_pos(t_us_bag *us_bag, rvec *f, rvec *x, int64_t step, double dt, t_pbc *pbc, gmx_bool doFlush, const t_commrec *cr) {
  rvec bias, measuredForce, l3rvec;
  double lc_r;
  int ii, d, gidx;

  clear_rvec(measuredForce);
  clear_rvec(l3rvec);

  if (us_bag->do1D) {
    rvec_sub(us_bag->ligand.com, us_bag->windowCenter, bias);
    lc_r = iprod(bias, us_bag->localAxis); // distance between COM and the plane (defined by the unit vector of the plane)
    svmul(lc_r, us_bag->localAxis, bias); // set the 3D vector (unit vector of the plane with the previously determined distance)
    lc_r = fabs(lc_r);
    rvec_add(us_bag->windowCenter, bias, l3rvec); rvec_sub(us_bag->ligand.com, l3rvec, l3rvec);
  }
  else {
    rvec_sub(us_bag->ligand.com, us_bag->windowCenter, bias);
    lc_r = norm(bias);
    lc_r -= us_bag->mx_orth_dist; // spherical fb
  }
  
  if (lc_r > 0) svmul(us_bag->k, bias, bias);
  else clear_rvec(bias);

  // restrain the ligand to stay within the cylinder (perpendicular to lambda)
  if (us_bag->do1D) {
    lc_r = norm(l3rvec);
    if (lc_r > us_bag->mx_orth_dist) {
      svmul((1./lc_r)*-us_bag->orth_force, l3rvec, l3rvec);
      svmul(lc_r - us_bag->mx_orth_dist, l3rvec, l3rvec);
      rvec_sub(bias, l3rvec, bias);
    }
  }

  svmul(us_bag->ligand.invtm, bias, bias);
  auto localAtomIndices = us_bag->ligand.atomSet->localIndex();
  auto search = us_bag->ligand.atomSet->collectiveIndex();

  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];

    for (d=0; d<DIM; d++) {
      measuredForce[d] += f[ii][d];
      f[ii][d] -= bias[d] * us_bag->ligand.masses[gidx];
      us_bag->ligand.groupAtomLocs[gidx][d] = x[ii][d];
    }
  }
  
  if (us_bag->outSteps == 0 || step%us_bag->outSteps == 0) {
    if (PAR(cr)) gmx_sum(3, measuredForce, cr);
    if (MASTER(cr)) {
      pbc_dx(pbc, us_bag->ligand.com, us_bag->windowCenter, l3rvec);
      for (d=0; d<DIM; d++) bias[d] = us_bag->windowCenter[d] + l3rvec[d];
      
      fwrite(measuredForce, sizeof(rvec), 1, us_bag->forces_fp);
      fwrite(bias, sizeof(rvec), 1, us_bag->mc_fp);
      fwrite(&step, sizeof(int64_t), 1, us_bag->time_fp);
    }
  }
  if (doFlush) { // make sure that the data is flushed to disk together with the restart file
    fflush(us_bag->forces_fp);
    fflush(us_bag->mc_fp);
    fflush(us_bag->time_fp);
  }
  
  
  if (us_bag->smd_speed != 0.0) {
    us_bag->windowCenter[0] = us_bag->windowCenter_initial[0] + us_bag->localAxis[0] * (us_bag->smd_speed*dt*step);
    us_bag->windowCenter[1] = us_bag->windowCenter_initial[1] + us_bag->localAxis[1] * (us_bag->smd_speed*dt*step);
    us_bag->windowCenter[2] = us_bag->windowCenter_initial[2] + us_bag->localAxis[2] * (us_bag->smd_speed*dt*step);
  }
}

void us_put_coms_in_box(t_us_container *us_container, PbcType ePBC, const matrix box) {
  for (int fid=0; fid<us_container->nfiles; fid++) {
    put_point_in_box(ePBC, box, us_container->bags[fid].ligand.com);
    if (us_container->bags[fid].distance_mode) put_point_in_box(ePBC, box, us_container->bags[fid].ligand2.com);
  }
}

void us_dis(t_us_bag *us_bag, rvec *f, rvec *x, int64_t step, double dt, t_pbc *pbc, gmx_bool doFlush, const t_commrec *cr) {
  rvec vec, measuredForce, measuredForce2;
  int ii, d, gidx;

  clear_rvec(measuredForce); clear_rvec(measuredForce2);

  pbc_dx(pbc, us_bag->ligand.com, us_bag->ligand2.com, vec);
  
  real rc = us_bag->windowCenter[0];
  real r = iprod(vec, us_bag->localAxis);
  real sign = 1;
  if (!us_bag->signed_distance_mode) {
    if (r < 0) sign = -1;
    r = fabs(r);
    rc = fabs(rc);
  }
  real DIFF = (r-rc)*sign;
  
  rvec tmp;
  for (d=0; d<DIM; d++) tmp[d] = us_bag->localAxis[d] * DIFF;
  rvec ZERO; clear_rvec(ZERO);
  pbc_dx(pbc, tmp, ZERO, tmp);
  DIFF = iprod(tmp, us_bag->localAxis);

  real sf1 = (us_bag->k*DIFF)/us_bag->ligand.tmass;
  real sf2 = (us_bag->k*DIFF)/us_bag->ligand2.tmass;
  
  auto localAtomIndices = us_bag->ligand.atomSet->localIndex();
  auto search = us_bag->ligand.atomSet->collectiveIndex();

  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];

    for (d=0; d<DIM; d++) {
      measuredForce[d] += f[ii][d];
      f[ii][d] -= us_bag->localAxis[d] * us_bag->ligand.masses[gidx] * sf1;
      us_bag->ligand.groupAtomLocs[gidx][d] = x[ii][d];
    }
  }
  
  localAtomIndices = us_bag->ligand2.atomSet->localIndex();
  search = us_bag->ligand2.atomSet->collectiveIndex();

  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];

    for (d=0; d<DIM; d++) {
      measuredForce2[d] += f[ii][d];
      f[ii][d] += us_bag->localAxis[d] * us_bag->ligand2.masses[gidx] * sf2;
      us_bag->ligand2.groupAtomLocs[gidx][d] = x[ii][d];
    }
  }
  
  if (us_bag->outSteps == 0 || step%us_bag->outSteps == 0) {
    if (PAR(cr)) {
      gmx_sum(3, measuredForce, cr);
      gmx_sum(3, measuredForce2, cr);
    }
    if (MASTER(cr)) {
      fwrite(measuredForce, sizeof(rvec), 1, us_bag->forces_fp);
      fwrite(measuredForce2, sizeof(rvec), 1, us_bag->forces2_fp);
      fwrite(us_bag->ligand.com, sizeof(rvec), 1, us_bag->mc_fp);
      fwrite(us_bag->ligand2.com, sizeof(rvec), 1, us_bag->mc2_fp);
      fwrite(&r, sizeof(real), 1, us_bag->r_fp);
      fwrite(&step, sizeof(int64_t), 1, us_bag->time_fp);
    }
  }

  if (MASTER(cr) && doFlush) { // make sure that the data is flushed to disk together with the restart file
    fflush(us_bag->forces_fp);
    fflush(us_bag->forces2_fp);
    fflush(us_bag->mc_fp);
    fflush(us_bag->mc2_fp);
    fflush(us_bag->r_fp);
    fflush(us_bag->time_fp);
  }

  if (us_bag->smd_speed != 0.0) us_bag->windowCenter[0] = us_bag->windowCenter_initial[0] + (us_bag->smd_speed*dt*step);
}

void apply_us(t_us_container *us_container, rvec *f, rvec *x, int64_t step, double dt, PbcType ePBC, const matrix box, gmx_bool doFlush, const t_commrec *cr) {
  t_pbc pbc; set_pbc(&pbc, ePBC, box);
  
  t_us_bag *us_bag;
  for (int fid=0; fid<us_container->nfiles; fid++) {
    us_bag = &us_container->bags[fid];
    if (us_bag->distance_mode) us_dis(us_bag, f, x, step, dt, &pbc, doFlush, cr);
    else us_pos(us_bag, f, x, step, dt, &pbc, doFlush, cr);
  }
}
