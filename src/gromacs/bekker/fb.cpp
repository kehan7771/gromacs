
#include "bekker.h"

#include <stdlib.h>

#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

struct t_fb_tmp_bag {
  FILE *energies_fp;
  // real epot_nb;
} fb_tmp_bag;



void bekker_fb_init(int nfile, const t_filenm fnm[]) {
  char energies_fn[STRLEN];
  std::string base(opt2fn("-g", nfile, fnm)); base.erase(base.length()-4);
  sprintf(energies_fn, "%s.fb.ene", base.c_str());
  fb_tmp_bag.energies_fp = fopen(energies_fn, "w");
}

void bekker_fb_finish() {
  fclose(fb_tmp_bag.energies_fp);
}

real apply_fb(t_fb_bag *fb_bag, const t_mdatoms *md, rvec *f, real *bekker_epot, gmx_enerdata_t *enerd, int64_t step, gmx_bool doFlush, gmx_bool fullEpot, const t_commrec *cr) {
  real epot, fbfactor;
  int i, d, currentBin;

  epot = enerd->term[F_EPOT];
  if (fb_bag->includeRestrEPOT) epot += *bekker_epot;
  
  if (PAR(cr)) gmx_sum(1, &epot, cr);
  // if (fullEpot) fb_tmp_bag.epot_nb = enerd->term[F_LJC14_Q] + enerd->term[F_LJC_PAIRS_NB] + enerd->term[F_LJ] + enerd->term[F_COUL_SR] + enerd->term[F_RF_EXCL] + enerd->term[F_COUL_RECIP] + enerd->term[F_LJ_RECIP];
  // else epot += fb_tmp_bag.epot_nb;

  if (MASTER(cr)) {
    fwrite(&epot, sizeof(real), 1, fb_tmp_bag.energies_fp);
    if (doFlush) fflush(fb_tmp_bag.energies_fp); // flush the buffer whenever the cpt file is written
  }
  
  if (fb_bag->heatingMode) fbfactor = fb_bag->initialTemp / fb_bag->heatingTemp;
  else {
    currentBin = floor((epot - fb_bag->minEnergyBin) / fb_bag->binSize);
    if (currentBin < 0) fbfactor = fb_bag->scaleFactor[0];
    else if (currentBin > fb_bag->nBins-1) fbfactor = fb_bag->scaleFactor[fb_bag->nBins-1];
    else fbfactor = fb_bag->scaleFactor[currentBin];
  }
  
  if (step%5000 == 0 && MASTER(cr)) fprintf(stdout, "%ld %f %f\n", step, epot, fbfactor);
  
  for (i=0; i<md->homenr; i++) for (d=0; d<DIM; d++) f[i][d] *= fbfactor;

  return fbfactor;
}
