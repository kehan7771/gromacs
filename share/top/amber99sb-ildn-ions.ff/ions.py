mono = """Monovalent ion parameters for Ewald and TIP3P water from Joung & Cheatham JPCB (2008)
MASS
Li+   6.94         0.029               lithium   pol: J. Phys. Chem. 11,1541,(1978)
Na+  22.99         0.250               sodium    pol: J. Phys. Chem. 11,1541,(1978)
K+   39.10         1.060               potassium
Rb+  85.47                             rubidium
Cs+ 132.91                             cesium
F-   19.00         0.320               fluorine
Cl-  35.45         1.910               chlorine  (Applequist)
Br-  79.90         2.880               bromine   (Applequist)
I-   126.9         4.690               iodine    (Applequist)
 
NONBON
  Li+      1.025     0.0279896
  Na+      1.369     0.0874393
  K+       1.705     0.1936829
  Rb+      1.813     0.3278219
  Cs+      1.976     0.4065394
  F-       2.303     0.0033640
  Cl-      2.513     0.0355910
  Br-      2.608     0.0586554
  I-       2.860     0.0536816"""

di = """Divalent ion parameters(CM set) for Particle Mesh Ewald and TIP3P water model from Li, Roberts, Chakravorty and Merz (JCTC 2013, 9, 2733-2748) 
MASS
Be2+ 9.01
Cu2+ 63.55
Ni2+ 58.69
Pt2+ 195.08
Zn2+ 65.4
Co2+ 58.93
Pd2+ 106.42
Ag2+ 107.87
Cr2+ 52.00
Fe2+ 55.85
Mg2+ 24.305
V2+  50.94
Mn2+ 54.94
Hg2+ 200.59
Cd2+ 112.41
Yb2+ 173.05
Ca2+ 40.08
Sn2+ 118.71
Pb2+ 207.2
Eu2+ 151.96
Sr2+ 87.62
Sm2+ 150.36 
Ba2+ 137.33
Ra2+ 226.03

BOND

ANGLE 

DIHE

NONBON
Be2+  0.956  0.00000395
Cu2+  1.218  0.00148497
Ni2+  1.255  0.00262320
Pt2+  1.266  0.00307642
Zn2+  1.271  0.00330286
Co2+  1.299  0.00483892
Pd2+  1.303  0.00509941
Ag2+  1.336  0.00770969
Cr2+  1.346  0.00868178
Fe2+  1.353  0.00941798
Mg2+  1.360  0.01020237
V2+   1.364  0.01067299
Mn2+  1.407  0.01686710
Hg2+  1.407  0.01686710
Cd2+  1.412  0.01773416
Yb2+  1.642  0.10185975
Ca2+  1.649  0.10592870
Sn2+  1.666  0.11617738
Pb2+  1.745  0.17018074
Eu2+  1.802  0.21475916
Sr2+  1.810  0.22132374
Sm2+  1.819  0.22878796
Ba2+  2.019  0.40664608
Ra2+  2.019  0.40664608
"""

nonbonded = {}

rename = {}
rename["Ca"] = "C0"
rename["Mg"] = "MG"

renameres = {}
renameres["C0"] = "CA"

def splitnonalpha(s):
  pos = 1
  while pos < len(s) and s[pos].isalpha(): pos+=1
  return (s[:pos], s[pos:])

def process(inp, comment = ""):
  mode = None
  modes = ["MASS", "BOND", "ANGLE", "DIHE", "NONBON"]

  for line in inp.splitlines():
    line = line.strip()
    if not line: continue
    if line in modes:
      mode = line
      continue
    if mode == "MASS":
      line = line.split()
      
      an, charge = splitnonalpha(line[0])
      an = rename.get(an, an)
      if len(charge) == 1: charge = "1"+charge
      charge = int(charge[-1]+charge[:-1])
      
      nonbonded[an] = [float(line[1]), 0.0, 0.0, charge, comment and " ; "+comment or ""] # mass, sigma, epsilon, charge
    if mode == "NONBON":
      line = line.split()
      sigma = float(line[1])
      epsilon = float(line[2])

      an, charge = splitnonalpha(line[0])
      an = rename.get(an, an)
      nonbonded[an][1] = 2 * sigma * 2**(-1./6) * .1
      nonbonded[an][2] = epsilon*4.184

def output():
  ffnonbonded = open("ffnonbonded.itp.ori").readlines()
  done = []
  
  resnames = {}
  
  for idx, line in enumerate(ffnonbonded):
    if line.split()[0] in nonbonded:
      name = line.split()[0]
      mass, sigma, epsilon, charge, comment = nonbonded[name]
      nr = int(mass/2)
      if nr == 0: nr = 1
      ffnonbonded[idx] = "%-9s %4i %10.2f  %8.4f  A  %12.5e %12.5e%s\n"%(name, nr, mass, 0.0, sigma, epsilon, comment)
      done.append(name)
      
      resname = renameres.get(name, name.upper())
      resnames[resname] = name
  
  ffnonbonded.append("\n")
  for name, (mass, sigma, epsilon, charge, comment) in nonbonded.iteritems():
    if name in done: continue
    nr = int(mass/2)
    if nr == 0: nr = 1
    ffnonbonded.append("%-9s %4i %10.2f  %8.4f  A  %12.5e %12.5e%s\n"%(name, nr, mass, 0.0, sigma, epsilon, comment))
    
  open("ffnonbonded.itp", "w").write("".join(ffnonbonded))
    
  ions = open("ions.itp.ori").readlines()
  done = []
  
  for idx, line in enumerate(ions):
    if not line.strip() or line[0] == ";": continue
    if line.strip() == "[ atoms ]": 
      mode = "atoms"
      continue
    elif line.strip() == "[ moleculetype ]": 
      mode = "moleculetype"
      continue
    elif "[" in line:
      print line
      exit()
    
    if mode == "atoms":
      if line.split()[1] in nonbonded:
        name = line.split()[1]
        mass, sigma, epsilon, charge, comment = nonbonded[name]
        
        resname = renameres.get(name, name.upper())
        ions[idx] = "1       %-2s              1       %-2s              %-2s       1      %7.5f\n"%(name, resname, resname, charge)
        done.append(name)
        
    if mode == "moleculetype":
      if line.split()[0] in resnames: ions[idx] = "%-2s              1\n"%line.split()[0]
    
  ions.append("\n")
    
  for name, (mass, sigma, epsilon, charge, comment) in nonbonded.iteritems():
    if name in done: continue
    resname = renameres.get(name, name.upper())

    ions.append("[ moleculetype ]\n")
    ions.append("; molname       nrexcl\n")
    ions.append("%-2s              1\n"%resname)
    ions.append("\n")
    ions.append("[ atoms ]\n")
    ions.append("; id    at type         res nr  residu name     at name  cg nr  charge\n")
    ions.append("1       %-2s              1       %-2s              %-2s       1      %7.5f%s\n"%(name, resname, resname, charge, comment))
    ions.append("\n")
    
  open("ions.itp", "w").write("".join(ions))
  
def output2():
  aminoacids = open("aminoacids.rtp.ori").readlines()
  
  done = ["RA"]
  UREline = -1
  for idx, line in enumerate(aminoacids):
    if line.startswith("[ URE ]"): UREline = idx
    if line[0] == "[": done.append(line.split()[1])
    
  for name, (mass, sigma, epsilon, charge, comment) in nonbonded.iteritems():
    resname = renameres.get(name, name.upper())
    if resname in done: continue
    line = """[ %s ]
 [ atoms ]
   %s         %s       %7.5f     1%s

"""%(resname, resname, name, charge, comment)

    aminoacids.insert(UREline, line)
    
  open("aminoacids.rtp", "w").write("".join(aminoacids))

  
  done = ["RA"]
  
  atomtypes = open("atomtypes.atp.ori").readlines()
  for idx, line in enumerate(atomtypes): done.append(line.split()[0])
  
  for name, (mass, sigma, epsilon, charge, comment) in nonbonded.iteritems():
    resname = renameres.get(name, name.upper())
    if name in done: continue
    
    atomtypes.append("%s                %7.5f %s\n"%(name, mass, comment))
    
  open("atomtypes.atp", "w").write("".join(atomtypes))
 
process(mono, "joung & cheatham 2008")
process(di, "li & merz 2013")
output2()























